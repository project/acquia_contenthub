<?php

namespace Drupal\Tests\acquia_contenthub\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub\Settings\ConnectionDetailsFromConfig
 *
 * @group acquia_contenthub
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel
 */
class ConnectionDetailsFromConfigTest extends KernelTestBase {

  use AcquiaContentHubAdminSettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'depcalc',
    'acquia_contenthub',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setup(): void {
    parent::setUp();
    $this->createAcquiaContentHubAdminSettings();
  }

  /**
   * @covers ::getOriginal
   */
  public function testOverriddenConfigValues(): void {
    // Override client name value.
    // @codingStandardsIgnoreLine
    $GLOBALS['config']['acquia_contenthub.admin_settings']['client_name'] = 'Overridden Client Name';
    /** @var \Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface $ach_connection_details */
    $ach_connection_details = $this->container->get('acquia_contenthub.connection_details_config');
    $this->assertEquals('test-client', $ach_connection_details->getOriginal('client_name'));
    $this->assertEquals('Overridden Client Name', $ach_connection_details->getClientName());
  }

}
