<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\PreEntitySave;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Event\PreEntitySaveEvent;
use Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\LoadPathField;
use Drupal\depcalc\DependencyStack;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\path_alias\PathAliasInterface;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests loading of path field on PreEntitySaveEvent.
 *
 * @group acquia_contenthub
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\LoadPathField
 *
 * @package Drupal\acquia_contenthub\EventSubscriber\PreEntitySave
 */
class LoadPathFieldTest extends KernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * LoadPathField event subscriber.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\LoadPathField
   */
  protected LoadPathField $loadPathField;

  /**
   * The PreEntitySave event.
   *
   * @var \Drupal\acquia_contenthub\Event\PreEntitySaveEvent
   */
  protected PreEntitySaveEvent $event;

  /**
   * The node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $node;

  /**
   * The path_alias entity.
   *
   * @var \Drupal\path_alias\PathAliasInterface
   */
  protected PathAliasInterface $pathAlias;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'depcalc',
    'user',
    'path',
    'path_alias',
    'field',
    'system',
    'text',
    'node',
    'filter',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('path_alias');
    $this->installConfig([
      'field',
      'filter',
      'node',
    ]);

    $this->createContentType([
      'type' => 'test',
    ]);
    $this->node = Node::create([
      'title' => 'Test',
      'type' => 'test',
    ]);
    $this->node->save();

    $this->pathAlias = PathAlias::create([
      'path' => '/node/' . $this->node->id(),
      'alias' => 'node_path',
    ]);
    $this->pathAlias->save();

    $cdf = new CDFObject(
      'drupal8_content_entity',
      'uuid',
      'date',
      'date',
      'uuid'
    );

    $this->event = new PreEntitySaveEvent($this->node, new DependencyStack(), $cdf);
    $module_handler = $this->container->get('module_handler');
    $this->loadPathField = new LoadPathField($module_handler);
  }

  /**
   * Tests loading of path field for node.
   *
   * @covers ::onPreEntitySave
   */
  public function testLoadPathField(): void {
    $this->assertEquals('node_path', $this->node->get('path')->getValue()['0']['alias']);
    $this->pathAlias->setAlias('node_path_updated')->save();
    $this->loadPathField->onPreEntitySave($this->event);
    $this->assertEquals('node_path_updated', $this->event->getEntity()->get('path')->getValue()['0']['alias']);
  }

  /**
   * Tests loading of path field when path_alias entity is deleted.
   *
   * @covers ::onPreEntitySave
   */
  public function testLoadPathFieldWhenPathAliasIsDeleted(): void {
    $this->assertEquals('node_path', $this->node->get('path')->getValue()['0']['alias']);
    $this->pathAlias->delete();
    $this->loadPathField->onPreEntitySave($this->event);
    $this->assertEquals('node_path', $this->event->getEntity()->get('path')->getValue()['0']['alias']);
  }

}
