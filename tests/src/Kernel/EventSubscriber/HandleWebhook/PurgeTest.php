<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\HandleWebhook;

use Acquia\Hmac\Key;
use Drupal\acquia_contenthub\Client\CdfMetricsManager;
use Drupal\acquia_contenthub\Event\HandleWebhookEvent;
use Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface;
use Drupal\acquia_contenthub_publisher\EventSubscriber\HandleWebhook\Purge;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests queue and export table purge.
 *
 * @group acquia_contenthub
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\EventSubscriber\HandleWebhook\Purge
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\HandleWebhook
 */
class PurgeTest extends EntityKernelTestBase {

  use ProphecyTrait;

  /**
   * Purge class instance.
   *
   * @var \Drupal\acquia_contenthub_publisher\EventSubscriber\HandleWebhook\Purge
   */
  protected $purge;

  /**
   * Content Hub Client Factory.
   *
   * @var \Drupal\acquia_contenthub\Client\ClientFactory
   */
  protected $clientFactory;

  /**
   * Contenhub configurations.
   *
   * @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface
   */
  protected ContentHubConfigurationInterface $achConfigurations;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Acquia Logger Channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerChannel;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'acquia_contenthub_server_test',
    'depcalc',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('acquia_contenthub_publisher', ['acquia_contenthub_publisher_export_tracking']);

    $this->achConfigurations = $this->container->get('acquia_contenthub.configuration');
    $this->createAcquiaContentHubAdminSettings();
    $this->clientFactory = $this->container->get('acquia_contenthub.client.factory');

    // Create a test node.
    Node::create([
      'type' => 'article',
      'title' => 'Test EN',
    ])->save();

    $this->queueFactory = $this->container->get('queue');
    $this->database = $this->container->get('database');
    $this->loggerChannel = $this->prophesize(LoggerInterface::class)
      ->reveal();

    $cdf_metrics_manager = $this->prophesize(CdfMetricsManager::class);
    $cdf_metrics_manager
      ->sendClientCdfUpdates(Argument::any())
      ->shouldBeCalledOnce();
    $this->purge = new Purge($this->queueFactory, $this->loggerChannel, $cdf_metrics_manager->reveal(), $this->database);
  }

  /**
   * Creates Acquia Content Hub settings.
   */
  public function createAcquiaContentHubAdminSettings(): void {
    $ch_connection = $this->achConfigurations->getConnectionDetails();
    $ch_connection->setClientName('test-client');
    $ch_connection->setClientUuid('00000000-0000-0001-0000-123456789123');
    $ch_connection->setApiKey('12312321312321');
    $ch_connection->setSecretKey('12312321312321');
    $ch_connection->setHostname('https://example.com');
    $ch_connection->setSharedSecret('12312321312321');
  }

  /**
   * Tests queue and export table purging.
   */
  public function testQueueAndTablePurge() {
    // Before purge.
    $queue = $this->queueFactory->get('acquia_contenthub_publish_export');
    $this->assertGreaterThan(0, $queue->numberOfItems());
    $this->assertGreaterThan(0, $this->getExportTableCount());

    $request = Request::createFromGlobals();
    $key = new Key('id', 'secret');
    $payload = [
      'crud' => 'purge',
      'status' => 'successful',
    ];
    $event = new HandleWebhookEvent($request, $payload, $key, $this->clientFactory->getClient());
    $this->purge->onHandleWebhook($event);

    // After purge.
    $this->assertEquals($queue->numberOfItems(), 0);
    $this->assertEquals($this->getExportTableCount(), 0);
  }

  /**
   * Fetch table rows count.
   *
   * @return int
   *   Table rows count.
   */
  protected function getExportTableCount(): int {
    return $this->database->select('acquia_contenthub_publisher_export_tracking', 'pet')
      ->fields('pet', [])
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
