<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\HandleWebhook\Traits;

use Acquia\ContentHubClient\ContentHubClient;
use Acquia\Hmac\Key;
use Drupal\acquia_contenthub\Event\HandleWebhookEvent;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\RequestTrait;
use Drupal\Tests\acquia_contenthub\Traits\CommonRandomGenerator;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Helper function for HandleWebhookEvent tests.
 */
trait HandleWebhookEventTestTrait {

  use AcquiaContentHubAdminSettingsTrait;
  use CommonRandomGenerator;
  use ProphecyTrait;
  use RequestTrait;

  /**
   * Returns a HandleWebhookEvent object.
   *
   * @param array $payload
   *   The payload.
   *
   * @code
   *   [
   *     'status' => 'successful',
   *     'crud' => 'purge',
   *   ]
   * @endcode
   *
   * @return \Drupal\acquia_contenthub\Event\HandleWebhookEvent
   *   The HandleWebhookEvent.
   */
  public function newHandleWebhookEvent(array $payload): HandleWebhookEvent {
    $this->createAcquiaContentHubAdminSettings();

    /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configuration */
    $ach_configuration = $this->container->get('acquia_contenthub.configuration');
    $settings = $ach_configuration->getSettings();
    $client = $this->prophesize(ContentHubClient::class);
    $client->getSettings()->willReturn($settings);

    return new HandleWebhookEvent(
      $this->createSignedRequest(),
      $payload,
      new Key($settings->getApiKey(), $settings->getSecretKey()),
      $client->reveal(),
    );
  }

}
