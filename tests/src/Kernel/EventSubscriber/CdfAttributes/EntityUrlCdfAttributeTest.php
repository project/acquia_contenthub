<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\CdfAttributes;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Event\CdfAttributesEvent;
use Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\EntityUrlCdfAttribute;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\depcalc\DependentEntityWrapper;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Tests whether url attribute is added to entity CDF.
 *
 * @group acquia_contenthub
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\EntityUrlCdfAttribute
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\CdfAttributes
 */
class EntityUrlCdfAttributeTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'depcalc',
    'system',
    'node',
    'language',
  ];

  /**
   * ES default node.
   *
   * @var \Drupal\node\NodeInterface
   */
  private NodeInterface $esDefaultNode;

  /**
   * Event Subscriber for entity url attribute.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\EntityUrlCdfAttribute
   */
  protected EntityUrlCdfAttribute $entityUrlAttribute;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritDoc}
   */
  protected function setup(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['language']);
    $this->languageManager = $this->container->get('language_manager');
    $language = ConfigurableLanguage::createFromLangcode('es');
    $language->save();
    $config = $this->config('language.negotiation');
    $config->set('url.prefixes', ['en' => 'en', 'es' => 'es'])
      ->save();
    \Drupal::service('kernel')->rebuildContainer();
    // Change default langcode.
    $this->config('system.site')->set('default_langcode', 'es')->set('langcode', 'es')->save();
    $this->languageManager->reset();
    $this->esDefaultNode = Node::create([
      'type' => 'article',
      'langcode' => 'es',
      'uid' => 1,
      'title' => 'Es default node',
    ]);
    $this->esDefaultNode->save();
    $this->entityUrlAttribute = new EntityUrlCdfAttribute();
  }

  /**
   * Tests url attribute in entity cdf.
   *
   * @throws \Exception
   */
  public function testUrlCdfAttribute(): void {
    $es_default_cdf = new CDFObject('drupal8_content_entity', $this->esDefaultNode->uuid(), date('c'), date('c'), 'default-uuid');
    $es_event = new CdfAttributesEvent($es_default_cdf, $this->esDefaultNode, new DependentEntityWrapper($this->esDefaultNode));
    $this->entityUrlAttribute->onPopulateAttributes($es_event);
    $url = $es_event->getCdf()->getAttribute('url')->getValue()[Language::LANGCODE_NOT_SPECIFIED];
    $this->assertEquals('http://localhost/es/node/' . $this->esDefaultNode->id(), $url);
  }

}
