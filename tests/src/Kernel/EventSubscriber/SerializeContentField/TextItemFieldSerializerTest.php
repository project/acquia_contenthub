<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\SerializeContentField;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Event\SerializeCdfEntityFieldEvent;
use Drupal\acquia_contenthub\EventSubscriber\SerializeContentField\TextItemFieldSerializer;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\NodeInterface;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\SerializeContentField\TextItemFieldSerializer
 *
 * @requires module depcalc
 *
 * @package acquia_contenthub
 */
class TextItemFieldSerializerTest extends EntityKernelTestBase {

  use NodeCreationTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'acquia_contenthub',
    'depcalc',
  ];

  /**
   * Node entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $entity;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig('node');
    $this->createContentType(['type' => 'page']);
    $this->entity = $this->createNode([
      'title' => 'The node title',
      'type' => 'page',
      'body' => [
        [
          'value' => '',
        ],
      ],
    ]);
  }

  /**
   * Tests onSerializeContentField.
   *
   * @covers ::onSerializeContentField
   *
   * @throws \Exception
   */
  public function testTextItemFieldSerialization(): void {
    $event = new SerializeCdfEntityFieldEvent($this->entity, 'body', $this->entity->get('body'), $this->prophesize(CDFObject::class)->reveal());
    $serializer = new TextItemFieldSerializer($this->prophesize(EntityTypeManagerInterface::class)->reveal());
    $serializer->onSerializeContentField($event);
    $values = $event->getFieldData();
    self::assertEquals([], $values['value']['en']);
  }

}
