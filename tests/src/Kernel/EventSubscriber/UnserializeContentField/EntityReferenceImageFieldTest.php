<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\UnserializeContentField;

use Drupal\acquia_contenthub\Event\UnserializeCdfEntityFieldEvent;
use Drupal\acquia_contenthub\EventSubscriber\UnserializeContentField\EntityReferenceImageField;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\depcalc\DependencyStack;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\acquia_contenthub\Unit\Helpers\LoggerMock;

/**
 * Test that entity reference are handled correctly during unserialization.
 *
 * @group acquia_contenthub
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\UnserializeContentField\EntityReferenceImageField
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\EntityReferenceImageField
 */
class EntityReferenceImageFieldTest extends KernelTestBase {

  /**
   * Entity Bundle name.
   */
  protected const BUNDLE = 'article';

  /**
   * Field name.
   */
  protected const FIELD_NAME = 'test_image';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'depcalc',
    'field',
    'filter',
    'system',
    'file',
    'user',
  ];

  /**
   * Logger channel mock.
   *
   * @var \Drupal\Tests\acquia_contenthub\Unit\Helpers\LoggerMock
   */
  protected $loggerChannel;

  /**
   * EntityReferenceImageField instance.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\UnserializeContentField\EntityReferenceImageField
   */
  protected $sut;

  /**
   * Unserialize Cdf entity field event.
   *
   * @var \Drupal\acquia_contenthub\Event\UnserializeCdfEntityFieldEvent
   */
  protected $event;

  /**
   * Dependency stack.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $stack;

  /**
   * Entity type.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $entityType;

  /**
   * Field meta data.
   *
   * @var string[]
   */
  protected $metaData;

  /**
   * Field mock value.
   *
   * @var string[]
   */
  protected $mockField;

  /**
   * Image uuid to check.
   *
   * @var string
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');

    $this->stack = $this->prophesize(DependencyStack::class);
    $this->entityType = $this->prophesize(EntityTypeInterface::class);
    $this->metaData = [
      'type' => 'image',
      'target' => 'file',
    ];

    $this->uuid = $this->container->get('uuid')->generate();
    $this->mockField = [
      'value' => [
        'en' => [
          'target_id' => $this->uuid,
        ],
      ],
    ];
    $this->event = new UnserializeCdfEntityFieldEvent($this->entityType->reveal(), self::BUNDLE, self::FIELD_NAME, $this->mockField, $this->metaData, $this->stack->reveal());
    $this->loggerChannel = new LoggerMock();
    $this->sut = new EntityReferenceImageField($this->loggerChannel, $this->container->get('acquia_contenthub.pruned_entities.tracker'));
  }

  /**
   * @covers ::onUnserializeContentField
   *
   * @throws \Exception
   */
  public function testImageFieldUnSerializerException(): void {
    $this->sut->onUnserializeContentField($this->event);

    $log_messages = $this->loggerChannel->getLogMessages();
    $this->assertNotEmpty($log_messages);
    $this->assertEquals("Entity with $this->uuid not found in DependencyStack or subscriber's database while unserializing field values.", $log_messages[RfcLogLevel::ERROR][0]);
    $this->assertTrue($this->event->isPropagationStopped());
  }

}
