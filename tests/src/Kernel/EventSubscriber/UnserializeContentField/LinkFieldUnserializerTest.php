<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\UnserializeContentField;

use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\UnserializeCdfEntityFieldEvent;
use Drupal\acquia_contenthub\EventSubscriber\UnserializeContentField\LinkFieldUnserializer;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\depcalc\DependencyStack;
use Drupal\depcalc\DependentEntityWrapper;
use Drupal\KernelTests\KernelTestBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\acquia_contenthub\Unit\Helpers\LoggerMock;
use Prophecy\Argument;

/**
 * Test that links are handled correctly during unserialization.
 *
 * @group acquia_contenthub
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\UnserializeContentField\LinkFieldUnserializer
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\UnserializeContentField
 */
class LinkFieldUnserializerTest extends KernelTestBase {

  /**
   * Logger channel mock.
   *
   * @var \Drupal\Tests\acquia_contenthub\Unit\Helpers\LoggerMock
   */
  protected LoggerMock $loggerChannel;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'depcalc',
    'user',
    'link',
    'field',
    'system',
    'taxonomy',
    'text',
  ];

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcher
   */
  protected $dispatcher;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setup(): void {
    parent::setUp();
    $this->installEntitySchema('field_config');
    $this->installEntitySchema('taxonomy_term');
    $this->installConfig(['system']);

    $this->dispatcher = $this->container->get('event_dispatcher');
    $this->loggerChannel = new LoggerMock();
  }

  /**
   * Tests link field un-serializer.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testLinkFieldUnSerializer() {
    // Create mock taxonomy term.
    $term = Term::create([
      'name' => $this->randomMachineName(),
      'vid' => 'tags',
    ]);
    $term->save();

    $stack = $this->prophesize(DependencyStack::class);
    $wrapper = $this->prophesize(DependentEntityWrapper::class);
    $wrapper->getEntity()->willReturn($term);
    $stack->getDependency(Argument::any())->willReturn($wrapper->reveal());

    $meta_data = [
      'type' => 'link',
    ];

    $mock_field = $this->getMockField();

    $entity_type = $this->prophesize(EntityTypeInterface::class);
    $event = new UnserializeCdfEntityFieldEvent($entity_type->reveal(), 'test', 'link', $mock_field, $meta_data, $stack->reveal());
    $this->dispatcher->dispatch($event, AcquiaContentHubEvents::UNSERIALIZE_CONTENT_ENTITY_FIELD);

    $expected = [
      'en' => [
        'link' => [
          0 => [
            'uri' => 'internal:/taxonomy/term/1',
            'title' => 'test',
            'options' => [],
          ],
        ],
      ],
      'hu' => [
        'link' => [
          0 => [],
        ],
      ],
    ];

    $this->assertEquals($expected, $event->getValue());
  }

  /**
   * Tests link field unserializer with missing dependency.
   *
   * @throws \Exception
   */
  public function testLinkFieldUnSerializerWithMissingDependency(): void {
    $stack = $this->prophesize(DependencyStack::class);
    $stack->getDependency(Argument::any())->willReturn(NULL);

    $mock_field = $this->getMockField();
    $meta_data = [
      'type' => 'link',
    ];
    $sut = new LinkFieldUnserializer($this->loggerChannel);
    $entity_type = $this->prophesize(EntityTypeInterface::class);
    $event = new UnserializeCdfEntityFieldEvent($entity_type->reveal(), 'test', 'link', $mock_field, $meta_data, $stack->reveal());

    $sut->onUnserializeContentField($event);
    $log_messages = $this->loggerChannel->getLogMessages();
    $this->assertNotEmpty($log_messages);
    $uuid = $mock_field['value']['en'][0]['uri'];
    $this->assertEquals("Entity {$uuid} could not be found in the dependency stack during link field unserialsation.", $log_messages[RfcLogLevel::ERROR][0]);

    $dependency_wrapper = $this->prophesize(DependentEntityWrapper::class);
    $entity = Term::create([
      'name' => $this->randomMachineName(),
      'vid' => 'tags',
    ]);
    $entity->save();
    $dependency_wrapper->getEntity()->willReturn($entity);
    $stack->getDependency(Argument::any())->willReturn($dependency_wrapper->reveal());
    $this->loggerChannel->reset();
    $sut = new LinkFieldUnserializer($this->loggerChannel);
    $event = new UnserializeCdfEntityFieldEvent($entity->getEntityType(), 'test', 'link', $mock_field, $meta_data, $stack->reveal());
    $sut->onUnserializeContentField($event);
    $log_messages = $this->loggerChannel->getLogMessages();
    $this->assertEmpty($log_messages);
  }

  /**
   * Provides mock link field data.
   *
   * @return array[]
   *   Link field data.
   */
  private function getMockField(): array {
    return [
      'value' => [
        'en' => [
          0 => [
            'uri' => 'uuid',
            'title' => 'test',
            'options' => [],
            'uri_type' => 'internal',
            'internal_type' => 'internal_entity',
          ],
        ],
        'hu' => [
          0 => NULL,
        ],
      ],
    ];
  }

}
