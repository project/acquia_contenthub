<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\Stubs;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Plugin\FileSchemeHandler\FileHandlerTrait;
use Drupal\acquia_contenthub\Plugin\FileSchemeHandler\PublicFileSchemeHandler as OriginalPublicFileSchemeHandler;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Handler for public files.
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel\Stubs
 */
class PublicFileSchemeHandler extends OriginalPublicFileSchemeHandler {

  use FileHandlerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFile(CDFObject $object) {
    $this->validateAttributes($object);
    $url = $object->getAttribute('file_location')->getValue()[LanguageInterface::LANGCODE_NOT_SPECIFIED];
    $path = \Drupal::service('module_handler')->getModule('acquia_contenthub')->getPath();
    $url = str_replace('module::', $path, $url);
    $contents = file_get_contents($url);
    $uri = $object->getAttribute('file_uri')->getValue()[LanguageInterface::LANGCODE_NOT_SPECIFIED];
    return $this->saveData($contents, $uri, FileSystemInterface::EXISTS_REPLACE);
  }

}
