<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\Traits;

/**
 * Trait for ACH admin settings configurations.
 */
trait AcquiaContentHubAdminSettingsTrait {

  /**
   * Content Hub admin settings.
   *
   * @var string[]
   */
  private $chAdminSettings = [
    'client_name' => 'test-client',
    'origin' => '00000000-0000-0001-0000-123456789123',
    'api_key' => '12312321312321',
    'secret_key' => '12312321312321',
    'hostname' => 'https://dev.content-hub.dev',
    'shared_secret' => '12312321312321',
    'event_service_url' => 'http://example.com',
    'webhook' => [
      'uuid' => 'valid-uuid',
    ],
  ];

  /**
   * Get Acquia Content Hub settings.
   *
   * @param array $settings
   *   Content Hub settings.
   */
  protected function createAcquiaContentHubAdminSettings(array $settings = []): void {
    $this->chAdminSettings = array_merge($this->chAdminSettings, $settings);

    /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configuration */
    $ach_configuration = $this->container->get('acquia_contenthub.configuration');
    $ch_connection = $ach_configuration->getConnectionDetails();

    $ch_connection->resetAchConfigs($this->chAdminSettings);
  }

  /**
   * Returns the admin settings value.
   *
   * @return array
   *   The admin settings array saved as config.
   */
  public function getChAdminSettings(): array {
    return $this->chAdminSettings;
  }

}
