<?php

namespace Drupal\Tests\acquia_contenthub\Kernel\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Helper trait for field storage and field config.
 */
trait FieldTrait {

  /**
   * Creates field storage.
   *
   * @param string $field_name
   *   The name of the field.
   * @param string $entity_type
   *   The entity type.
   * @param string $field_type
   *   The field type.
   * @param array $settings
   *   Array of settings for field storage.
   * @param int $cardinality
   *   Value for cardinality.
   *
   * @return \Drupal\field\Entity\FieldStorageConfig
   *   Creates and returns field storage configuration.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldStorage(string $field_name, string $entity_type, string $field_type, array $settings = [], int $cardinality = 1): FieldStorageConfig {
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => $field_type,
      'cardinality' => $cardinality,
      'settings' => $settings,
    ]);
    $field_storage->save();

    return $field_storage;
  }

  /**
   * Creates field configurations.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $field_storage
   *   The field storage configuration.
   * @param string $bundle
   *   The bundle name.
   *
   * @return \Drupal\field\Entity\FieldConfig
   *   Creates and returns field configurations.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldConfig(FieldStorageConfig $field_storage, string $bundle): FieldConfig {
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $bundle,
    ]);
    $field->save();

    return $field;
  }

  /**
   * Creates a field of an entity reference field storage for specified bundle.
   *
   * @param string $entity_type
   *   The type of entity the field will be attached to.
   * @param string $bundle
   *   The bundle name of the entity the field will be attached to.
   * @param string $field_name
   *   The name of the field; if it already exists, a new instance for existing
   *   field will be created.
   * @param string $field_label
   *   The label of the field.
   * @param string $target_entity_type
   *   The type of the referenced entity.
   * @param string $selection_handler
   *   The selection handler used by this field.
   * @param array $selection_handler_settings
   *   An array of settings supported by the selection handler specified above.
   *   (e.g. 'target_bundles', 'sort', 'auto_create', etc).
   * @param int $cardinality
   *   The cardinality of the field.
   *
   * @see \Drupal\Core\Entity\Plugin\EntityReferenceSelection\SelectionBase::buildConfigurationForm()
   */
  protected function createEntityReferenceField($entity_type, $bundle, $field_name, $field_label, $target_entity_type, $selection_handler = 'default', $selection_handler_settings = [], $cardinality = 1) {
    // Look for or add the specified field to the requested entity bundle.
    if (!FieldStorageConfig::loadByName($entity_type, $field_name)) {
      FieldStorageConfig::create([
        'field_name' => $field_name,
        'type' => 'entity_reference',
        'entity_type' => $entity_type,
        'cardinality' => $cardinality,
        'settings' => [
          'target_type' => $target_entity_type,
        ],
      ])->save();
    }
    if (!FieldConfig::loadByName($entity_type, $bundle, $field_name)) {
      FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $field_label,
        'settings' => [
          'handler' => $selection_handler,
          'handler_settings' => $selection_handler_settings,
        ],
      ])->save();
    }
  }

}
