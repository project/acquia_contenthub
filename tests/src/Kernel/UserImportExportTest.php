<?php

namespace Drupal\Tests\acquia_contenthub\Kernel;

/**
 * Tests user export and import.
 *
 * @group acquia_contenthub
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel
 */
class UserImportExportTest extends ImportExportTestBase {

  /**
   * {@inheritdoc}
   */
  protected $fixtures = [
    [
      'cdf' => 'user/user.json',
      'expectations' => 'expectations/user/user.php',
    ],
    [
      'cdf' => 'user/user_no_email.json',
      'expectations' => 'expectations/user/user_no_email.php',
    ],
    [
      'cdf' => 'node/node_page.json',
      'expectations' => 'expectations/node/node_page_user.php',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'field',
    'depcalc',
    'acquia_contenthub',
    'editor',
    'ckeditor5',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setup(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
  }

  /**
   * Tests "user" Drupal entity.
   *
   * @param mixed $args
   *   Arguments. @see ImportExportTestBase::contentEntityImportExport() for the
   *   details.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider userImportExportDataProvider
   */
  public function testUserImportExport(...$args) {
    parent::contentEntityImportExport(...$args);
  }

  /**
   * Data provider for testUserImport.
   *
   * @return array
   *   Data provider set.
   */
  public static function userImportExportDataProvider(): array {
    $export_uuid = '5d1ba3c3-d527-4328-8fce-a6b714c5ef79';
    $user_uuid = '995f955b-08a9-4436-a0c7-1cde093ee174';

    return [
      [
        0,
        [['type' => 'user', 'uuid' => 'f150c156-ef63-4f08-8d69-f15e5ee11106']],
        'user',
        'f150c156-ef63-4f08-8d69-f15e5ee11106',
      ],
      [
        1,
        [['type' => 'user', 'uuid' => 'f150c156-ef63-4f08-8d69-f15e5ee11106']],
        'user',
        'f150c156-ef63-4f08-8d69-f15e5ee11106',
      ],
      [
        2,
        [['type' => 'user', 'uuid' => $user_uuid]],
        'node',
        $export_uuid,
      ],
    ];
  }

}
