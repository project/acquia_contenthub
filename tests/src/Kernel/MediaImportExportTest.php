<?php

namespace Drupal\Tests\acquia_contenthub\Kernel;

use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\Tests\acquia_contenthub\Kernel\Core\FileSystemTrait;

/**
 * Tests media export and import.
 *
 * @group acquia_contenthub
 *
 * @package Drupal\Tests\acquia_contenthub\Kernel
 */
class MediaImportExportTest extends ImportExportTestBase {

  use FileSystemTrait;

  /**
   * {@inheritdoc}
   */
  protected $fixtures = [
    [
      'cdf' => 'media/node-with-media-image.json',
      'expectations' => 'expectations/media/reference_1.php',
    ],
    [
      'cdf' => 'media/node-with-multiple-media.json',
      'expectations' => 'expectations/media/reference_2.php',
    ],
    [
      'cdf' => 'media/node-with-media-image.json',
      'expectations' => 'expectations/media/reference_3.php',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'file',
    'field',
    'image',
    'media_library',
    'media',
    'views',
    'depcalc',
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
  ];

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installSchema('acquia_contenthub_subscriber', [SubscriberTracker::IMPORT_TRACKING_TABLE]);
    $this->installEntitySchema('node');
    $this->installEntitySchema('view');
    $this->installEntitySchema('media');
    $this->fileSystemSetUp();
  }

  /**
   * Tests import/export of node with media.
   *
   * @param int $delta
   *   Delta.
   * @param array $validate_data
   *   Data.
   * @param string $export_type
   *   Entity type ID.
   * @param string $export_uuid
   *   Uuid.
   *
   * @throws \Exception
   *
   * @dataProvider mediaImportExportDataProvider
   */
  public function testMediaImportExport($delta, array $validate_data, $export_type, $export_uuid) {
    parent::contentEntityImportExport($delta, $validate_data, $export_type, $export_uuid);
  }

  /**
   * Data provider for testFileImportExport.
   */
  public static function mediaImportExportDataProvider() {
    yield [
      0,
      [
        [
          'type' => 'file',
          'uuid' => '7e67c03a-24b1-4e38-892b-17405d1cbbd0',
        ],
      ],
      'node',
      'ccebfd47-eecf-400e-8647-5a2c8b35b636',
    ];
    yield [
      1,
      [
        [
          'type' => 'file',
          'uuid' => 'f35293d3-7ade-4514-852f-bcbca4aae158',
        ],
        [
          'type' => 'file',
          'uuid' => '7e67c03a-24b1-4e38-892b-17405d1cbbd0',
        ],
      ],
      'node',
      'a5df0de7-0184-4734-88d7-6eef3e6ef5d4',
    ];
    yield [
      2,
      [
        [
          'type' => 'media',
          'uuid' => '130c8c1d-e83e-4990-8ccd-41eb316b3d43',
        ],
      ],
      'node',
      'ccebfd47-eecf-400e-8647-5a2c8b35b636',
    ];
  }

}
