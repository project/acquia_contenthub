<?php

namespace Drupal\Tests\acquia_contenthub\Traits;

use Drupal\Component\Uuid\Php;

/**
 * Contains helper methods for common use cases.
 */
trait CommonRandomGenerator {

  /**
   * Generates a new uuid.
   *
   * @return string
   *   The uuid.
   */
  public function generateUuid(): string {
    $uuid = new Php();
    return $uuid->generate();
  }

  /**
   * Takes a random element of an array.
   *
   * @param array $input
   *   The array to take an element from.
   *
   * @return mixed
   *   The value of the element.
   */
  public function takeRandom(array $input) {
    $index = array_rand($input);
    return $input[$index];
  }

}
