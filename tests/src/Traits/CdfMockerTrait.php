<?php

namespace Drupal\Tests\acquia_contenthub\Traits;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\Core\Language\LanguageInterface;

/**
 * Provides helper functions to generate mock CDFs.
 */
trait CdfMockerTrait {

  use CommonRandomGenerator;

  /**
   * Generates a file CDF.
   *
   * The minimal valuable CDF.
   *
   * @param \Acquia\ContentHubClient\CDFAttribute[] $attributes
   *   The attributes to use for the generation.
   *
   * @return \Acquia\ContentHubClient\CDF\CDFObject
   *   The file CDF.
   *
   * @throws \Exception
   */
  public function generateFileCdf(array $attributes = []): CDFObject {
    $object = new CDFObject('drupal8_content_entity', $this->generateUuid(), time(), time(), $this->generateUuid());
    $object->addAttribute('entity_type', 'string', 'file');
    foreach ($attributes as $attribute) {
      $object->addAttribute($attribute->getId(), $attribute->getType(), $attribute->getValue()[LanguageInterface::LANGCODE_NOT_SPECIFIED]);
    }
    return $object;
  }

}
