<?php

namespace Drupal\Tests\acquia_contenthub\Traits;

use PHPUnit\Framework\Assert;

/**
 * Helper functions for database assertions.
 */
trait DatabaseAssertionsTrait {

  /**
   * Asserts the number of rows in a given table.
   *
   * @param string $table_name
   *   The table to use for the assertion.
   * @param int $expected
   *   The number of rows expected.
   */
  public function assertNumberOfRows(string $table_name, int $expected) {
    $actual = \Drupal::database()
      ->select($table_name)
      ->countQuery()
      ->execute()
      ->fetchField();
    Assert::assertEquals($expected, $actual);
  }

}
