<?php

namespace Drupal\Tests\acquia_contenthub\Unit\EventSubscriber\DisablePathautoTest;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Event\PreEntitySaveEvent;
use Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\DisablePathauto;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\depcalc\DependencyStack;
use Drupal\node\NodeInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests disable pathauto.
 *
 * @group acquia_contenthub
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\DisablePathauto
 *
 * @package Drupal\acquia_contenthub\EventSubscriber\PreEntitySave
 */
class DisablePathautoTest extends UnitTestCase {

  /**
   * DisablePathauto event subscriber.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\DisablePathauto
   */
  protected $disablePathAuto;

  /**
   * The DisablePathauto event.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\PreEntitySave\DisablePathauto
   */
  protected $event;

  /**
   * The entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $cdf = new CDFObject(
      'drupal8_content_entity',
      'uuid',
      'date',
      'date',
      'uuid'
    );
    $field = (object) [
      'pathauto' => 1,
    ];
    $this->entity = $this->prophesize(NodeInterface::class);
    $this->entity->hasField('path')->willReturn(TRUE);
    $this->entity->get('path')->willReturn($field);
    $this->entity = $this->entity->reveal();
    $this->event = new PreEntitySaveEvent($this->entity, new DependencyStack(), $cdf);

    $module_handler = $this->prophesize(ModuleHandler::class);
    $module_handler->moduleExists('pathauto')->willReturn(TRUE);
    $this->disablePathAuto = new DisablePathauto($module_handler->reveal());

  }

  /**
   * Tests disabling of pathauto.
   */
  public function testDisablePathAuto(): void {
    $this->assertSame(1, $this->entity->get('path')->pathauto);
    $this->disablePathAuto->onPreEntitySave($this->event);
    $this->assertSame(0, $this->entity->get('path')->pathauto);
  }

}
