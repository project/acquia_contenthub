<?php

namespace Drupal\Tests\acquia_contenthub\Unit\Libs;

use Acquia\ContentHubClient\ContentHubClient;
use Acquia\ContentHubClient\Settings;
use Acquia\ContentHubClient\StatusCodes;
use Drupal\acquia_contenthub\Exception\ContentHubClientException;
use Drupal\acquia_contenthub\Exception\PlatformIncompatibilityException;
use Drupal\acquia_contenthub\Exception\ServiceUnderMaintenanceException;
use Drupal\acquia_contenthub\Libs\Common\PlatformCompatibilityChecker;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCache;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub\Libs\Common\PlatformCompatibilityChecker
 *
 * @group acquia_contenthub
 */
class PlatformCompatibilityCheckerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * SUT object.
   *
   * @var \Drupal\acquia_contenthub\Libs\Common\PlatformCompatibilityChecker
   */
  protected $checker;

  /**
   * In memory cache to test proper caching mechanism.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCache
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->cache = new MemoryCache($this->prophesize(TimeInterface::class)->reveal());
    $this->checker = new PlatformCompatibilityChecker(
      $this->prophesize(MessengerInterface::class)->reveal(),
      $this->prophesize(LoggerInterface::class)->reveal(),
      $this->cache
    );
  }

  /**
   * @covers ::intercept
   */
  public function testInterceptWhenAccountIsFeatured(): void {
    $client = $this->newClientWithSettings(TRUE);

    $expected = $this->checker->intercept($client->reveal());
    $this->assertSame($expected, $client->reveal());
  }

  /**
   * @covers ::intercept
   */
  public function testInterceptWhenAccountIsNotFeatured(): void {
    $client = $this->newClientWithSettings(FALSE);

    $expected = $this->checker->intercept($client->reveal());
    $this->assertNull($expected);
  }

  /**
   * @covers ::interceptAndDelete
   */
  public function testInterceptAndDeleteWhenAccountIsNotFeatured(): void {
    $settings = $this->prophesize(Settings::class);
    $settings->getUuid()->willReturn('someuuid');

    $client = $this->prophesize(ContentHubClient::class);
    $client->getResponse()->willReturn(new Response(200));
    $client->isFeatured()->willReturn(FALSE);

    $client->getSettings()->willReturn($settings->reveal());
    $client->deleteClient(Argument::exact('someuuid'))->willReturn();

    $this->expectException(PlatformIncompatibilityException::class);
    $this->checker->interceptAndDelete($client->reveal());
  }

  /**
   * @covers ::interceptAndDelete
   */
  public function testInterceptAndDeleteWhenAccountIsFeatured(): void {
    $client = $this->newClientWithSettings(TRUE);

    $expected = $this->checker->interceptAndDelete($client->reveal());
    $this->assertSame($expected, $client->reveal());
  }

  /**
   * @covers ::isFeatured
   */
  public function testIsFeaturedReturnValuesWithFeaturedAccount(): void {
    $client = $this->newClientWithSettings(TRUE);
    $actual = $this->checker->isFeatured($client->reveal());
    $this->assertTrue($actual);

    $actual = $this->cache->get('some_uuid');
    $this->assertEquals(TRUE, $actual->data);
    $this->assertEquals(-1, $actual->expire);

    // Cache is still active.
    $client = $this->newClientWithSettings(FALSE);
    $client->isFeatured()->shouldNotBeCalled();
    $actual = $this->checker->isFeatured($client->reveal());
    $this->assertTrue($actual);
  }

  /**
   * @covers ::isFeatured
   */
  public function testIsFeaturedReturnValuesWithNonFeaturedAccount(): void {
    $client = $this->newClientWithSettings(FALSE);
    $actual = $this->checker->isFeatured($client->reveal());
    $this->assertFalse($actual);

    $actual = $this->cache->get('some_uuid');
    $this->assertEquals(FALSE, $actual->data);
    $time = time();
    $expire = $actual->expire;
    $this->assertTrue($expire > $time && $expire <= $time + 3600);

    // Cache is still active.
    $client = $this->newClientWithSettings(TRUE);
    $client->isFeatured()->shouldNotBeCalled();
    $actual = $this->checker->isFeatured($client->reveal());
    $this->assertFalse($actual);
  }

  /**
   * Tests interception in case of maintenance error.
   */
  public function testInterceptOnMaintenanceError(): void {
    $client = $this->newClientWithSettings(FALSE);
    $client->getResponse()->willReturn(new Response(503, [], json_encode([
      'error' => [
        'code' => StatusCodes::SERVICE_UNDER_MAINTENANCE,
        'message' => 'service under maintenance',
      ],
    ])));
    $this->expectException(ServiceUnderMaintenanceException::class);
    $this->checker->isFeatured($client->reveal());
  }

  /**
   * Tests interception in case of maintenance error.
   */
  public function testInterceptOnClientError(): void {
    $client = $this->newClientWithSettings(FALSE);
    $client->getResponse()->willReturn(new Response(503, [], json_encode([
      'error' => [
        'code' => 503,
        'message' => 'Internal Server Error',
      ],
    ])));
    $this->expectException(ContentHubClientException::class);
    $this->checker->isFeatured($client->reveal());
  }

  /**
   * Returns a new ContentHubClient prophecy.
   *
   * @param bool $will_return
   *   The value to return for isFeatured.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy|\Acquia\ContentHubClient\ContentHubClient
   *   The client prophecy.
   *
   * @throws \Exception
   */
  protected function newClientWithSettings(bool $will_return): ObjectProphecy {
    $client = $this->prophesize(ContentHubClient::class);
    $settings = $this->prophesize(Settings::class);
    $settings->getUuid()->willReturn('some_uuid');
    $client->getSettings()->willReturn($settings->reveal());
    $client->isFeatured()->willReturn($will_return);
    $client->getResponse()->willReturn(new Response(200));
    return $client;
  }

}
