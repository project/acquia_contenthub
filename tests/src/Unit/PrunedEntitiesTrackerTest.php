<?php

namespace Drupal\Tests\acquia_contenthub\Unit;

use Drupal\acquia_contenthub\PrunedEntitiesTracker;
use Drupal\Component\Uuid\Php as Uuid;
use Drupal\Tests\UnitTestCase;

/**
 * Tests pruned entities tracker.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub\PrunedEntitiesTracker
 */
class PrunedEntitiesTrackerTest extends UnitTestCase {

  /**
   * Pruned entity uuids.
   *
   * @var array
   */
  protected $prunedEntities = [];

  /**
   * SUT.
   *
   * @var \Drupal\acquia_contenthub\PrunedEntitiesTracker
   */
  protected $prunedEntitiesTracker;

  /**
   * Uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->prunedEntitiesTracker = new PrunedEntitiesTracker();
    $this->uuidGenerator = new Uuid();
    for ($i = 0; $i < 9; $i++) {
      $uuid = $this->uuidGenerator->generate();
      $this->prunedEntities[] = $uuid;
    }
  }

  /**
   * @covers ::setPrunedEntities
   * @covers ::getPrunedEntities
   */
  public function testPrunedEntities(): void {
    $this->assertEmpty($this->prunedEntitiesTracker->getPrunedEntities());
    $this->prunedEntitiesTracker->setPrunedEntities($this->prunedEntities);
    $this->assertNotEmpty($this->prunedEntitiesTracker->getPrunedEntities());
    $this->assertEqualsCanonicalizing($this->prunedEntities, $this->prunedEntitiesTracker->getPrunedEntities());
  }

  /**
   * @covers ::isPruned
   */
  public function testIsEntityPruned(): void {
    $this->assertFalse($this->prunedEntitiesTracker->isPruned($this->prunedEntities[0]));
    $this->prunedEntitiesTracker->setPrunedEntities($this->prunedEntities);
    $this->assertTrue($this->prunedEntitiesTracker->isPruned($this->prunedEntities[0]));
    $this->assertFalse($this->prunedEntitiesTracker->isPruned($this->uuidGenerator->generate()));
  }

  /**
   * @covers ::addPrunedEntities
   */
  public function testAddPrunedEntities(): void {
    $uuid = $this->uuidGenerator->generate();
    $this->assertFalse($this->prunedEntitiesTracker->isPruned($uuid));
    $this->prunedEntitiesTracker->addPrunedEntities($uuid);
    $this->assertTrue($this->prunedEntitiesTracker->isPruned($uuid));
    $this->prunedEntitiesTracker->addPrunedEntities(...$this->prunedEntities);
    $this->assertEqualsCanonicalizing(array_merge([$uuid], $this->prunedEntities), $this->prunedEntitiesTracker->getPrunedEntities());
  }

  /**
   * @covers ::resetPrunedEntities
   */
  public function testResetPrunedEntities(): void {
    $this->prunedEntitiesTracker->setPrunedEntities($this->prunedEntities);
    $this->assertNotEmpty($this->prunedEntitiesTracker->getPrunedEntities());
    $this->prunedEntitiesTracker->resetPrunedEntities();
    $this->assertEmpty($this->prunedEntitiesTracker->getPrunedEntities());
  }

}
