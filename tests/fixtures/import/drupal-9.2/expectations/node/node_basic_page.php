<?php

/**
 * @file
 * Expectation for node page scenario.
 */

use Drupal\Tests\acquia_contenthub\Kernel\Stubs\CdfExpectations;

$data = [
  'uuid' => [
    'en' => [
      0 => [
        'value' => '211e243d-9dd5-42a9-a141-2c195c728c51',
      ],
    ],
  ],
  'langcode' => [
    'en' => [
      0 => [
        'value' => 'en',
      ],
    ],
  ],
  'type' => [
    'en' => [
      0 => [
        'target_id' => '011e7c45-c7ec-4726-ac81-d27eb1a700e3',
      ],
    ],
  ],
  'revision_timestamp' => [
    'en' => [
      0 => [
        'value' => '1702323915',
      ],
    ],
  ],
  'revision_uid' => [
    'en' => [
      0 => [
        'target_id' => 'e29f0d26-dd21-4bcc-be57-1f086a9bcff3',
      ],
    ],
  ],
  'revision_log' => [
    'en' => [],
  ],
  'status' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'title' => [
    'en' => [
      0 => [
        'value' => 'Test Optional config syndication',
      ],
    ],
  ],
  'uid' => [
    'en' => [
      0 => [
        'target_id' => 'e29f0d26-dd21-4bcc-be57-1f086a9bcff3',
      ],
    ],
  ],
  'created' => [
    'en' => [
      0 => [
        'value' => '1702282137',
      ],
    ],
  ],
  'changed' => [
    'en' => [
      0 => [
        'value' => '1545339655',
      ],
    ],
  ],
  'promote' => [
    'en' => [
      0 => [
        'value' => '0',
      ],
    ],
  ],
  'sticky' => [
    'en' => [
      0 => [
        'value' => '0',
      ],
    ],
  ],
  'default_langcode' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'revision_default' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'revision_translation_affected' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'moderation_state' => [
    'en' => [
      0 => [
        'value' => 'published',
      ],
    ],
  ],
  'body' => [
    'en' => [
      0 => [
        'value' => "<p>test</p>",
        'summary' => '',
        'format' => 'basic_html',
      ],
    ],
  ],
];

$expectations = [
  '211e243d-9dd5-42a9-a141-2c195c728c51' => new CdfExpectations($data, [
    'nid', 'vid', 'changed', 'path',
  ]),
];

return $expectations;
