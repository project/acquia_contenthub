<?php

/**
 * @file
 * Media file expectation.
 */

use Drupal\Tests\acquia_contenthub\Kernel\Stubs\CdfExpectations;

$expectations = [];

$data = [
  'uuid' => [
    'en' => [
      0 => [
        'value' => '130c8c1d-e83e-4990-8ccd-41eb316b3d43',
      ],
    ],
  ],
  'langcode' => [
    'en' => [
      0 => [
        'value' => 'en',
      ],
    ],
  ],
  'uid' => [
    'en' => [
      0 => [
        'target_id' => 'c07e5269-91d0-4a1b-a31e-b0dcea8fcbaf',
      ],
    ],
  ],
  'bundle' => [
    'en' => [
      0 => [
        'target_id' => 'b85f27e6-21da-42f3-96a6-f0ceabb1b0d0',
      ],
    ],
  ],
  'status' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'name' => [
    'en' => [
      0 => [
        'value' => 'druplicon.png',
      ],
    ],
  ],
  'thumbnail' => [
    'en' => [
      0 => [
        'target_id' => '7e67c03a-24b1-4e38-892b-17405d1cbbd0',
        'alt' => 'Thumbnail',
        'title' => NULL,
        'width' => '88',
        'height' => '100',
      ],
    ],
  ],
  'created' => [
    'en' => [
      0 => [
        'value' => '1675431946',
      ],
    ],
  ],
  'changed' => [
    'en' => [
      0 => [
        'value' => '1675432135',
      ],
    ],
  ],
  'default_langcode' => [
    'en' => [
      0 => [
        'value' => 1,
      ],
    ],
  ],
  'revision_default' => [
    'en' => [
      0 => [
        'value' => 1,
      ],
    ],
  ],
  'revision_translation_affected' => [
    'en' => [
      0 => [
        'value' => 1,
      ],
    ],
  ],
  'field_media_image' => [
    'en' => [
      0 => [
        'target_id' => '7e67c03a-24b1-4e38-892b-17405d1cbbd0',
        'alt' => 'Thumbnail',
        'title' => '',
        'width' => '88',
        'height' => '100',
      ],
    ],
  ],
  'revision_user' => [
    'en' => [
      0 => [
        'target_id' => 'c07e5269-91d0-4a1b-a31e-b0dcea8fcbaf',
      ],
    ],
  ],
];

// 'revision_created' changes dynamically. Skip this field.
$expectations['130c8c1d-e83e-4990-8ccd-41eb316b3d43'] = new CdfExpectations($data, [
  'revision_created',
  'mid',
  'vid',
  'path',
]);

return $expectations;
