<?php

/**
 * @file
 * Media file expectation.
 */

use Drupal\Tests\acquia_contenthub\Kernel\Stubs\CdfExpectations;

$data = [
  'uuid' => [
    'en' => [
      0 => [
        'value' => '7e67c03a-24b1-4e38-892b-17405d1cbbd0',
      ],
    ],
  ],
  'langcode' => [
    'en' => [
      0 => [
        'value' => 'en',
      ],
    ],
  ],
  'uid' => [
    'en' => [
      0 => [
        'target_id' => 'c07e5269-91d0-4a1b-a31e-b0dcea8fcbaf',
      ],
    ],
  ],
  'filename' => [
    'en' => [
      0 => [
        'value' => 'druplicon.png',
      ],
    ],
  ],
  'uri' => [
    'en' => [
      0 => [
        'value' => 'public://2023-02/druplicon.png',
      ],
    ],
  ],
  'filemime' => [
    'en' => [
      0 => [
        'value' => 'image/png',
      ],
    ],
  ],
  'filesize' => [
    'en' => [
      0 => [
        'value' => '3905',
      ],
    ],
  ],
  'status' => [
    'en' => [
      0 => [
        'value' => '1',
      ],
    ],
  ],
  'created' => [
    'en' => [
      0 => [
        'value' => '1675432014',
      ],
    ],
  ],
  'changed' => [
    'en' => [
      0 => [
        'value' => '1675432135',
      ],
    ],
  ],
];

$expectations = ['7e67c03a-24b1-4e38-892b-17405d1cbbd0' => new CdfExpectations($data, ['fid'])];

return $expectations;
