#!/usr/bin/env bash

set -ev

cd "$(dirname "$0")" || exit; source _includes.sh

${orca_bin}/ci/before_install.sh
exit_code=$?
[[ "$exit_code" != 0 ]] && exit $exit_code

source ${orca_includes}

## If ORCA version is overridden then use that.
#if [ "$ORCA_VERSION" != "$ORCA_VERSION_OVERRIDE" ]; then
#  # Remove ORCA if it is already installed and install appropriate version of
#  # ORCA using the ORCA_VERSION_OVERRIDE environment variable. This allows
#  # us to change the ORCA version at runtime.
#  rm -rf ../orca
#  composer create-project --no-dev --ignore-platform-req=php acquia/orca ../orca "$ORCA_VERSION_OVERRIDE"
#fi

if [[ "$ORCA_JOB" == "STATIC_CODE_ANALYSIS" ]]; then
  rm -Rf "$ORCA_SUT_DIR/modules/acquia_contenthub_curation/ember"
  rm -Rf "$ORCA_SUT_DIR/modules/acquia_contenthub_dashboard/dashboard"
fi
if [[ "$ORCA_JOB" == "STATIC_CODE_ANALYSIS" && "$ACQUIACI" == TRUE ]]; then
  rm -Rf "/ramfs$ORCA_SUT_DIR/modules/acquia_contenthub_curation/ember"
  rm -Rf "/ramfs$ORCA_SUT_DIR/modules/acquia_contenthub_dashboard/dashboard"
fi

if [[ "$ORCA_JOB" == "ISOLATED_TEST_ON_CURRENT" || "$ORCA_JOB" == "ISOLATED_TEST_ON_NEXT_MAJOR_LATEST_MINOR_DEV" || "$ORCA_JOB" == "INTEGRATED_TEST_ON_CURRENT" || "$ORCA_JOB" == "ISOLATED_TEST_ON_CURRENT_DEV" || "$ORCA_JOB" == "INTEGRATED_TEST_ON_CURRENT_DEV" ]]; then
  # Removes drupal/webform from composer.json because it is not D11 compatible.
  jq 'del(.["require-dev"]["drupal/webform"])' "/ramfs$ORCA_SUT_DIR/composer.json" > removed_webform.json && mv removed_webform.json "/ramfs$ORCA_SUT_DIR/composer.json"
fi
