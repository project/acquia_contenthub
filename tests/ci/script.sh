#!/usr/bin/env bash

cd "$(dirname "$0")" || exit; source _includes.sh

# Extend _run().
# Exit codes are handled after the scripts were executed.
_run() {
  source ${orca_includes}

  if [[ "$ORCA_JOB" == "LOOSE_DEPRECATED_CODE_SCAN" ]]; then
    phpstan analyse /ramfs/acquia/acquia_contenthub -c /ramfs/acquia/acquia_contenthub/phpstan.neon
    return $?
  fi
}

${orca_bin}/ci/script.sh
exit_code=$?

if [[ "$exit_code" == 0 ]]; then
  _run
  exit_code=$?
fi

# Handle exit codes, factor in ALLOWED_FAILURE.
if [[ "$exit_code" == 0 ]]; then
  echo 'Executing after success script...'
  ${orca_bin}/ci/after_success.sh
  exit 0
fi

echo 'Executing after failure script...'
${orca_bin}/ci/after_failure.sh

if [[ "$ALLOWED_FAILURE" == 'true' ]]; then
  exit_code=0
fi

exit $exit_code
