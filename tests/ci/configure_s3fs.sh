#!/usr/bin/env bash

# Specific case to support s3fs.

set -ev

cd "$(dirname "$0")" || exit; source _includes.sh

source ${orca_includes}

[[ -d ${ORCA_FIXTURE_DIR} ]] && cd ${ORCA_FIXTURE_DIR}

drupal_version=$(get_drupal_version_using_composer)
echo "Drupal version: $drupal_version"

if [[ -z "${drupal_version}" ]]; then
  echo "No fixtures generated; exiting."
  exit 0
fi

composer require drupal/s3fs:3.x-dev@dev -W
# Enable if there's an active Drupal installation available
/ramfs/acquia/orca-build/vendor/bin/drush pm:enable s3fs acquia_contenthub_s3 2> /dev/null || echo 'S3fs and acquia_contenthub_s3 have not been enabled!'
