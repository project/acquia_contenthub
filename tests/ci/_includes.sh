#!/usr/bin/env bash

# Includes useful for all scripts.

orca_bin=../../../orca/bin
orca_includes=${orca_bin}/ci/_includes.sh

# Only after installation of packages.
get_drupal_version_using_composer() {
  composer show drupal/core --format json | jq '.versions[0]' -r
}
