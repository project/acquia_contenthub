#!/usr/bin/env bash

cd "$(dirname "$0")" || exit; source _includes.sh

source ${orca_includes}

if [[ "$ORCA_COVERAGE_ENABLE" = true ]]; then
  cd /ramfs${CI_WORKSPACE}
  php .acquia/fixup-xml-paths.php /acquia/acquia_contenthub/clover.xml
  php .acquia/fixup-xml-paths.php /acquia/acquia_contenthub/junit.xml
fi
