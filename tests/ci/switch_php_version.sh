#!/usr/bin/env bash

cd "$(dirname "$0")" || exit; source _includes.sh

_to_83() {
  echo 'http://dl-cdn.alpinelinux.org/alpine/v3.19/community' >> /etc/apk/repositories
  echo 'http://dl-cdn.alpinelinux.org/alpine/v3.19/main' >> /etc/apk/repositories

  # Download php83
  apk update && apk upgrade && apk add php83 php83-cli php83-common \
     php83-zip php83-gd php83-mbstring php83-tokenizer \
     php83-curl php83-bcmath php83-xml \
     php83-intl php83-sqlite3 php83-mysqli php83-dev \
     php83-gmp php83-soap php83-sockets \
     php83-phar php83-dom php83-xmlwriter php83-pdo php83-simplexml php83-ctype \
     php83-session php83-pdo_sqlite libcurl php83-pecl-apcu \
     mysql mysql-client sqlite php83-pdo_mysql php83-posix

  apk fix musl

  update-alternatives --install /usr/local/bin/php php /usr/bin/php83 82
  update-alternatives --set php /usr/bin/php83
  update-alternatives --force --all

  # Updating php-config.
  update-alternatives --install /usr/local/bin/php-config php-config /usr/bin/php-config83 82
  update-alternatives --set php-config /usr/bin/php-config83
  update-alternatives --force --all

  # Updating phpize.
  update-alternatives --install /usr/local/bin/phpize phpize /usr/bin/phpize83 82
  update-alternatives --set phpize /usr/bin/phpize83
  update-alternatives --force --all

  if [ "$ORCA_COVERAGE_ENABLE" = "TRUE" ]; then
    # Installing xdebug.
      pecl install xdebug

    # Adding Configuration
     docker-php-ext-enable xdebug
     echo "zend_extension=/usr/lib/php83/modules/xdebug.so" >> /etc/php83/php.ini
     echo xdebug.mode=coverage > /usr/local/etc/php/conf.d/xdebug.ini
  fi

  apk update && apk upgrade \
    && apk add --no-cache --virtual .build-deps --update linux-headers \
    $PHPIZE_DEPS \
    lsb-release \
    wget \
    unzip \
    gpg \
    libgd \
    bash \
    jq \
    git \
    ca-certificates

  apk del php81-sodium libsodium -r
  apk cache clean
  apk update && apk upgrade && apk add php83-sodium

  echo 'memory_limit = 2048M' >> /etc/php83/php.ini
}

_to_82() {
  update-alternatives --install /usr/local/bin/php php /usr/bin/php8.2 80
}

_to_74() {
  update-alternatives --install /usr/local/bin/php php /usr/bin/php7.4 81
  update-alternatives --set php /usr/bin/php7.4
}

case "$JENKINS_PHP_VERSION" in
  '8.2')
    _to_82
  ;;
  '7.4')
    _to_74
  ;;
  '8.3')
    _to_83
esac

php -v
