<?php

namespace Drupal\acquia_contenthub_test\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Example entity implementation for non-translatable, 'und' langcode entities.
 *
 * @ContentEntityType(
 *   id = "custom_entity_wo_langcode",
 *   label = @Translation("Custom Entity Without Langcode"),
 *   base_table = "custom_entity_wo_langcode",
 *   data_table = "custom_entity_wo_langcode_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   }
 * )
 */
class EntityWithoutLangcode extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);
    return $fields;
  }

}
