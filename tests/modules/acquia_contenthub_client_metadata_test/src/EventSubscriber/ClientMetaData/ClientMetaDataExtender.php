<?php

namespace Drupal\acquia_contenthub_client_metadata_test\EventSubscriber\ClientMetaData;

use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\ClientMetaDataEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Additional event subscriber that extends client metadata config.
 */
class ClientMetaDataExtender implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AcquiaContentHubEvents::CLIENT_METADATA] =
      ['extendClientMetaData', 100];
    return $events;
  }

  /**
   * Extends client metadata config and adds api key to the config.
   *
   * @param \Drupal\acquia_contenthub\Event\ClientMetaDataEvent $event
   *   Client metadata event.
   */
  public function extendClientMetaData(ClientMetaDataEvent $event): void {
    $additional_config = ['api_key' => 'client-api-key'];
    $event->setAdditionalConfig($additional_config);
    $event->stopPropagation();
  }

}
