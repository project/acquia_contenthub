<?php

namespace Drupal\acquia_contenthub;

/**
 * Tracks pruned entities that were removed during import.
 *
 * This service can be used by event subscribers
 * that tend to prune/tamper the CDF and want to
 * manipulate the pruned entities list. In CH space,
 * it's used by EntityCdfSerializer to set the initial pruning list,
 * however it can be used by EntityDataTamper event subscribers as well.
 */
class PrunedEntitiesTracker {

  /**
   * Entities that were pruned from CDF.
   *
   * @var array
   */
  protected $prunedEntities = [];

  /**
   * Sets pruned entities.
   *
   * @param array $pruned_entities
   *   Array of pruned entity uuids.
   */
  public function setPrunedEntities(array $pruned_entities): void {
    $this->prunedEntities = $pruned_entities;
  }

  /**
   * Adds entities as pruned.
   *
   * @param string ...$pruned_entities
   *   Entities list to be added as pruned.
   */
  public function addPrunedEntities(string ...$pruned_entities): void {
    $this->prunedEntities = array_merge($this->prunedEntities, $pruned_entities);
  }

  /**
   * Returns pruned entities list.
   *
   * @return array
   *   Pruned enities list.
   */
  public function getPrunedEntities(): array {
    return $this->prunedEntities;
  }

  /**
   * Checks if entity has been pruned.
   *
   * @param string $uuid
   *   Entity uuid.
   *
   * @return bool
   *   True if pruned else false.
   */
  public function isPruned(string $uuid): bool {
    return in_array($uuid, $this->prunedEntities, TRUE);
  }

  /**
   * Resets the pruned entities.
   */
  public function resetPrunedEntities(): void {
    $this->prunedEntities = [];
  }

}
