<?php

namespace Drupal\acquia_contenthub\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Client metadata event.
 */
class ClientMetaDataEvent extends Event {

  /**
   * Additional client metadata config that can be sent to content hub service.
   *
   * @var array
   */
  private array $additionalConfig = [];

  /**
   * Currently parsed config which can be used by event subscribers.
   *
   * For additional parsing.
   *
   * @var array
   */
  private array $readOnlyConfig;

  /**
   * ClientMetaDataEvent constructor.
   *
   * @param array $read_only_config
   *   Read only config that is already parsed.
   */
  public function __construct(array $read_only_config) {
    $this->readOnlyConfig = $read_only_config;
  }

  /**
   * Returns read only config.
   *
   * @return array
   *   Read only config.
   */
  public function getReadOnlyConfig(): array {
    return $this->readOnlyConfig;
  }

  /**
   * Returns additional config.
   *
   * @return array
   *   Additional config.
   */
  public function getAdditionalConfig(): array {
    return $this->additionalConfig;
  }

  /**
   * Merges and sets additional config.
   *
   * @param array $additional_config
   *   Additional config.
   */
  public function setAdditionalConfig(array $additional_config): void {
    $this->additionalConfig = array_merge($this->additionalConfig, $additional_config);
  }

}
