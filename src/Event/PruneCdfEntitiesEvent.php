<?php

namespace Drupal\acquia_contenthub\Event;

use Acquia\ContentHubClient\CDFDocument;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Use to remove entities a subscriber does not want to import.
 *
 * This event has some technical ramifications and requires a deep
 * understanding of your dependencies. Removal of stand alone entities within
 * the CDF object is completely ok and normal with this process, but is seldom
 * necessary. If you are looking to replace an item slated for import with an
 * existing local entity, look at the EntityDataTamperEvent instead.
 */
class PruneCdfEntitiesEvent extends Event {

  /**
   * The CDF object.
   *
   * @var \Acquia\ContentHubClient\CDFDocument
   */
  protected $cdf;

  /**
   * Original Cdf document.
   *
   * @var \Acquia\ContentHubClient\CDFDocument
   */
  protected $originalCdf;

  /**
   * PruneCdfEntitiesEvent constructor.
   *
   * @param \Acquia\ContentHubClient\CDFDocument $cdf
   *   The CDF document.
   */
  public function __construct(CDFDocument $cdf) {
    $this->cdf = $cdf;
    $this->originalCdf = clone $cdf;
  }

  /**
   * The CDF Objects to consider pruning.
   *
   * @return \Acquia\ContentHubClient\CDFDocument
   *   The CDF document.
   */
  public function getCdf() {
    return $this->cdf;
  }

  /**
   * Get original CDF document before pruning.
   *
   * @return \Acquia\ContentHubClient\CDFDocument
   *   Original CDF document.
   */
  public function getOriginalCdf(): CDFDocument {
    return $this->originalCdf;
  }

  /**
   * Get uuids of entities those were pruned.
   *
   * @return array
   *   List of pruned entity uuids.
   */
  public function getPrunedEntities(): array {
    return array_diff(array_keys($this->originalCdf->getEntities()), array_keys($this->cdf->getEntities()));
  }

  /**
   * The list of pruned CDF objects.
   *
   * @param \Acquia\ContentHubClient\CDFDocument $cdf
   *   The CDF document.
   */
  public function setCdf(CDFDocument $cdf) {
    $this->cdf = $cdf;
  }

}
