<?php

namespace Drupal\acquia_contenthub\Event\Queue;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event is fired right after a queue item has been processed.
 *
 * @see \Drupal\acquia_contenthub\AcquiaContentHubEvents::QUEUE_ITEM_PROCESS_FINISHED
 */
class QueueItemProcessFinishedEvent extends Event {

  /**
   * The name of the queue.
   *
   * @var string
   */
  private $queueName;

  /**
   * The contents of the item in hand.
   *
   * @var array
   */
  private $itemContent;

  /**
   * The result of the process if there was any.
   *
   * @var mixed
   */
  private $result;

  /**
   * Constructs a new instance of this class.
   *
   * @param string $queue_name
   *   The import or export queue name.
   * @param array $item_content
   *   The contents of 1 item.
   * @param mixed $result
   *   The result of the run.
   */
  public function __construct(string $queue_name, array $item_content, $result) {
    $this->queueName = $queue_name;
    $this->itemContent = $item_content;
    $this->result = $result;
  }

  /**
   * Returns the queue name.
   *
   * @return string
   *   The queue name.
   */
  public function getQueueName(): string {
    return $this->queueName;
  }

  /**
   * Returns the contents of the item in hand.
   *
   * @return array
   *   The contents of the item.
   */
  public function getItemContent(): array {
    return $this->itemContent;
  }

  /**
   * Returns the result of the queue item process.
   *
   * @return mixed
   *   The result of the process.
   */
  public function getResult() {
    return $this->result;
  }

}
