<?php

namespace Drupal\acquia_contenthub\EventSubscriber\Cdf;

use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\CreateCdfEntityEvent;
use Drupal\acquia_contenthub\Plugin\FileSchemeHandler\FileSchemeHandlerManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Manipulates file content entity CDF representation to better support files.
 */
class FileEntityHandler implements EventSubscriberInterface {

  /**
   * The file scheme handler manager.
   *
   * @var \Drupal\acquia_contenthub\Plugin\FileSchemeHandler\FileSchemeHandlerManagerInterface
   */
  protected $manager;

  /**
   * FileEntityHandler constructor.
   *
   * @param \Drupal\acquia_contenthub\Plugin\FileSchemeHandler\FileSchemeHandlerManagerInterface $manager
   *   File scheme handler manager.
   */
  public function __construct(FileSchemeHandlerManagerInterface $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AcquiaContentHubEvents::CREATE_CDF_OBJECT][] = ['onCreateCdf', 90];
    return $events;
  }

  /**
   * Add attributes to file entity CDF representations.
   *
   * @param \Drupal\acquia_contenthub\Event\CreateCdfEntityEvent $event
   *   The create CDF entity event.
   */
  public function onCreateCdf(CreateCdfEntityEvent $event) {
    if ($event->getEntity()->getEntityTypeId() == 'file') {
      /** @var \Drupal\file\FileInterface $entity */
      $entity = $event->getEntity();
      $handler = $this->manager->getHandlerForFile($entity);
      $handler->addAttributes($event->getCdf($entity->uuid()), $entity);
    }
  }

}
