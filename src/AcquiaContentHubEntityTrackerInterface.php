<?php

namespace Drupal\acquia_contenthub;

/**
 * Represents abstraction layer of entity tracker.
 */
interface AcquiaContentHubEntityTrackerInterface {

  /**
   * Indicates that a tracked item is to be re-imported or re-exported.
   */
  public const QUEUED = 'queued';

  /**
   * Delete rows based on given field and value.
   *
   * @param string $field_name
   *   The field used in filter condition.
   * @param string $field_value
   *   The dynamic field value for which to remove tracking.
   */
  public function delete(string $field_name, string $field_value): void;

  /**
   * Obtains a list of tracked entities.
   *
   * @param string|array $status
   *   The status of the entities to list or an array of statuses.
   * @param string $entity_type_id
   *   The Entity type.
   *
   * @return array
   *   An array of Tracked Entities set to reindex.
   */
  public function listTrackedEntities($status, string $entity_type_id = ''): array;

  /**
   * Nullifies hashes in the Tracker.
   *
   * @param array $statuses
   *   An array of status.
   * @param array $entity_types
   *   An array of entity types.
   * @param array $uuids
   *   An array of Entity UUIDs.
   */
  public function nullifyHashes(array $statuses = [], array $entity_types = [], array $uuids = []): void;

}
