<?php

namespace Drupal\acquia_contenthub\Client;

use Acquia\ContentHubClient\MetaData\ClientMetaData;
use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\ClientMetaDataEvent;
use Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\AchVersionAttribute;
use Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\ValidSslAttribute;
use Drupal\acquia_contenthub\PubSubModuleStatusChecker;
use Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface;
use Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Client metadata builder.
 */
class ClientMetaDataBuilder {

  /**
   * Client type.
   */
  public const CLIENT_TYPE = 'drupal';

  /**
   * Pub Sub module status checker.
   *
   * @var \Drupal\acquia_contenthub\PubSubModuleStatusChecker
   */
  protected PubSubModuleStatusChecker $moduleStatusChecker;

  /**
   * Ach version handler.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\AchVersionAttribute
   */
  protected AchVersionAttribute $achVersionHandler;

  /**
   * Valid Ssl Handler.
   *
   * @var \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\ValidSslAttribute
   */
  protected ValidSslAttribute $validSslHandler;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $dispatcher;

  /**
   * Content Hub connection details.
   *
   * @var \Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface
   */
  protected ConnectionDetailsInterface $achConnectionDetails;

  /**
   * ClientMetaDataBuilder constructor.
   *
   * @param \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\AchVersionAttribute $ach_version_handler
   *   Ach version handler.
   * @param \Drupal\acquia_contenthub\EventSubscriber\CdfAttributes\ValidSslAttribute $valid_ssl_handler
   *   Valid ssl handler.
   * @param \Drupal\acquia_contenthub\PubSubModuleStatusChecker $module_status_checker
   *   Pub Sub module status checker.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher
   *   Event dispatcher.
   * @param \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configurations
   *   CH configurations.
   */
  public function __construct(AchVersionAttribute $ach_version_handler, ValidSslAttribute $valid_ssl_handler, PubSubModuleStatusChecker $module_status_checker, EventDispatcherInterface $dispatcher, ContentHubConfigurationInterface $ach_configurations) {
    $this->achVersionHandler = $ach_version_handler;
    $this->validSslHandler = $valid_ssl_handler;
    $this->moduleStatusChecker = $module_status_checker;
    $this->dispatcher = $dispatcher;
    $this->achConnectionDetails = $ach_configurations->getConnectionDetails();
  }

  /**
   * Builds and returns client metadata.
   *
   * @return \Acquia\ContentHubClient\MetaData\ClientMetaData
   *   Client metadata.
   *
   * @throws \Exception
   */
  public function buildClientMetadata(): ClientMetaData {
    $valid_ssl = $this->validSslHandler->getValidStatusForSite();
    $ach_version = $this->achVersionHandler->getAchVersion();
    $is_publisher = $this->moduleStatusChecker->isPublisher();
    $is_subscriber = $this->moduleStatusChecker->isSubscriber();
    $webhook_version = $this->achConnectionDetails->shouldUseSingleEntityPayload() ? '1.0' : '2.0';
    $default_config = [
      'valid_ssl' => $valid_ssl,
      'ch_version' => $ach_version,
      'drupal_version' => \Drupal::VERSION,
    ];
    $event = new ClientMetaDataEvent($default_config);
    $this->dispatcher->dispatch($event, AcquiaContentHubEvents::CLIENT_METADATA);
    $final_config = array_merge($default_config, $event->getAdditionalConfig());

    return new ClientMetadata(
      self::CLIENT_TYPE,
      $is_publisher,
      $is_subscriber,
      $webhook_version,
      $final_config
    );
  }

}
