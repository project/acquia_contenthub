<?php

namespace Drupal\acquia_contenthub\Client;

/**
 * Client data manager interface.
 */
interface ClientMetaDataManagerInterface {

  /**
   * Updates client metadata if it has changed from last updated values.
   *
   * @return bool
   *   TRUE if metadata is updated, else FALSE in any other conditions.
   */
  public function updateClientMetaData(): bool;

  /**
   * Retrieves remote client metadata from cache if available.
   *
   * Otherwise, fetches it from remote and caches it.
   *
   * @param string $uuid
   *   Client uuid.
   *
   * @return array
   *   Return remote metadata.
   */
  public function getRemoteMetaData(string $uuid): array;

}
