<?php

namespace Drupal\acquia_contenthub;

/**
 * Defines the behaviour of a re-queuer.
 */
interface RequeuerInterface {

  /**
   * Re enqueues the entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle name.
   * @param string $uuid
   *   The entity uuid.
   * @param bool $only_queued_entities
   *   True value if only queued entities to be re-enqueued.
   * @param bool $use_tracking_table
   *   True value if only tracking table entities to be re-enqueued.
   *
   * @throws \Exception
   */
  public function reQueue(string $entity_type, string $bundle, string $uuid, bool $only_queued_entities, bool $use_tracking_table): void;

}
