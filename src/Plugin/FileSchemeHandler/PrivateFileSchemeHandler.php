<?php

namespace Drupal\acquia_contenthub\Plugin\FileSchemeHandler;

use Acquia\ContentHubClient\CDF\CDFObject;
use Acquia\ContentHubClient\CDFAttribute;
use Drupal\acquia_contenthub\ContentHubCommonActions;
use Drupal\acquia_contenthub\Exception\ContentHubFileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * File scheme handler for private files.
 *
 * @FileSchemeHandler(
 *   id = "private",
 *   label = @Translation("Private file handler")
 * )
 */
class PrivateFileSchemeHandler extends PluginBase implements FileSchemeHandlerInterface, ContainerFactoryPluginInterface {

  use FileHandlerTrait;

  /**
   * Common actions for Content Hub.
   *
   * @var \Drupal\acquia_contenthub\ContentHubCommonActions
   */
  private $contentHubCommonActions;

  /**
   * PrivateFileSchemeHandler constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The definition.
   * @param \Drupal\acquia_contenthub\ContentHubCommonActions $common_actions
   *   Content Hub Common Actions.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ContentHubCommonActions $common_actions) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->contentHubCommonActions = $common_actions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('acquia_contenthub_common_actions')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function addAttributes(CDFObject $object, FileInterface $file) {
    $uri = $file->getFileUri();
    $webhook_url = Url::fromUri('internal:/acquia-contenthub/webhook', ['absolute' => TRUE])->toString();
    $object->addAttribute('file_scheme', CDFAttribute::TYPE_STRING, 'private');
    $object->addAttribute('file_location', CDFAttribute::TYPE_STRING, $webhook_url);
    $object->addAttribute('file_uri', CDFAttribute::TYPE_STRING, $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function getFile(CDFObject $object) {
    $this->validateAttributes($object);
    $url = $object->getAttribute('file_location')->getValue()[LanguageInterface::LANGCODE_NOT_SPECIFIED];
    $uri = $object->getAttribute('file_uri')->getValue()[LanguageInterface::LANGCODE_NOT_SPECIFIED];
    $uuid = $object->getUuid();
    $this->validateDirectory($uri, $object->getUuid());

    try {
      $contents = $this->contentHubCommonActions->requestRemoteEntity($url, $uri, $uuid, 'private');
    }
    catch (\Exception $exception) {
      // Transform exception into a more specific file exception.
      $e = new ContentHubFileException(
        'unable to request file from remote source: ' . $exception->getMessage(),
        ContentHubFileException::REMOTE_REQUEST,
        $uuid,
      );
      $e->setResourceUri($url);
      throw $e;
    }

    return $this->saveData($contents, $uri, FileSystemInterface::EXISTS_REPLACE);
  }

}
