<?php

namespace Drupal\acquia_contenthub\Libs\InterestList;

/**
 * Defines behaviour for interest list storage.
 */
interface InterestListStorageInterface {

  /**
   * Provides interest list if cached otherwise fetches from CH service.
   *
   * @param string $webhook_uuid
   *   Webhook uuid.
   * @param string $site_role
   *   Site role.
   * @param array $query
   *   Query params. Accepts disable_syndication, size, from and uuids.
   *
   * @code
   *   disable_syndication: boolean.
   *     Filter for disabled entities.
   *     If set to true, only disabled entities will be returned.
   *     If set to false, only enabled entities will be returned.
   *     If not set, all the entities will be returned.
   *   size: integer.
   *     Size of the interest list items to return.
   *     Max is 3000. Min is 1.
   *   from: integer.
   *     This is the offset value.
   *     Must be positive integer.
   *   uuids: array.
   *     Max 3000 uuids can be passed.
   * @endcode
   *
   * @return array
   *   An associate array keyed by the entity uuid.
   *
   * @throws \Drupal\acquia_contenthub\Exception\ContentHubClientException
   * @throws \Drupal\acquia_contenthub\Exception\ContentHubException
   */
  public function getInterestList(string $webhook_uuid, string $site_role, array $query = []): array;

  /**
   * Cleans up cached interest list.
   */
  public function resetCache(): void;

}
