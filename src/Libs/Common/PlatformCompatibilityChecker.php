<?php

namespace Drupal\acquia_contenthub\Libs\Common;

use Acquia\ContentHubClient\ContentHubClient;
use Drupal\acquia_contenthub\Exception\ContentHubClientException;
use Drupal\acquia_contenthub\Exception\PlatformIncompatibilityException;
use Drupal\acquia_contenthub\Exception\ServiceUnderMaintenanceException;
use Drupal\acquia_contenthub\Libs\Traits\ResponseCheckerTrait;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;

/**
 * Responsible for platform related compatibility checks.
 */
class PlatformCompatibilityChecker {

  use ResponseCheckerTrait;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The acquia_contenthub logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The acquia_contenthub cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Psr\Log\LoggerInterface $channel
   *   The acquia_contenthub logger channel.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The acquia_contenthub cache bin.
   */
  public function __construct(MessengerInterface $messenger, LoggerInterface $channel, CacheBackendInterface $cache) {
    $this->messenger = $messenger;
    $this->logger = $channel;
    $this->cache = $cache;
  }

  /**
   * Checks platform compatibility and returns an appropriate value.
   *
   * If the subscription supports new features provided by Content Hub,
   * the client is returned, otherwise the return value is NULL.
   *
   * @param \Acquia\ContentHubClient\ContentHubClient $client
   *   The registered client to check if the platform is compatible.
   *
   * @return \Acquia\ContentHubClient\ContentHubClient|null
   *   The ContentHubClient if the platform is compatible or null.
   *
   * @throws \Exception
   */
  public function intercept(ContentHubClient $client): ?ContentHubClient {
    try {
      $is_featured = $this->isFeatured($client);
    }
    catch (ContentHubClientException $e) {
      $this->messenger->addWarning(PlatformIncompatibilityException::$clientError);
      $this->logger->warning(PlatformIncompatibilityException::$clientError);
      return NULL;
    }
    catch (ServiceUnderMaintenanceException $e) {
      $this->messenger->addError($e->getMessage() . ' Please try to connect Content Hub later!');
      $this->logger->error($e->getMessage());
      $this->cache->set('ch_maintenance_mode', TRUE, time() + 300);
      return NULL;
    }
    if (!$is_featured) {
      $this->messenger->addWarning(PlatformIncompatibilityException::$incompatiblePlatform);
      $this->logger->warning(PlatformIncompatibilityException::$incompatiblePlatform);
      return NULL;
    }
    return $client;
  }

  /**
   * Checks the subscription and deletes the client if the check fails.
   *
   * @param \Acquia\ContentHubClient\ContentHubClient $client
   *   The registered client to check.
   *
   * @return \Acquia\ContentHubClient\ContentHubClient
   *   The client in case of successful check.
   *
   * @throws \Drupal\acquia_contenthub\Exception\PlatformIncompatibilityException
   */
  public function interceptAndDelete(ContentHubClient $client): ContentHubClient {
    if (!$this->isFeatured($client)) {
      $client->deleteClient($client->getSettings()->getUuid());
      throw new PlatformIncompatibilityException(
        PlatformIncompatibilityException::$incompatiblePlatform
      );
    }
    return $client;
  }

  /**
   * Returns whether the account is featured or not.
   *
   * The value is cached for an hour. If the account is featured the cache is
   * permanent.
   *
   * @param \Acquia\ContentHubClient\ContentHubClient $client
   *   The client to use for the check.
   *
   * @return bool
   *   Returns whether the account is featured or not.
   *
   * @throws \Exception
   */
  public function isFeatured(ContentHubClient $client): bool {
    $uuid = $client->getSettings()->getUuid();
    $is_featured = $this->cache->get($uuid);
    if ($is_featured) {
      return $is_featured->data;
    }

    $is_featured = $client->isFeatured();
    $response = $client->getResponse();
    if (!$this->isSuccessful($response)) {
      if ($this->isMaintenanceError($response)) {
        throw new ServiceUnderMaintenanceException("Platform error.");
      }

      throw new ContentHubClientException(
        PlatformIncompatibilityException::$clientError
      );
    }
    $ttl = $is_featured ? CacheBackendInterface::CACHE_PERMANENT : time() + 3600;
    $this->cache->set($uuid, (int) $is_featured, $ttl);

    return $is_featured;
  }

}
