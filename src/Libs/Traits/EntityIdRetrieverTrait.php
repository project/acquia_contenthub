<?php

namespace Drupal\acquia_contenthub\Libs\Traits;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Retrieves the entity id based on the config syndication settings.
 */
trait EntityIdRetrieverTrait {

  /**
   * Returns the appropriate identifier of the config entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return string
   *   The identifier, either uuid or id. ID if config syndication is disabled.
   */
  protected function getEntityIdentifier(EntityInterface $entity): string {
    if (!\Drupal::hasService('acquia_contenthub_publisher.config_syndication.settings') || !$entity instanceof ConfigEntityInterface) {
      return $entity->uuid();
    }

    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config */
    $config = \Drupal::service('acquia_contenthub_publisher.config_syndication.settings');
    return $config->isConfigSyndicationDisabled() ? $entity->id() : $entity->uuid();
  }

}
