<?php

namespace Drupal\acquia_contenthub;

/**
 * Defines the behaviour of a queue inspector.
 */
interface QueueInspectorInterface {

  /**
   * Fetch all content without limit.
   */
  public const NO_LIMIT = -1;

  /**
   * Provides items from specified queue.
   *
   * @param string $type
   *   The entity type.
   * @param string $uuid
   *   The entity uuid.
   * @param int $limit
   *   Limit of the queue items to be provided.
   *
   * @return array
   *   The list of queue items.
   */
  public function getQueueData(string $type = '', string $uuid = '', int $limit = self::NO_LIMIT): array;

  /**
   * Returns the applicable queue name.
   *
   * @return string
   *   The name of the queue.
   */
  public function getQueueName(): string;

}
