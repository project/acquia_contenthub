<?php

namespace Drupal\acquia_contenthub\Settings;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Contenthub config settings.
 */
class ConfigSettings implements ConfigSettingsInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * ConfigSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function enableContentHubUpdates(): void {
    $this->getEditableConfigs()->set(self::CONTENTHUB_UPDATES, TRUE)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function disableContentHubUpdates(): void {
    $this->getEditableConfigs()->set(self::CONTENTHUB_UPDATES, FALSE)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function shouldSendContentHubUpdates(): bool {
    return $this->getConfigs()->get(self::CONTENTHUB_UPDATES) ?? TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function enableClientCdfUpdates(): void {
    $this->getEditableConfigs()->set(self::CLIENT_CDF_UPDATES, TRUE)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function disableClientCdfUpdates(): void {
    $this->getEditableConfigs()->set(self::CLIENT_CDF_UPDATES, FALSE)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function shouldSendClientCdfUpdates(): bool {
    return $this->getConfigs()->get(self::CLIENT_CDF_UPDATES) ?? TRUE;
  }

  /**
   * Provides acquia contenthub admin settings immutable configurations.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Returns immutable CH admin settings configs.
   */
  private function getConfigs(): ImmutableConfig {
    return $this->configFactory->get(self::ACQUIA_CH_ADMIN_SETTINGS);
  }

  /**
   * Provides acquia contenthub admin settings editable configurations.
   *
   * @return \Drupal\Core\Config\Config
   *   Returns editable CH admin settings configs.
   */
  private function getEditableConfigs(): Config {
    return $this->configFactory->getEditable(self::ACQUIA_CH_ADMIN_SETTINGS);
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateContentHubSettings(): void {
    $this->configFactory->getEditable(self::ACQUIA_CH_ADMIN_SETTINGS)->delete();
  }

}
