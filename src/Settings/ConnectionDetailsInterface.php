<?php

namespace Drupal\acquia_contenthub\Settings;

/**
 * Content Hub connection details interface.
 *
 * Defines how to access the values of connection settings.
 */
interface ConnectionDetailsInterface {

  /**
   * Sets Hostname.
   *
   * @param string $hostname
   *   The hostname.
   */
  public function setHostname(string $hostname): void;

  /**
   * Provides Original value of the attribute.
   *
   * Without overrides.
   *
   * @param string $attribute
   *   Name of the attribute.
   *
   * @return string
   *   Value of the attribute.
   */
  public function getOriginal(string $attribute): string;

  /**
   * Provides Hostname.
   *
   * @return string
   *   Returns hostname.
   */
  public function getHostname(): string;

  /**
   * Sets Api key.
   *
   * @param string $api_key
   *   Api Key.
   */
  public function setApiKey(string $api_key): void;

  /**
   * Provides Api key.
   *
   * @return string
   *   Returns api key.
   */
  public function getApiKey(): string;

  /**
   * Sets secret key.
   *
   * @param string $secret_key
   *   Secret Key.
   */
  public function setSecretKey(string $secret_key): void;

  /**
   * Provides secret key.
   *
   * @return string
   *   Returns secret key.
   */
  public function getSecretKey(): string;

  /**
   * Sets client uuid.
   *
   * @param string $uuid
   *   The uuid.
   */
  public function setClientUuid(string $uuid): void;

  /**
   * Provides client uuid.
   *
   * @return string
   *   Returns client uuid.
   */
  public function getClientUuid(): string;

  /**
   * Sets client name.
   *
   * @param string $client_name
   *   Client name.
   */
  public function setClientName(string $client_name): void;

  /**
   * Provides client name.
   *
   * @return string
   *   Return client name.
   */
  public function getClientName(): string;

  /**
   * Sets shared secret key.
   *
   * @param string $shared_secret_key
   *   The shared secret key.
   */
  public function setSharedSecret(string $shared_secret_key): void;

  /**
   * Provides shared secret key.
   *
   * @return string
   *   Returns shared secret key.
   */
  public function getSharedSecret(): string;

  /**
   * Sets event service url.
   *
   * @param string $service_url
   *   Event service url.
   */
  public function setEventServiceUrl(string $service_url): void;

  /**
   * Provides event service url.
   *
   * @return string
   *   Returns event service url.
   */
  public function getEventServiceUrl(): string;

  /**
   * Checks if webook is suppressed or not.
   *
   * @return bool
   *   Returns true if suppressed flag is enabled.
   */
  public function isWebhookSuppressed(): bool;

  /**
   * Determines if overrides are applied to a key for CH configurations.
   *
   * @param string $key
   *   (optional) A string that maps to a key within the CH configuration data.
   *
   * @return bool
   *   TRUE if there are any overrides for the key, otherwise FALSE.
   */
  public function isOverridden(string $key = ''): bool;

  /**
   * Enables webhook suppression.
   */
  public function suppressWebhook(): void;

  /**
   * Disables webhook suppression.
   */
  public function unsuppressWebhook(): void;

  /**
   * Sets webhook data.
   *
   * @param array $webhook
   *   Webhook data.
   */
  public function setWebhook(array $webhook): void;

  /**
   * Returns the webhook UUID associated with this site.
   *
   * @param string $op
   *   Optional setting to grab the specific key for a webhook. Options are:
   *     * uuid: The UUID for the webhook.
   *     * url: The fully qualified URL that plexus accesses the site.
   *     * settings_url: The nice URL that users interact with.
   *
   * @return string
   *   The Webhook if set from connection settings.
   */
  public function getWebhook(string $op = 'settings_url'): string;

  /**
   * Sets webhook uuid.
   *
   * @param string $uuid
   *   Webhook uuid.
   */
  public function setWebhookUuid(string $uuid): void;

  /**
   * Toggles webhook_V1 .
   *
   * @param bool $enabled
   *   False by default. If set to true, Webhook_V1 will be enabled.
   */
  public function toggleWebhookV1Payload(bool $enabled): void;

  /**
   * Determines if single entity per webhook payload shall be used.
   *
   * @return bool
   *   True if single entity per webhook payload, else False.
   */
  public function shouldUseSingleEntityPayload(): bool;

  /**
   * Sets multiple configurations.
   *
   * @param array $data
   *   Array of configuration data. Example:
   *     [
   *        "client_name" => "example-client",
   *        "secret_key" => "0a1b2c3d4e5f6g7h",
   *        "api_key" => "h7g6f5e4d3c2b1a0",
   *      ].
   */
  public function setMultiple(array $data): void;

}
