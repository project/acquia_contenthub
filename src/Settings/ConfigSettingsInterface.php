<?php

namespace Drupal\acquia_contenthub\Settings;

/**
 * Content Hub config settings interface.
 *
 * Defines configuration that is specific to the site in hand and are stored
 * in Drupal config.
 */
interface ConfigSettingsInterface {

  /**
   * CH admin settings configurations.
   */
  public const ACQUIA_CH_ADMIN_SETTINGS = 'acquia_contenthub.admin_settings';

  /**
   * Contenthub updates flag.
   */
  public const CONTENTHUB_UPDATES = 'send_contenthub_updates';

  /**
   * CH client cdf updates flag.
   */
  public const CLIENT_CDF_UPDATES = 'send_clientcdf_updates';

  /**
   * Enables updates to Content Hub.
   *
   * This flag is active by default. Interest lists e.g would only get updated
   * if this flag is active.
   *
   * There are close to no reason to disable this flag.
   */
  public function enableContentHubUpdates(): void;

  /**
   * Disables updates to Content Hub.
   */
  public function disableContentHubUpdates(): void;

  /**
   * Checks if Content Hub updates are enabled or disabled.
   *
   * @return bool
   *   Returns value of Content Hub send updates flag.
   */
  public function shouldSendContentHubUpdates(): bool;

  /**
   * Enables client cdf updates.
   *
   * This flag is active by default. Client CDFs would only get updated and sent
   * to the service if this flag is on.
   *
   * There are close to no reason to disable this flag.
   */
  public function enableClientCdfUpdates(): void;

  /**
   * Disables client cdf updates.
   */
  public function disableClientCdfUpdates(): void;

  /**
   * Checks if client cdf updates are enabled or disabled.
   *
   * @return bool
   *   Returns value of client cdf updates flag.
   */
  public function shouldSendClientCdfUpdates(): bool;

  /**
   * Deletes acquia contenthub admin settings configurations.
   */
  public function invalidateContentHubSettings(): void;

}
