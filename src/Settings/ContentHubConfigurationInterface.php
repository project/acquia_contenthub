<?php

namespace Drupal\acquia_contenthub\Settings;

use Acquia\ContentHubClient\Settings;

/**
 * Interface for Content Hub configurations.
 *
 * A common interface which enables access to the local configuration and
 * connection settings of Content Hub.
 *
 * @see \Drupal\acquia_contenthub\Settings\ConfigSettingsInterface
 * @see \Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface
 */
interface ContentHubConfigurationInterface {

  /**
   * Provides connection details.
   *
   * @return ConnectionDetailsInterface
   *   CH connection details.
   */
  public function getConnectionDetails(): ConnectionDetailsInterface;

  /**
   * Provides configuration settings.
   *
   * @return ConfigSettingsInterface
   *   CH config settings.
   */
  public function getContentHubConfig(): ConfigSettingsInterface;

  /**
   * Returns a settings object containing CH credentials and other related info.
   *
   * @return \Acquia\ContentHubClient\Settings|null
   *   ContentHub Client settings.
   */
  public function getSettings(): ?Settings;

  /**
   * Gets the settings provider from the settings event for contenthub settings.
   *
   * @return string
   *   The name of settings provider.
   */
  public function getProvider(): string;

  /**
   * Verifies whether Content Hub has been configured or not.
   *
   * @return bool
   *   TRUE if configuration is set, FALSE otherwise.
   */
  public function isConfigurationSet(Settings $settings = NULL): bool;

}
