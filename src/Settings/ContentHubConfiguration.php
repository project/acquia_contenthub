<?php

namespace Drupal\acquia_contenthub\Settings;

use Acquia\ContentHubClient\Settings;
use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\AcquiaContentHubSettingsEvent;
use Drupal\Component\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Contenhub's configuration data.
 */
class ContentHubConfiguration implements ContentHubConfigurationInterface {

  /**
   * CH connection details.
   *
   * @var \Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface
   */
  protected ConnectionDetailsInterface $connectionDetails;

  /**
   * CH config settings.
   *
   * @var \Drupal\acquia_contenthub\Settings\ConfigSettingsInterface
   */
  protected ConfigSettingsInterface $configDetails;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $dispatcher;

  /**
   * Settings object.
   *
   * @var \Acquia\ContentHubClient\Settings|null
   */
  protected ?Settings $settings;

  /**
   * Settings Provider.
   *
   * @var string
   */
  protected string $settingsProvider;

  /**
   * ContentHubConfiguration constructor.
   *
   * @param \Drupal\acquia_contenthub\Settings\ConnectionDetailsInterface $connection_details
   *   CH connection details.
   * @param \Drupal\acquia_contenthub\Settings\ConfigSettingsInterface $config_details
   *   CH config settings.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   Event dispatcher.
   */
  public function __construct(ConnectionDetailsInterface $connection_details, ConfigSettingsInterface $config_details, EventDispatcherInterface $dispatcher) {
    $this->connectionDetails = $connection_details;
    $this->configDetails = $config_details;
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function getConnectionDetails(): ConnectionDetailsInterface {
    return $this->connectionDetails;
  }

  /**
   * {@inheritDoc}
   */
  public function getContentHubConfig(): ConfigSettingsInterface {
    return $this->configDetails;
  }

  /**
   * Call the event to populate contenthub settings.
   */
  private function initializeSettings(): void {
    $event = new AcquiaContentHubSettingsEvent();
    $this->dispatcher->dispatch($event, AcquiaContentHubEvents::GET_SETTINGS);
    $this->settings = $event->getSettings();
    $this->settingsProvider = $event->getProvider();
  }

  /**
   * {@inheritDoc}
   */
  public function getProvider(): string {
    $this->initializeSettings();
    return $this->settingsProvider;
  }

  /**
   * {@inheritDoc}
   */
  public function getSettings(): ?Settings {
    $this->initializeSettings();
    return $this->settings;
  }

  /**
   * {@inheritDoc}
   */
  public function isConfigurationSet(Settings $settings = NULL): bool {
    $settings = $settings ?? $this->getSettings();
    return $settings
      && Uuid::isValid($settings->getUuid())
      && $settings->getName()
      && $settings->getUrl()
      && $settings->getApiKey()
      && $settings->getSecretKey();
  }

}
