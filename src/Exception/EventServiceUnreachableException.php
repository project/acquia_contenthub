<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * An exception that occurs when event microservice is unreachable.
 */
class EventServiceUnreachableException extends \Exception {
}
