<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * Thrown in case of file related Content Hub errors.
 */
class ContentHubFileException extends \Exception {

  /**
   * Error codes related to file syndication.
   */
  public const FETCHING_ERROR = 1;
  public const MISSING_ATTRIBUTE = 2;
  public const DIRECTORY_NOT_WRITABLE = 3;
  public const REMOTE_REQUEST = 4;

  /**
   * The file url.
   *
   * @var string
   */
  protected $resourceUri = 'n/a';

  /**
   * The file entity uuid.
   *
   * @var string
   */
  protected $uuid;

  /**
   * Constructs this object.
   *
   * @param string $message
   *   The error message.
   * @param int $code
   *   The error code.
   * @param string $uuid
   *   The file entity uuid.
   * @param \Throwable|null $previous
   *   The previous exception in the stack.
   */
  public function __construct(string $message = "", int $code = 0, string $uuid = '', ?\Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
    $this->uuid = $uuid;
  }

  /**
   * Sets the fqn of the file.
   *
   * @param string $filename
   *   The filename.
   */
  public function setResourceUri(string $filename): void {
    $this->resourceUri = $filename;
  }

  /**
   * Returns the fqn of the file.
   *
   * @return string
   *   The uri.
   */
  public function getResourceUri(): string {
    return $this->resourceUri;
  }

}
