<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * Represents ContentHub Client is incorrect.
 */
class ContentHubClientException extends \Exception {

  /**
   * Represents CH client is improperly configured.
   */
  public const CLIENT_CONFIG_ERROR = 1;

}
