<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * Thrown on platform incompatibility.
 */
class PlatformIncompatibilityException extends \Exception {

  /**
   * General message of platform incompatibility error.
   *
   * @var string
   */
  public static string $incompatiblePlatform = 'This version of module is not compatible with the current Content Hub Service API. ' .
   'Please downgrade the module a major version or contact Acquia Support. ' .
   'The subscription is not yet enabled for extended features.';

  /**
   * An error happened when checking whether the client is available or not.
   *
   * @var string
   */
  public static string $clientError = 'An error occurred during platform compatibility check. ' .
    'We could not determine account readiness. ' .
    'Please check if the client is properly configured.';

}
