<?php

namespace Drupal\acquia_contenthub\Exception;

use Acquia\ContentHubClient\StatusCodes;

/**
 * Thrown when the service is under maintenance.
 */
class ServiceUnderMaintenanceException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct($message = "", ?\Throwable $previous = NULL) {
    $message = implode(" ", [$message, "Service is under maintenance."]);
    parent::__construct($message, StatusCodes::SERVICE_UNDER_MAINTENANCE, $previous);
  }

}
