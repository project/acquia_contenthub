<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * Represents invalid CDF exception cases.
 */
class InvalidCdfException extends \Exception {

  public const MISSING_ENTITIES_ENTRY = 200;

}
