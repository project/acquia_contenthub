<?php

namespace Drupal\acquia_contenthub\Exception;

/**
 * An exception that gets thrown.
 *
 * When a different site tries to delete an entity.
 */
class EntityOriginMismatchException extends \Exception {}
