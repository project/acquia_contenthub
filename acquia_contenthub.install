<?php

/**
 * @file
 * @defgroup updates-8.6.x-contenthub-configuration
 * Update functions to Configuration Entities.
 */

/**
 * Updates Content Hub legacy settings to 2.x version.
 *
 * @see acquia_contenthub_post_update_update_config_entities()
 */
function acquia_contenthub_update_82001() {
  $database = \Drupal::database();
  /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configurations */
  $ach_configurations = \Drupal::service('acquia_contenthub.configuration');
  if (!$database->schema()->tableExists('acquia_contenthub_entities_tracking')) {
    return;
  }
  $ch_connection = $ach_configurations->getConnectionDetails();

  // Updates Configuration Entity "acquia_contenthub.admin_settings".
  /** @var \Drupal\acquia_contenthub\Client\ClientFactory $client_factory */
  $client_factory = \Drupal::service('acquia_contenthub.client.factory');
  $settings = $ach_configurations->getSettings();
  $client = $client_factory->getClient($settings);
  $remote = $client->getRemoteSettings();
  $ch_connection->setSharedSecret($remote['shared_secret']);

  $state = \Drupal::state();

  // We will store Webhook URL and Webhook UUID in a state variable to be able
  // to delete the legacy config and make space for the new way of storing this
  // information.
  $state->set('acquia_contenthub_update_82001_webhook_uuid', $ch_connection->getWebhook('uuid'));
  $state->set('acquia_contenthub_update_82001_webhook_url', $ch_connection->getWebhook('url'));
  $webhook = [
    'settings_url' => $ch_connection->getWebhook(),
  ];
  $ch_connection->setWebhook($webhook);

  // Delete Configuration Entity "acquia_contenthub.entity_config".
  // We store these settings in a state variable to be used later by the
  // acquia_lift_publisher module to obtain information about what are the
  // legacy configured view displays to export as rendered entities.
  $entity_type_configuration = [];
  $config_factory = \Drupal::configFactory();
  foreach ($config_factory->listAll('acquia_contenthub.entity') as $contenthub_entity_type_config_name) {
    $contenthub_entity_type_config = $config_factory->getEditable($contenthub_entity_type_config_name);
    $entity_type_configuration[$contenthub_entity_type_config_name] = $contenthub_entity_type_config->getRawData();
    $contenthub_entity_type_config->delete();
  }
  $state->set('acquia_contenthub_update_82001_entity_type_config', $entity_type_configuration);
  $entity_config = $config_factory->getEditable('acquia_contenthub.entity_config');
  $state->set('acquia_contenthub_update_82001_entity_config', $entity_config->getRawData());
  $entity_config->delete();

  // Delete legacy state variables.
  $state->delete('acquia_contenthub.shared_secret');
  $state->delete('acquia_contenthub.reindexing_state');
}

/**
 * Updates ACH configuration with default value of send_contenthub_updates.
 */
function acquia_contenthub_update_82002(&$sandbox) {
  /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configurations */
  $ach_configurations = \Drupal::service('acquia_contenthub.configuration');
  $config = $ach_configurations->getContentHubConfig();
  $config->enableContentHubUpdates();
}

/**
 * Updates ACH configuration with default value of send_clientcdf_updates.
 */
function acquia_contenthub_update_82003(&$sandbox) {
  /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configurations */
  $ach_configurations = \Drupal::service('acquia_contenthub.configuration');
  $config = $ach_configurations->getContentHubConfig();
  $config->enableClientCdfUpdates();
}

/**
 * @} End of "addtogroup updates-8.6.x-contenthub-configuration".
 */
