<?php

namespace Drupal\Tests\acquia_contenthub_dashboard\Kernel;

use Drupal\acquia_contenthub_dashboard\Libs\ContentHubCors;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Tests ContentHubCors class.
 *
 * @group acquia_contenthub_dashboard
 *
 * @package Drupal\Tests\acquia_contenthub_dashboard\Kernel
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_dashboard\Libs\ContentHubCors
 */
class ContentHubCorsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'depcalc',
    'system',
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'acquia_contenthub_dashboard',
  ];

  /**
   * @covers ::setCorsConfiguration
   *
   * @dataProvider corsConfigurationDataProvider
   */
  public function testSetCorsConfiguration(array $options, array $headers, array $methods, array $origins): void {
    $kernel = $this->prophesize(HttpKernelInterface::class);
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->container->get('config.factory');
    $config = $config_factory->getEditable('acquia_contenthub_dashboard.settings');
    $config->set('allowed_origins', ['http://example.contenthub']);
    $config->set('auto_publisher_discovery', TRUE)->save();
    $ch_cors = new ContentHubCors($kernel->reveal(), $options);
    $ref_ch_cors = new \ReflectionMethod($ch_cors, 'setCorsConfiguration');
    $options = $ref_ch_cors->invoke($ch_cors, $options);

    self::assertSame($headers, $options['allowedHeaders'], 'ContentHub Cors allowed headers have been added');
    self::assertSame($methods, $options['allowedMethods'], 'ContentHub Cors allowed methods have been added');
    self::assertSame($origins, $options['allowedOrigins'], 'ContentHub Cors allowed origins have been added');
  }

  /**
   * Returns test cases and expectations.
   *
   * @return array[]
   *   The test data provider array.
   */
  public static function corsConfigurationDataProvider(): array {
    return [
      [
        [],
        ContentHubCors::HEADERS_TO_ADD,
        ContentHubCors::METHODS_TO_ADD,
        ['http://example.contenthub'],
      ],
      [
        [
          'allowedHeaders' => [''],
          'allowedMethods' => [''],
          'allowedOrigins' => [''],
        ],
        ContentHubCors::HEADERS_TO_ADD,
        ContentHubCors::METHODS_TO_ADD,
        ['http://example.contenthub'],
      ],
      [
        [
          'allowedHeaders' => ['some_header'],
          'allowedMethods' => ['PATCH'],
          'allowedOrigins' => ['http://another.site.example'],
        ],
        ['some_header', ...ContentHubCors::HEADERS_TO_ADD],
        ['PATCH', ...ContentHubCors::METHODS_TO_ADD],
        ['http://another.site.example', 'http://example.contenthub'],
      ],
      [
        [
          'allowedHeaders' => ['*'],
          'allowedMethods' => ['*'],
          'allowedOrigins' => ['*'],
        ],
        ['*'],
        ['*'],
        ['*'],
      ],
      [
        [
          'allowedHeaders' => ['some_header', '*'],
          'allowedMethods' => ['*', 'PATCH', 'GET'],
          'allowedOrigins' => ['http://another.site.example', '*'],
        ],
        ['some_header', ...ContentHubCors::HEADERS_TO_ADD],
        ['PATCH', ...ContentHubCors::METHODS_TO_ADD],
        ['http://another.site.example', 'http://example.contenthub'],
      ],
    ];
  }

}
