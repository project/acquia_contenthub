<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\acquia_contenthub_publisher\Controller\ContentHubExportQueueController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests CH export queue controller.
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Controller\ContentHubExportQueueController
 */
class ContentHubExportQueueControllerTest extends EntityKernelTestBase {

  use AcquiaContentHubAdminSettingsTrait;
  use ContentTypeCreationTrait;
  use MessengerTrait;

  /**
   * An entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * CH export queue controller.
   *
   * @var \Drupal\acquia_contenthub_publisher\Controller\ContentHubExportQueueController
   */
  protected ContentHubExportQueueController $chExportQueueController;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'depcalc',
    'acquia_contenthub',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('acquia_contenthub_publisher', ['acquia_contenthub_publisher_export_tracking']);
    $this->createAcquiaContentHubAdminSettings();
    $this->installConfig('node');

    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettingsInterface $export_settings */
    $export_settings = $this->container->get('acquia_contenthub_publisher.export_settings');
    $export_settings->toggleManualExportMode(TRUE);
    $export_settings->save();

    $this->createContentType([
      'type' => 'article',
    ])->save();
    $this->entity = Node::create([
      'type' => 'article',
      'title' => 'Test Node',
    ]);
    $this->entity->save();

    $this->chExportQueueController = new ContentHubExportQueueController(
      $this->container->get('acquia_contenthub_publisher.entity_enqueuer'),
      $this->container->get('acquia_contenthub.configuration'),
      $this->container->get('acquia_contenthub.logger_channel'),
      $this->container->get('cache.depcalc'),
      $this->container->get('acquia_contenthub_publisher.tracker'),
    );
  }

  /**
   * Test add entity to export queue with invalid uuid.
   *
   * @covers ::addEntity
   */
  public function testAddEntityToExportQueueWithInvalidEntityTypeOrUuid(): void {
    $request = Request::create('', 'GET', [
      'entity_type' => 'invalid-type',
      'entity_uuid' => 'invalid-uuid',
    ]);
    $request->headers->set('referer', 'https://www.example.com');
    $this->chExportQueueController->addEntity($request);
    $messages = $this->messenger()->all();
    $this->assertArrayHasKey(MessengerInterface::TYPE_ERROR, $messages);
    $this->assertSame('Invalid entity type or entity uuid.', $messages[MessengerInterface::TYPE_ERROR][0]->__toString());
  }

  /**
   * Test add entity to export queue with non-existing entity.
   *
   * @covers ::addEntity
   */
  public function testAddEntityToExportQueueWithNonExistingEntity(): void {
    $request = Request::create('', 'GET', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'entity_uuid' => '97853f88-fdc8-4ba1-98e6-b1ff5bb4647d',
    ]);
    $request->headers->set('referer', 'https://www.example.com');
    $this->chExportQueueController->addEntity($request);
    $messages = $this->messenger()->all();
    $this->assertArrayHasKey(MessengerInterface::TYPE_ERROR, $messages);
    $this->assertSame('Cannot retrieve entity.', $messages[MessengerInterface::TYPE_ERROR][0]->__toString());
  }

  /**
   * Test add entity to export queue.
   *
   * @covers ::addEntity
   */
  public function testAddEntityToExportQueue(): void {
    $request = Request::create('', 'GET', [
      'entity_type' => $this->entity->getEntityTypeId(),
      'entity_uuid' => $this->entity->uuid(),
    ]);
    $request->headers->set('referer', 'https://www.example.com');
    $this->chExportQueueController->addEntity($request);
    $messages = $this->messenger()->all();
    $this->assertArrayHasKey(MessengerInterface::TYPE_STATUS, $messages);
    $this->assertSame('1 entity enqueued.', $messages[MessengerInterface::TYPE_STATUS][0]->__toString());

    // Re-enqueue entity.
    $this->messenger()->deleteAll();
    $this->chExportQueueController->addEntity($request);
    $messages = $this->messenger()->all();
    $this->assertArrayHasKey(MessengerInterface::TYPE_WARNING, $messages);
    $this->assertSame('Entity already enqueued.', $messages[MessengerInterface::TYPE_WARNING][0]->__toString());
  }

}
