<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettings
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 */
class ExcludeSettingsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
  ];

  /**
   * @covers ::excludeFields
   * @covers ::getExcludedFields
   */
  public function testExcludeFieldsSettings(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface $sut */
    $sut = $this->container->get('acquia_contenthub_publisher.exclude.settings');
    $sut->excludeFields('node', 'article', ['field1', 'field2']);
    $sut->excludeFields('node', 'page', ['page_field1', 'page_field2']);
    $sut->save();

    $expected = [
      'article' => ['field1', 'field2'],
      'page' => ['page_field1', 'page_field2'],
    ];
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'All the bundle with fields are returned for node');

    $expected = ['field1', 'field2'];
    $actual = $sut->getExcludedFields('node', 'article');
    $this->assertEquals($expected, $actual, 'All the fields are returned for article.');

    $expected = [];
    $actual = $sut->getExcludedFields('node', 'invalid_bundle');
    $this->assertEquals($expected, $actual, 'Empty array returned if bundle does not exists.');

    $actual = $sut->getExcludedFields('invalid_type');
    $this->assertEquals($expected, $actual, 'Empty array returned if entity type does not exists.');
  }

  /**
   * @covers ::resetExcludedFields
   */
  public function testResetExcludeFieldsSettings(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface $sut */
    $sut = $this->container->get('acquia_contenthub_publisher.exclude.settings');
    $sut->excludeFields('term', 'tag', ['field1', 'field2']);
    $sut->save();

    $expected = ['field1', 'field2'];
    $actual = $sut->getExcludedFields('term', 'tag');
    $this->assertEquals($expected, $actual, 'Excluded fields persists.');

    $sut->resetExcludedFields();
    $actual = $sut->getExcludedFields('term', 'tag');
    $this->assertEquals([], $actual, 'Excluded fields are removed.');
  }

  /**
   * @covers ::removeExcludedFields
   */
  public function testRemoveExcludedFields(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface $sut */
    $sut = $this->container->get('acquia_contenthub_publisher.exclude.settings');
    $sut->excludeFields('node', 'article', ['field1', 'field2']);
    $sut->excludeFields('node', 'page', ['page_field1', 'page_field2']);
    $sut->save();

    $expected = [
      'article' => ['field1', 'field2'],
      'page' => ['page_field1', 'page_field2'],
    ];
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'All the bundle with fields are returned for node');

    $sut->removeExcludedFields('node', 'article', ['field1']);
    $expected = [
      'article' => ['field2'],
      'page' => ['page_field1', 'page_field2'],
    ];
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'Excluded field is removed from article bundle.');

    $sut->removeExcludedFields('node', 'page', ['page_field1']);
    $expected = [
      'article' => ['field2'],
      'page' => ['page_field2'],
    ];
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'Excluded field is removed from page bundle.');

    $sut->removeExcludedFields('node', 'invalid_bundle', ['field1']);
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'No change if bundle does not exists.');

    $sut->removeExcludedFields('invalid_type', 'article', ['field1']);
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'No change if entity type does not exists.');

    $sut->removeExcludedFields('node', 'article', ['field2']);
    $expected = [
      'page' => ['page_field2'],
    ];
    $actual = $sut->getExcludedFields('node');
    $this->assertEquals($expected, $actual, 'All fields are removed from article along with article bundle.');
  }

}
