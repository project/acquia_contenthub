<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\SerializeContentField;

use Acquia\ContentHubClient\CDF\CDFObject;
use Drupal\acquia_contenthub\Event\SerializeCdfEntityFieldEvent;
use Drupal\acquia_contenthub\EventSubscriber\SerializeContentField\EntityReferenceFieldSerializer;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub\EventSubscriber\SerializeContentField\EntityReferenceFieldSerializer
 *
 * @requires module depcalc
 *
 * @group acquia_contenthub_publisher
 *
 * @package acquia_contenthub
 */
class EntityReferenceFieldSerializerTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
  ];

  /**
   * User entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $entity;

  /**
   * Role entity.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected RoleInterface $userRole;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userRole = Role::create(['id' => 'test_role', 'label' => 'Test role']);
    $this->userRole->save();
    $this->entity = User::create([
      'name' => 'test_user',
    ]);
    $this->entity->save();
    $this->entity->addRole($this->userRole->id());
  }

  /**
   * Tests reference field serialization when config syndication is allowed.
   *
   * @throws \Exception
   */
  public function testReferenceFieldSerialization(): void {
    $event = new SerializeCdfEntityFieldEvent($this->entity, 'roles', $this->entity->get('roles'), $this->prophesize(CDFObject::class)->reveal());
    $serializer = new EntityReferenceFieldSerializer();
    $serializer->onSerializeContentField($event);
    $values = $event->getFieldData();
    $expected_values['value'][$this->entity->language()->getId()][] = $this->userRole->uuid();
    $this->assertEquals($expected_values, $values);
  }

  /**
   * Tests reference field serialization when config syndication is disabled.
   *
   * @throws \Exception
   */
  public function testReferenceFieldSerializationWithConfigSyndicationDisabled(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config_syndication */
    $config_syndication = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    // Disabling config syndication.
    $config_syndication->toggleConfigSyndication(TRUE);
    $config_syndication->save();

    $event = new SerializeCdfEntityFieldEvent($this->entity, 'roles', $this->entity->get('roles'), $this->prophesize(CDFObject::class)->reveal());
    $serializer = new EntityReferenceFieldSerializer();
    $serializer->onSerializeContentField($event);
    $values = $event->getFieldData();
    $expected_values['value'][$this->entity->language()->getId()][] = $this->userRole->id();
    $this->assertEquals($expected_values, $values);
  }

}
