<?php

namespace Drupal\Test\acquia_contenthub_publisher\Kernel\EventSubscriber\Config;

use Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\WatchdogAssertsTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Tests the removal of excluded fields when Bundle is deleted.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\EventSubscriber\Config\BundleConfigSubscriber
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @package Drupal\Test\acquia_contenthub_publisher\Kernel\EventSubscriber\Config
 */
class BundleConfigSubscriberTest extends EntityKernelTestBase {

  use ContentTypeCreationTrait;
  use WatchdogAssertsTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The exclude settings.
   *
   * @var \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface
   */
  protected ExcludeSettingsInterface $excludeSettings;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
    'node',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig([
      'filter',
      'node',
    ]);
    $this->entityFieldManager = $this->container->get('entity_field.manager');
    $this->excludeSettings = $this->container->get('acquia_contenthub_publisher.exclude.settings');
  }

  /**
   * Tests the removal of excluded fields when Bundle is deleted.
   *
   * @covers ::onConfigDelete
   */
  public function testRemovalOfExcludedFieldsWhenBundleIsDeleted(): void {
    $article = $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'page']);

    $fields_to_exclude = ['revision_log', 'revision_timestamp'];
    $this->excludeSettings->excludeFields('node', 'article', $fields_to_exclude);
    $this->excludeSettings->excludeFields('node', 'page', $fields_to_exclude);
    $this->excludeSettings->save();

    $this->assertEquals($this->excludeSettings->getExcludedFields('node', 'article'), $fields_to_exclude);
    $this->assertEquals($this->excludeSettings->getExcludedFields('node', 'page'), $fields_to_exclude);

    $article->delete();

    $this->excludeSettings = $this->container->get('acquia_contenthub_publisher.exclude.settings');
    $this->assertEmpty($this->excludeSettings->getExcludedFields('node', 'article'), 'Excluded fields are removed from the settings after bundle is deleted.');
    $this->assertEquals($this->excludeSettings->getExcludedFields('node', 'page'), $fields_to_exclude);
  }

}
