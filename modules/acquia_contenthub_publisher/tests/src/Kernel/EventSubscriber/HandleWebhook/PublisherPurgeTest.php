<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\HandleWebhook;

use Drupal\acquia_contenthub_publisher\ContentHubExportQueue;
use Drupal\acquia_contenthub_publisher\PublisherTracker;
use Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\HandleWebhook\PurgeTestBase;
use Drupal\Tests\acquia_contenthub_publisher\Traits\PublisherTrackerTestTrait;

/**
 * Tests purge operation on publisher.
 *
 * PR: on purge operation the publisher clears its tracking table, the queue,
 * and recreates the client CDF.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\EventSubscriber\HandleWebhook\Purge
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @see \Drupal\Tests\acquia_contenthub\Kernel\EventSubscriber\HandleWebhook\PurgeTest
 *    Additional test cases for publisher.
 */
class PublisherPurgeTest extends PurgeTestBase {

  use PublisherTrackerTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('acquia_contenthub_publisher', [PublisherTracker::EXPORT_TRACKING_TABLE]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueueName(): string {
    return ContentHubExportQueue::QUEUE_NAME;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTrackingTableName(): string {
    return PublisherTracker::EXPORT_TRACKING_TABLE;
  }

}
