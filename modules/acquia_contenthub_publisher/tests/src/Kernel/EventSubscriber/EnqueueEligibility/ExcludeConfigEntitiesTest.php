<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\EnqueueEligibility;

use Drupal\acquia_contenthub_publisher\Event\ContentHubEntityEligibilityEvent;
use Drupal\acquia_contenthub_publisher\EventSubscriber\EnqueueEligibility\ExcludeConfigEntities;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for directly enqueing config entities.
 *
 * @group acquia_contenthub_publisher
 *
 * @package Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\EnqueueEligibility
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\EventSubscriber\EnqueueEligibility\ExcludeConfigEntities
 */
class ExcludeConfigEntitiesTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
  ];

  /**
   * Tests config entity eligibility.
   *
   * @covers ::onEnqueueCandidateEntity
   *
   * @throws \Exception
   */
  public function testConfigEntityEligibility(): void {
    $config_entity = $this->prophesize(ConfigEntityInterface::class);

    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings $config */
    $config = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    $config->toggleConfigSyndication(TRUE);
    $config->save();
    $subscriber = new ExcludeConfigEntities($config);

    // Test insert.
    $event = new ContentHubEntityEligibilityEvent($config_entity->reveal(), 'insert');
    $subscriber->onEnqueueCandidateEntity($event);
    $this->assertFalse($event->getEligibility());
    $this->assertEquals('Config entities syndication is disabled.', $event->getReason(), 'Reason should be properly set.');

    // Test update.
    $event = new ContentHubEntityEligibilityEvent($config_entity->reveal(), 'update');
    $subscriber->onEnqueueCandidateEntity($event);
    $this->assertFalse($event->getEligibility());
    $this->assertEquals('Config entities syndication is disabled.', $event->getReason(), 'Reason should be properly set.');

    $config->toggleConfigSyndication(FALSE);
    $config->save();

    $event = new ContentHubEntityEligibilityEvent($config_entity->reveal(), 'insert');
    $subscriber->onEnqueueCandidateEntity($event);
    $this->assertTrue($event->getEligibility());
    $this->assertEquals('Not set.', $event->getReason(), 'Config syndication is active.');
  }

}
