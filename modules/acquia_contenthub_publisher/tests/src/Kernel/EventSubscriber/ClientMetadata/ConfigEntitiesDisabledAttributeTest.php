<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\ClientMetadata;

use Drupal\acquia_contenthub\Event\ClientMetaDataEvent;
use Drupal\acquia_contenthub_publisher\EventSubscriber\ClientMetadata\ConfigEntitiesDisabledAttribute;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests site's metadata, if config syndication is disabled.
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub_publisher\Kernel\EventSubscriber\ClientMetadata
 *
 * @covers \Drupal\acquia_contenthub_publisher\EventSubscriber\ClientMetadata\ConfigEntitiesDisabledAttribute
 */
class ConfigEntitiesDisabledAttributeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
  ];

  /**
   * Tests client metadata, if config syndication is disabled.
   */
  public function testEnableConfigSyndicationBuildClientMetaData(): void {
    $client_metadata_event = new ClientMetaDataEvent([]);
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');

    // Disabling config syndication.
    $config_syndication_settings->toggleConfigSyndication(FALSE);
    $config_syndication_settings->save();
    $sut = new ConfigEntitiesDisabledAttribute($config_syndication_settings);
    $sut->onCreateClientMetadata($client_metadata_event);
    $additional_config = $client_metadata_event->getAdditionalConfig();
    $this->assertNotEmpty($additional_config);
    $this->assertArrayHasKey('config_syndication', $additional_config);
    $this->assertSame(TRUE, $additional_config['config_syndication']);
  }

  /**
   * Tests client metadata, if config syndication is enabled.
   */
  public function testDisableConfigSyndicationBuildClientMetaData(): void {
    $client_metadata_event = new ClientMetaDataEvent([]);
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');

    // Enabling config syndication.
    $config_syndication_settings->toggleConfigSyndication(TRUE);
    $config_syndication_settings->save();
    $sut = new ConfigEntitiesDisabledAttribute($config_syndication_settings);
    $sut->onCreateClientMetadata($client_metadata_event);
    $additional_config = $client_metadata_event->getAdditionalConfig();
    $this->assertNotEmpty($additional_config);
    $this->assertArrayHasKey('config_syndication', $additional_config);
    $this->assertSame(FALSE, $additional_config['config_syndication']);
  }

}
