<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Libs\ExportSettings
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 */
class ExportSettingsKernelTest extends KernelTestBase {

  private const CONFIG_NAME = 'acquia_contenthub_publisher.export_settings';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
  ];

  /**
   * Tests if configuration was properly saved into database.
   */
  public function testExportSettings(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettings $sut */
    $sut = $this->container->get('acquia_contenthub_publisher.export_settings');
    $sut->toggleManualExportMode(TRUE);
    $sut->save();
    $returned = $sut->isManualExportModeEnabled();
    $this->assertTrue($returned);

    $data = $this->config(self::CONFIG_NAME)->getRawData();
    $this->assertTrue($data['manual_export_mode']);

    $sut->toggleManualExportMode(FALSE);
    $sut->save();
    $returned = $sut->isManualExportModeEnabled();
    $this->assertFalse($returned);

    $data = $this->config(self::CONFIG_NAME)->getRawData();
    $this->assertFalse($data['manual_export_mode']);
  }

}
