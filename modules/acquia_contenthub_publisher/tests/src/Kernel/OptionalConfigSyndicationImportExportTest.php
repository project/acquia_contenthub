<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\Tests\acquia_contenthub\Kernel\ImportExportTestBase;

/**
 * Tests node imports and exports.
 *
 * @group acquia_contenthub_publisher
 *
 * @package Drupal\Tests\acquia_contenthub_publisher
 */
class OptionalConfigSyndicationImportExportTest extends ImportExportTestBase {

  /**
   * {@inheritdoc}
   */
  protected $fixtures = [
    0 => [
      'cdf' => 'node/node_basic_page.json',
      'expectations' => 'expectations/node/node_basic_page.php',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'file',
    'acquia_contenthub_test',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setup(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Tests Node entity import/export.
   *
   * @param mixed $args
   *   Arguments. @see ImportExportTestBase::contentEntityImportExport() for the
   *   details.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider nodeEntityDataProvider
   */
  public function testImportExportWithOptionalSyndicationDisabled(...$args): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');

    // Enabling config syndication.
    $config_syndication_settings->toggleConfigSyndication(TRUE);
    $config_syndication_settings->save();
    parent::contentEntityImportExport(...$args);
  }

  /**
   * Data provider for testNodeEntity.
   *
   * @return array
   *   Data sets.
   */
  public static function nodeEntityDataProvider(): array {
    return [
      // Single Language, Simple Node.
      [
        0,
        [['type' => 'node', 'uuid' => '211e243d-9dd5-42a9-a141-2c195c728c51']],
        'node',
        '211e243d-9dd5-42a9-a141-2c195c728c51',
      ],
    ];
  }

}
