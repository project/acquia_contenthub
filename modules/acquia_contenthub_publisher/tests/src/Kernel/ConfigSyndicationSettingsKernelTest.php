<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 */
class ConfigSyndicationSettingsKernelTest extends KernelTestBase {

  private const CONFIG_NAME = 'acquia_contenthub_publisher.exclude_settings';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'user',
  ];

  /**
   * Tests if configuration was properly saved into database.
   */
  public function testConfigSyndicationSettings(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings $sut */
    $sut = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    $sut->toggleConfigSyndication(TRUE);
    $sut->save();
    $returned = $sut->isConfigSyndicationDisabled();
    $this->assertEquals(TRUE, $returned);

    $data = $this->config(self::CONFIG_NAME)->getRawData();
    $this->assertEquals(TRUE, $data['exclude_config_entities']);

    $sut->toggleConfigSyndication(FALSE);
    $sut->save();
    $returned = $sut->isConfigSyndicationDisabled();
    $this->assertEquals(FALSE, $returned);

    $data = $this->config(self::CONFIG_NAME)->getRawData();
    $this->assertEquals(FALSE, $data['exclude_config_entities']);
  }

}
