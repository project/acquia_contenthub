<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\acquia_contenthub\Traits\QueueTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests publisher export queue data.
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub_publisher\Kernel
 */
class PublisherExportQueueInspectorTest extends EntityKernelTestBase {

  use AcquiaContentHubAdminSettingsTrait;
  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use QueueTestTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'acquia_contenthub_server_test',
    'depcalc',
    'node',
    'taxonomy',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('acquia_contenthub_publisher', ['acquia_contenthub_publisher_export_tracking']);
    $this->createAcquiaContentHubAdminSettings();
    $this->installConfig([
      'filter',
      'node',
    ]);

    $this->createContentType([
      'type' => 'article',
    ]);
    $this->installEntitySchema('taxonomy_term');
  }

  /**
   * Tests export queue data.
   */
  public function testExportQueueData(): void {
    /** @var \Drupal\acquia_contenthub\QueueInspectorInterface $queue_inspector */
    $queue_inspector = $this->container->get('acquia_contenthub_publisher.export_queue_inspector');
    $node = $this->createNode([
      'type' => 'article',
    ]);
    $node2 = $this->createNode([
      'type' => 'article',
    ]);
    $vocab = $this->createVocabulary();
    $term = $this->createTerm($vocab);
    $this->addEntitiesToQueue($queue_inspector->getQueueName(), $node, $node2, $term);

    $data = $queue_inspector->getQueueData();
    $this->assertCount(3, $data, 'By default getQueueData returns every item.');
    $this->assertSame('node', $data[0]['type']);
    $this->assertSame('node', $data[1]['type']);
    $this->assertSame('taxonomy_term', $data[2]['type']);

    $data = $queue_inspector->getQueueData('node');
    $this->assertCount(2, $data, 'Filter by node returns 2.');
    $this->assertSame($node->uuid(), $data[0]['uuid']);
    $this->assertSame($node2->uuid(), $data[1]['uuid']);
    $this->assertSame($node->getEntityTypeId(), $data[0]['type']);
    $this->assertSame($node2->getEntityTypeId(), $data[1]['type']);

    $data = $queue_inspector->getQueueData('', $node->uuid());
    $this->assertCount(1, $data, 'Get data by uuid returns 1 node.');
    $this->assertSame($node->uuid(), $data[0]['uuid']);

    $data = $queue_inspector->getQueueData('', '', 5);
    $this->assertCount(3, $data, 'Limit by 5 returns 3.');

    $data = $queue_inspector->getQueueData('node', '', 1);
    $this->assertCount(1, $data, 'Limit by 1 and node returns 1.');

    $data = $queue_inspector->getQueueData('', '', 0);
    $this->assertCount(0, $data, 'Limit by 0 returns 0.');

    $data = $queue_inspector->getQueueData('', '', -1);
    $this->assertCount(3, $data, 'Limit by -1, result is 3.');

    $data = $queue_inspector->getQueueData('taxonomy_term', 'somerandomuuid');
    $this->assertCount(0, $data, 'Filter by term and random uuid will return 0.');

    $data = $queue_inspector->getQueueData('taxonomy_term', $node->uuid());
    $this->assertCount(0, $data, 'Filter by term and a valid node uuid will return 0.');

    $data = $queue_inspector->getQueueData('some_other_entity', $node->uuid());
    $this->assertCount(0, $data, 'Filter by invalid type and a valid node uuid will return 0.');

    $data = $queue_inspector->getQueueData('', 'invalid uuid');
    $this->assertCount(0, $data, 'Filter by invalid uuid will return 0.');
  }

}
