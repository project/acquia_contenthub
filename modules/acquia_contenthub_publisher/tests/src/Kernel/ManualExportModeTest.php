<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Acquia\ContentHubClient\ContentHubClient;
use Drupal\acquia_contenthub\AcquiaContentHubEntityTrackerInterface;
use Drupal\acquia_contenthub\Client\ClientFactory;
use Drupal\acquia_contenthub_publisher\Libs\ExportSettingsInterface;
use Drupal\acquia_contenthub_publisher\PublisherTracker;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests publisher's manual export mode.
 *
 * @group acquia_contenthub_publisher
 *
 * @requires module depcalc
 *
 * @package Drupal\Tests\acquia_contenthub_publisher\Kernel
 */
class ManualExportModeTest extends EntityKernelTestBase {

  use AcquiaContentHubAdminSettingsTrait;
  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use ProphecyTrait;

  /**
   * Export settings.
   *
   * @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettingsInterface
   */
  protected ExportSettingsInterface $exportSettings;

  /**
   * Publisher tracker.
   *
   * @var \Drupal\acquia_contenthub\AcquiaContentHubEntityTrackerInterface
   */
  protected AcquiaContentHubEntityTrackerInterface $publisherTracker;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'depcalc',
    'node',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('acquia_contenthub_publisher', ['acquia_contenthub_publisher_export_tracking']);
    $this->createAcquiaContentHubAdminSettings();
    $this->installConfig([
      'filter',
      'node',
    ]);
    $this->installSchema('node', ['node_access']);
    $this->createContentType([
      'type' => 'article',
    ]);
    $this->exportSettings = $this->container->get('acquia_contenthub_publisher.export_settings');
    $this->publisherTracker = $this->container->get('acquia_contenthub_publisher.tracker');
  }

  /**
   * Tests tracking of entity with PublisherTracker::READY_TO_QUEUE status.
   */
  public function testReadyToQueueEntityTracking(): void {
    $node1 = $this->createNode([
      'type' => 'article',
    ]);
    $node1->save();
    $record = $this->publisherTracker->getRecord($node1->uuid());
    $this->assertNotNull($record);
    $this->assertNotSame(PublisherTracker::READY_TO_QUEUE, $record->status);

    // Enabling manual export mode.
    $this->exportSettings->toggleManualExportMode(TRUE);
    $this->exportSettings->save();

    $node2 = $this->createNode([
      'type' => 'article',
    ]);
    $node2->save();
    $record = $this->publisherTracker->getRecord($node2->uuid());
    $this->assertNotNull($record);
    $this->assertSame(PublisherTracker::READY_TO_QUEUE, $record->status);
  }

  /**
   * Tests entity deletion when manual export mode enabled.
   */
  public function testEntityDeletionWithManualExportMode(): void {
    // Enabling manual export mode.
    $this->exportSettings->toggleManualExportMode(TRUE);
    $this->exportSettings->save();

    $node = $this->createNode([
      'type' => 'article',
    ]);
    $node->save();

    $client = $this->prophesize(ContentHubClient::class);
    $client->getEntity($node->uuid())->willReturn([]);
    $client_factory = $this->prophesize(ClientFactory::class);
    $client_factory->getClient()->willReturn($client->reveal());
    $this->container->set('acquia_contenthub.client.factory', $client_factory->reveal());

    $record = $this->publisherTracker->getRecord($node->uuid());
    $this->assertNotNull($record);
    $this->assertSame(PublisherTracker::READY_TO_QUEUE, $record->status);

    $client->deleteEntity($node->uuid())->shouldNotBeCalled();
    // Delete entity.
    $node->delete();
    $record = $this->publisherTracker->getRecord($node->uuid());
    $this->assertFalse($record);
  }

}
