<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\views\Views;

/**
 * Tests ContentHub Export Tracker View.
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExportTrackerViewKernelTest extends EntityKernelTestBase {

  use AcquiaContentHubAdminSettingsTrait;
  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'file',
    'user',
    'node',
    'views',
    'taxonomy',
    'depcalc',
    'acquia_contenthub',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('acquia_contenthub_publisher', ['acquia_contenthub_publisher_export_tracking']);
    $this->createAcquiaContentHubAdminSettings();
    $this->installConfig([
      'filter',
      'node',
      'acquia_contenthub_publisher',
    ]);
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('file');
    $this->installEntitySchema('taxonomy_term');
    $this->container->get('config.installer')->installDefaultConfig('module', 'acquia_contenthub_publisher');
    $this->createRequiredData();
  }

  /**
   * Tests tracker view data.
   */
  public function testContentHubExportTrackerView() {
    $view = Views::getView('ch_export_tracker');
    // Set display and execute the view.
    $view->setDisplay('default');
    $view->setItemsPerPage(40);
    $view->execute();

    // Ensure the view is correctly loaded.
    $this->assertNotNull($view, 'Export Tracker view is loaded.');
    // Validate the view result array.
    $results = $view->result;

    $entity_id = [];
    $entity_type = [];
    $status = [];
    foreach ($results as $row) {
      $row = (array) $row;
      if (is_numeric($row['acquia_contenthub_publisher_export_tracking_entity_id'])) {
        $entity_id[] = $this->getEntityNameFromId($row['acquia_contenthub_publisher_export_tracking_entity_type'], $row['acquia_contenthub_publisher_export_tracking_entity_id']);
      }
      else {
        $entity_id[] = $row['acquia_contenthub_publisher_export_tracking_entity_id'];
      }
      $entity_type[] = $row['acquia_contenthub_publisher_export_tracking_entity_type'];
      $status[] = $row['acquia_contenthub_publisher_export_tracking_status'];
    }

    $this->assertContains('Test Node', $entity_id, 'Entity id/name should match.');
    $this->assertContains('node', $entity_type, 'Entity type should match.');
    $this->assertContains('queued', $status, 'Entity status should match.');

    $this->assertContains('test_vocabulary', $entity_id, 'Entity id/name should match.');
    $this->assertContains('taxonomy_vocabulary', $entity_type, 'Entity type should match.');
    $this->assertContains('queued', $status, 'Entity status should match.');

    $this->assertContains('Test Term', $entity_id, 'Entity id/name should match.');
    $this->assertContains('taxonomy_term', $entity_type, 'Entity type should match.');
    $this->assertContains('queued', $status, 'Entity status should match.');
  }

  /**
   * Helper function to create dummy data.
   */
  protected function createRequiredData() {
    $this->createContentType([
      'type' => 'article',
    ])->save();

    Node::create([
      'type' => 'article',
      'title' => 'Test Node',
    ])->save();

    Vocabulary::create([
      'name' => 'Test vocabulary',
      'vid' => 'test_vocabulary',
    ])->save();

    Term::create([
      'vid' => 'test_vocabulary',
      'name' => 'Test Term',
    ])->save();
  }

  /**
   * Helper function to get the entity name from its ID.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID.
   *
   * @return string|null
   *   The entity label or NULL if not found.
   */
  protected function getEntityNameFromId($entity_type, $entity_id) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
    return $entity ? $entity->label() : NULL;
  }

}
