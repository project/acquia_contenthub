<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Functional\Wizard;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\acquia_contenthub\Traits\CommonRandomGenerator;
use Drupal\Tests\views\Functional\Wizard\WizardTestBase;

/**
 * Tests ContentHub Export Tracker View Filter.
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExportTrackerViewFilterTest extends WizardTestBase {

  use CommonRandomGenerator;

  /**
   * User that has administrator permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $authorizedUser;

  /**
   * Path to content hub export tracker view.
   */
  const ACH_EXPORT_TRACKER_VIEW_PATH = 'admin/content/ach_export_tracker';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'views',
    'views_ui',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = []): void {
    parent::setUp();
    $this->authorizedUser = $this->drupalCreateUser(['administer acquia content hub', 'administer nodes']);
    $this->container->get('config.installer')->installDefaultConfig('module', 'acquia_contenthub_publisher');
    $this->drupalLogin($this->authorizedUser);

    // Inserting test data into tracker table.
    $this->insertDataInTrackerTable();
  }

  /**
   * Tests Content-Hub Export Tracker View filters are rendered properly.
   */
  public function testContentHubExportTrackerViewFiltersRenderedProperly(): void {
    $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);
    $page = $this->getSession()->getPage();

    // Check the number of dropdown filters.
    $dropdown = $this->cssSelect('.view-filters .views-exposed-form select');
    $this->assertCount(2, $dropdown);

    // Check the number of input filters.
    $input_filters = $this->cssSelect('.view-filters .views-exposed-form input[type="text"]');
    $this->assertCount(2, $input_filters);

    // Check the number of submit button.
    $submit = $this->cssSelect('.view-filters .views-exposed-form input[type="submit"]');
    $this->assertCount(1, $submit);

    $value = $page->find('css', 'label[for="edit-entity-type"]')->getText();
    $this->assertEquals('Entity Type', $value, 'Label for Entity Type filter.');

    $value = $page->find('css', 'label[for="edit-status"]')->getText();
    $this->assertEquals('Status', $value, 'Label for Status filter.');

    $value = $page->find('css', 'label[for="edit-entity-label"]')->getText();
    $this->assertEquals('Title', $value, 'Label for Title filter.');

    $value = $page->find('css', 'label[for="edit-entity-uuid"]')->getText();
    $this->assertEquals('UUID', $value, 'Label for UUID filter.');

    $value = $page->findButton('Filter')->getAttribute('value');
    $this->assertEquals('Filter', $value, 'Label for submit button.');
  }

  /**
   * Tests Content-Hub Export Tracker View filters.
   */
  public function testContentHubExportTrackerViewFilter(): void {
    $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);
    $page = $this->getSession()->getPage();

    // Testing Status field.
    $page->hasSelect('#edit-status');
    $page->selectFieldOption('status', 'queued');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(1, $result);

    // Testing Status field.
    $page->hasSelect('#edit-status');
    $page->selectFieldOption('status', 'exported');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(2, $result);

    // Testing Entity Type field.
    $page->hasSelect('#edit-entity-type');
    $page->selectFieldOption('entity_type', 'node');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(1, $result);

    // Testing Entity Type field.
    $page->hasSelect('#edit-entity-type');
    $page->selectFieldOption('edit-entity-type', 'menu');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(1, $result);

    // Testing Entity Type field.
    $page->hasSelect('#edit-entity-type');
    $page->selectFieldOption('edit-entity-type', 'view');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(1, $result);

    // Testing Title field.
    $page->fillField('entity_label', 'Test Node 1');
    $page->findButton('Filter')->click();
    // Check the number of rows in result.
    $result = $this->cssSelect('tbody tr');
    $this->assertCount(1, $result);

  }

  /**
   * Inserts data into the Tracker table.
   */
  protected function insertDataInTrackerTable(): void {
    $data = [
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'view',
        'status' => 'exported',
        'entity_id' => 'ch_export_tracker',
        'label' => 'Content Hub Export Tracker View',
      ],
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'menu',
        'status' => 'exported',
        'entity_id' => '23',
        'label' => 'Test Menu',
      ],
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'node',
        'status' => 'queued',
        'entity_id' => '1',
        'label' => 'Test Node 1',
      ],
    ];
    $query = $this->container->get('database')->insert('acquia_contenthub_publisher_export_tracking');
    foreach ($data as $item) {
      $query->fields(array_keys($item));
      $query->values(array_values($item));
    }
    $query->execute();
  }

}
