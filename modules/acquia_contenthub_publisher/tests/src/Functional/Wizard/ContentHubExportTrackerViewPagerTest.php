<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Functional\Wizard;

use Drupal\Component\Utility\Random;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\acquia_contenthub\Traits\CommonRandomGenerator;
use Drupal\Tests\views\Functional\Wizard\WizardTestBase;

/**
 * Tests ContentHub Export Tracker View Pager.
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExportTrackerViewPagerTest extends WizardTestBase {

  use CommonRandomGenerator;

  /**
   * User that has administrator permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $authorizedUser;

  /**
   * Path to content hub export tracker view.
   */
  const ACH_EXPORT_TRACKER_VIEW_PATH = 'admin/content/ach_export_tracker';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'views',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = []): void {
    parent::setUp();
    $this->authorizedUser = $this->drupalCreateUser(['administer acquia content hub', 'administer nodes']);
    $this->container->get('config.installer')->installDefaultConfig('module', 'acquia_contenthub_publisher');
    $this->drupalLogin($this->authorizedUser);
  }

  /**
   * Tests Content-Hub Export Tracker View pager.
   */
  public function testContentHubExportTrackerViewPagerNotExists(): void {
    // Inserting less than 25 rows into tracker table.
    $this->insertDataInTrackerTable(10);
    $session = $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);
    $session->statusCodeEquals(200);

    // Finding the existence of a pager.
    $session->elementNotExists('xpath', '//ul[contains(@class, "pager__items")]/li');

  }

  /**
   * Tests Content-Hub Export Tracker View pager.
   */
  public function testContentHubExportTrackerViewPagerExists(): void {
    // Inserting more than 25 rows into tracker table.
    $this->insertDataInTrackerTable(60);
    $session = $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);
    $session->statusCodeEquals(200);

    // Asserts every page contains 25 rows.
    $pager = $this->cssSelect('tbody tr');
    $this->assertCount(25, $pager);
    // Finding the existence of a pager.
    $session->elementExists('xpath', '//ul[contains(@class, "pager__items")]/li');

    $pager = $this->xpath('//ul[@class="pager__items js-pager__items"]/li[not(contains(@class, "pager__item--next")) and not(contains(@class, "pager__item--last"))]');
    $this->assertCount(3, $pager);
  }

  /**
   * Inserts data into the Tracker table.
   *
   * @param int $count
   *   Node count to be generated.
   */
  protected function insertDataInTrackerTable(int $count): void {
    $random = new Random();
    $data = [];
    for ($i = 0; $i < $count; $i++) {
      $data[] = [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'node',
        'status' => 'queued',
        'entity_id' => $random->string(10),
        'label' => $random->string(10),
      ];
    }
    $query = $this->container->get('database')->insert('acquia_contenthub_publisher_export_tracking');
    foreach ($data as $item) {
      $query->fields(array_keys($item));
      $query->values(array_values($item));
    }
    $query->execute();
  }

}
