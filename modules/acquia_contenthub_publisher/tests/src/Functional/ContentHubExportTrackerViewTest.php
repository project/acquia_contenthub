<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\acquia_contenthub\Traits\CommonRandomGenerator;
use Drupal\Tests\BrowserTestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests ContentHub Export Tracker View.
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExportTrackerViewTest extends BrowserTestBase {

  use CommonRandomGenerator;

  /**
   * User that has administrator permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $authorizedUser;

  /**
   * Path to content hub export tracker view.
   */
  const ACH_EXPORT_TRACKER_VIEW_PATH = 'admin/content/ach_export_tracker';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * User that has administrator permission.
   *
   * @var \Drupal\views\Tests\ViewTestData
   */
  protected ViewTestData $view;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'views',
    'acquia_contenthub_publisher',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->authorizedUser = $this->drupalCreateUser(['administer acquia content hub', 'administer nodes']);
    $this->container->get('config.installer')->installDefaultConfig('module', 'acquia_contenthub_publisher');
    $this->drupalLogin($this->authorizedUser);
  }

  /**
   * Tests permissions for different users.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testContentHubExportTrackerViewPermissions() {
    $session = $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);

    $session->statusCodeEquals(200);
    $session->pageTextContains('Acquia Content Hub Export Tracker');

    $unauthorizedUser = $this->drupalCreateUser();
    $this->drupalLogout();
    $this->drupalLogin($unauthorizedUser);

    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);
    $session->pageTextContains('Access denied');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests Content-Hub Export Tracker View is rendered properly.
   */
  public function testContentHubExportTrackerViewRenderedProperly(): void {
    // Inserting dummy data into tracker table.
    $this->insertDataInTrackerTable();
    $session = $this->assertSession();
    $this->drupalGet(self::ACH_EXPORT_TRACKER_VIEW_PATH);

    // Check the number of columns in the header.
    $headers = $this->cssSelect('thead tr th');
    $this->assertCount(8, $headers);

    // Validate views header.
    $session->elementExists('css', '.views-field-ch-bulk-form');
    $session->pageTextContains('Title');
    $session->pageTextContains('Entity Type');
    $session->pageTextContains('Status');
    $session->pageTextContains('UUID');
    $session->pageTextContains('Hash');
    $session->pageTextContains('Last Updated');
    $session->pageTextContains('Operations');

    // Check the number of rows in the body.
    $rows = $this->cssSelect('tbody tr');
    $this->assertCount(3, $rows);

    // Validate views data.
    $session->pageTextContains('view');
    $session->pageTextContains('exported');
    $session->pageTextContains('897687986567897');
    $session->pageTextContains('2024-05-22T02:52:09+00:00');

    $session->pageTextContains('menu');
    $session->pageTextContains('exported');
    $session->pageTextContains('987687678976869');
    $session->pageTextContains('2024-05-22T02:52:09+00:00');

    $session->pageTextContains('node');
    $session->pageTextContains('queued');
    $session->pageTextContains('987687678976786867');
    $session->pageTextContains('2023-05-22T02:52:09+00:00');
  }

  /**
   * Inserts data into the Tracker table.
   */
  protected function insertDataInTrackerTable(): void {
    $data = [
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'view',
        'status' => 'exported',
        'entity_id' => 'ch_export_tracker',
        'hash' => '897687986567897',
        'modified' => '2024-05-22T02:52:09+00:00',
        'label' => 'Content Hub Export Tracker View',
      ],
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'menu',
        'status' => 'exported',
        'entity_id' => 'test_menu',
        'hash' => '987687678976869',
        'modified' => '2024-05-22T02:52:09+00:00',
        'label' => 'Test Menu',
      ],
      [
        'entity_uuid' => $this->generateUuid(),
        'entity_type' => 'node',
        'status' => 'queued',
        'entity_id' => '1',
        'hash' => '987687678976786867',
        'modified' => '2023-05-22T02:52:09+00:00',
        'label' => 'Test Node 1',
      ],
    ];
    $query = $this->container->get('database')->insert('acquia_contenthub_publisher_export_tracking');
    foreach ($data as $item) {
      $query->fields(array_keys($item));
      $query->values(array_values($item));
    }
    $query->execute();
  }

}
