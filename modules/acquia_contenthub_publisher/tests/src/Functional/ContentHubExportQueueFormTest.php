<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Functional;

use Drupal\acquia_contenthub_publisher\PublisherTracker;
use Drupal\Core\Url;
use Drupal\Tests\acquia_contenthub\Functional\ContentHubQueueFormTestBase;

/**
 * Tests the Export Queue form.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Form\ContentHubExportQueueForm
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExportQueueFormTest extends ContentHubQueueFormTestBase {

  /**
   * Path to publisher export queue form.
   */
  const QUEUE_FORM_PATH = '/admin/config/services/acquia-contenthub/export-queue';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'acquia_contenthub_publisher',
  ];

  /**
   * Tests form reachability for users with different permissions.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testExportQueuePagePermissions(): void {
    $this->checkFormAccessForUsers(self::QUEUE_FORM_PATH, 'Export Queue');
  }

  /**
   * Tests the form submissions by enabling/disabling manual export mode.
   */
  public function testFormSubmissionWithDifferentExportSettings(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettings $export_settings */
    $export_settings = $this->container->get('acquia_contenthub_publisher.export_settings');
    $this->assertFalse($export_settings->isManualExportModeEnabled());

    $this->drupalGet(Url::fromRoute('acquia_contenthub_publisher.export_queue'));
    $session = $this->getSession();
    $page = $session->getPage();
    $page->checkField('manual_export_mode');
    $page->pressButton('Save Export Settings');
    $this->assertSession()->pageTextContains('Export control settings have been saved!');

    $this->rebuildContainer();
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettings $export_settings */
    $export_settings = $this->container->get('acquia_contenthub_publisher.export_settings');
    $this->assertTrue($export_settings->isManualExportModeEnabled());

    $this->getSession()->reload();
    $page->uncheckField('manual_export_mode');
    $page->pressButton('Save Export Settings');

    $this->rebuildContainer();
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ExportSettings $export_settings */
    $export_settings = $this->container->get('acquia_contenthub_publisher.export_settings');
    $this->assertFalse($export_settings->isManualExportModeEnabled());
  }

  /**
   * {@inheritdoc}
   */
  public static function queueFormDataProvider(): array {
    return [
      [
        self::QUEUE_FORM_PATH,
        PublisherTracker::EXPORT_TRACKING_TABLE,
        'Export Items',
        'Purged all contenthub export queues.',
      ],
    ];
  }

}
