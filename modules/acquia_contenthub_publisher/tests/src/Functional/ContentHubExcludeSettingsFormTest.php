<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Functional;

use Drupal\Core\Url;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests exclude settings form elements and interactions.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_publisher\Form\ExcludeSettingsForm
 *
 * @group acquia_contenthub_publisher
 */
class ContentHubExcludeSettingsFormTest extends BrowserTestBase {

  use AcquiaContentHubAdminSettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_publisher',
    'acquia_contenthub_server_test',
    'user',
  ];

  /**
   * Content Hub administrator user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $chUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createAcquiaContentHubAdminSettings();
    /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configuration */
    $ach_configuration = $this->container->get('acquia_contenthub.configuration');
    $ach_configuration->getContentHubConfig()->disableContentHubUpdates();
    $this->chUser = $this->drupalCreateUser([
      'administer acquia content hub',
    ], 'user1');
    $this->drupalLogin($this->chUser);
  }

  /**
   * Tests if elements are displayed properly.
   */
  public function testUiElementsAreDisplayed(): void {
    $this->drupalGet(Url::fromRoute('acquia_contenthub_publisher.exclude_settings'));
    $session = $this->assertSession();

    $session->elementTextEquals('css', 'label[for=edit-exclude-config-entities]', 'Exclude config entities');
    $session->checkboxNotChecked('exclude_config_entities');
    $this->assertSession()->pageTextContains('Excluding entity types or bundles from the export queue does not control entities that are dependencies of other entities. All dependencies are exported regardless of the configuration of this form.');
    $session->buttonExists('Save configuration');
  }

  /**
   * Tests the form submissions by enabling/disabling config syndication.
   */
  public function testFormSubmissionWithDifferentConfigSyndicationValues(): void {
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    $this->assertFalse($config_syndication_settings->isConfigSyndicationDisabled());

    $this->drupalGet(Url::fromRoute('acquia_contenthub_publisher.exclude_settings'));
    $session = $this->getSession();
    $page = $session->getPage();
    $page->checkField('exclude_config_entities');
    $page->pressButton('Save configuration');
    $this->assertSession()->pageTextContains('The settings have been saved.');

    $this->rebuildContainer();
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    $this->assertTrue($config_syndication_settings->isConfigSyndicationDisabled());

    $this->getSession()->reload();
    $page->uncheckField('exclude_config_entities');
    $page->pressButton('Save configuration');

    $this->rebuildContainer();
    /** @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettings $config_syndication_settings */
    $config_syndication_settings = $this->container->get('acquia_contenthub_publisher.config_syndication.settings');
    $this->assertFalse($config_syndication_settings->isConfigSyndicationDisabled());
  }

}
