<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Traits;

use Drupal\acquia_contenthub_publisher\PublisherTracker;
use Drupal\Tests\acquia_contenthub\Traits\CommonRandomGenerator;
use Drupal\Tests\RandomGeneratorTrait;

/**
 * Helper functions for publisher tracker related tests.
 */
trait PublisherTrackerTestTrait {

  use CommonRandomGenerator;
  use RandomGeneratorTrait;

  /**
   * Populates tracking table with random data.
   *
   * @param int|null $num_of_rows
   *   The number of rows to insert.
   *
   * @throws \Exception
   */
  public function populateTrackingTableWithRandomData(?int $num_of_rows): void {
    $amount = $num_of_rows ?: random_int(100, 500);
    $query = \Drupal::database()
      ->insert(PublisherTracker::EXPORT_TRACKING_TABLE)
      ->fields([
        'entity_type',
        'entity_id',
        'entity_uuid',
        'status',
        'created',
        'modified',
        'hash',
        'queue_id',
        'label',
      ]);

    for ($i = 0; $i < $amount; $i++) {
      $query->values([
        'entity_type' => $this->randomMachineName(random_int(1, 20)),
        'entity_id' => $this->takeRandom([
          random_int(1, 100),
          $this->randomMachineName(random_int(1, 20)),
        ]),
        'entity_uuid' => $this->generateUuid(),
        'status' => $this->takeRandom(['confirmed', 'queued', 'exported']),
        'created' => time(),
        'modified' => time(),
        'hash' => sha1($this->randomString()),
        'queue_id' => $this->takeRandom(['', random_int(1, 10000)]),
        'label' => $this->randomMachineName(random_int(1, 10)),
      ]);
    }

    $query->execute();
  }

}
