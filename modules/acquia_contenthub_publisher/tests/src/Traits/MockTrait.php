<?php

namespace Drupal\Tests\acquia_contenthub_publisher\Traits;

/**
 * A trait providing helper methods for mocking.
 *
 * @internal
 */
trait MockTrait {

  /**
   * Helper for consecutive calls to mock objects.
   *
   * Since the removal of InvocationMocker::withConsecutive() from PHPUnit, we
   * can use InvocationMocker::with() instead, passing a stack of expected
   * calls to mocked methods.
   *
   * @param array<mixed> ...$expectedArgumentsSequence
   *   An array of expected arguments for a stack of calls to the method. Each
   *   array item contains the expected call arguments, either concrete values
   *   or Constraint objects (could be a partial list, in which case the actual
   *   arguments in excess of the expected ones are ignored).
   *
   * @return iterable<\PHPUnit\Framework\Constraint\Callback<mixed>>
   *   Each item yields a method's call expected arguments.
   */
  public static function consecutiveCalls(array ...$expectedArgumentsSequence): iterable {
    return (new ConsecutiveCallsIterator($expectedArgumentsSequence))->generator();
  }

}
