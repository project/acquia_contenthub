<?php

/**
 * @file
 * Provide views data for acquia_contenthub_publisher.module.
 */

/**
 * Implements hook_views_data().
 */
function acquia_contenthub_publisher_views_data() {
  $data['acquia_contenthub_publisher_export_tracking']['table']['group'] = t('Acquia Content Hub Export Tracker');
  $data['acquia_contenthub_publisher_export_tracking']['table']['provider'] = 'acquia_contenthub_publisher';

  $data['acquia_contenthub_publisher_export_tracking']['table']['base'] = [
    'field' => 'entity_uuid',
    'title' => t('Acquia Content Hub Export Tracker'),
  ];

  $data['acquia_contenthub_publisher_export_tracking']['ch_bulk_form'] = [
    'title' => t('Entity bulk operations'),
    'action_title' => ['default' => t('Action')],
    'include_exclude' => [
      'default' => 'exclude',
    ],
    'selected_actions' => [
      'default' => [],
    ],
    'field' => [
      'id' => 'ch_bulk_form',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['entity_uuid'] = [
    'title' => t('Entity UUID'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['entity_type'] = [
    'title' => t('Entity Type'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'in_operator',
      'options callback' => 'Drupal\acquia_contenthub_publisher\ContentHubExportTrackerViewHelper::getEntityTypes',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['entity_id'] = [
    'title' => t('Entity Id'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['entity_label'] = [
    'title' => t('Entity Label'),
    'real field' => 'entity_id',
    'link_to_entity' => ['default' => FALSE],
    'field' => [
      'id' => 'ch_entity_label',
      'entity type field' => 'entity_type',
      'additional fields' => [
        'type' => [
          'field' => 'entity_type',
        ],
      ],
    ],
    'argument' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'ch_entity_title_filter',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['status'] = [
    'title' => t('Status'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'in_operator',
      'options callback' => 'Drupal\acquia_contenthub_publisher\PublisherTracker::getStatus',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['created'] = [
    'title' => t('Created'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['modified'] = [
    'title' => t('Modified'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['hash'] = [
    'title' => t('Hash'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['queue_id'] = [
    'title' => t('Queue id'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['acquia_contenthub_publisher_export_tracking']['operations'] = [
    'title' => t('Operations'),
    'real field' => 'queue_id',
    'help' => t('Provides links to perform CH operations.'),
    'field' => [
      'id' => 'ch_operations',
    ],
  ];

  return $data;
}
