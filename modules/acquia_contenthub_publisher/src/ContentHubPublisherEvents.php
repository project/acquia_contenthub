<?php

namespace Drupal\acquia_contenthub_publisher;

/**
 * Defines events for the acquia_contenthub_publisher module.
 *
 * @see \Drupal\acquia_contenthub_publisher\Event\SerializeCdfEntityFieldEvent
 */
final class ContentHubPublisherEvents {

  /**
   * Event name fired for eligibility of an entity to POST to ContentHub.
   *
   * This event determines the eligibility of an entity before enqueuing it to
   * POST to Content hub. Event subscribers receive a
   * \Drupal\acquia_contenthub_publisher\Event\ContentHubEntityEligibilityEvent
   * instance. A simple TRUE/FALSE on eligibility check is expected for each
   * event.
   */
  const ENQUEUE_CANDIDATE_ENTITY = 'enqueue_candidate_entity';

  /**
   * Event triggered while creating CDF.
   *
   * This event allows to replace entity id with entity uuid.
   *
   * @see \Drupal\acquia_contenthub_publisher\Event\ViewFilterPluginReplaceEntityIdWithUuidEvent
   */
  public const REPLACE_ENTITY_ID_WITH_UUID = 'acquia_contenthub_replace_entity_id_with_uuid';

}
