<?php

namespace Drupal\acquia_contenthub_publisher\EventSubscriber\ReplaceEntityIdWithUuid;

/**
 * Replaces entity id with uuid for taxonomy_index_id plugin filter.
 *
 * @see \Drupal\acquia_contenthub_subscriber\EventSubscriber\ReplaceEntityUuidWithId\TaxonomyIndexIdViewFilterPlugin
 *   The unserializer for this plugin.
 *
 * @package \Drupal\acquia_contenthub_publisher\Event\ReplaceEntityIdWithUuidEvent
 */
class TaxonomyIndexIdViewFilterPlugin extends ViewFilterPluginReplaceEntityIdWithUuidBase {

  /**
   * Replaces entity id with uuid for taxonomy_index_tid plugin.
   *
   * @param array $filter_data
   *   The filter data.
   *
   * @return array
   *   The updated filter data.
   *
   * @throws \Exception
   */
  public function replaceEntityIdWithUuidForViewFilterPlugin(array $filter_data): array {
    if ($filter_data['plugin_id'] !== 'taxonomy_index_tid') {
      return [];
    }

    $entity_type = 'taxonomy_term';
    // Updating entity id with uuid.
    foreach ($filter_data['value'] as $key => $entity_id) {
      if (empty($entity_id)) {
        continue;
      }
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
      if ($entity) {
        $filter_data['value'][$key] = $entity->uuid();
        continue;
      }
      $this->logMissingEntity($entity_type, $entity_id, $filter_data['plugin_id']);
    }
    return $filter_data;
  }

}
