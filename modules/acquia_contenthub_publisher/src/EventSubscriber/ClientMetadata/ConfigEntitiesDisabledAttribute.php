<?php

namespace Drupal\acquia_contenthub_publisher\EventSubscriber\ClientMetadata;

use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\ClientMetaDataEvent;
use Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Updates site's metadata, if config syndication is disabled.
 */
class ConfigEntitiesDisabledAttribute implements EventSubscriberInterface {

  /**
   * Content Hub config entities syndication setting.
   *
   * @var \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface
   */
  protected ConfigSyndicationSettingsInterface $configSyndicationSetting;

  /**
   * ConfigEntitiesDisabledAttribute constructor.
   *
   * @param \Drupal\acquia_contenthub_publisher\Libs\ConfigSyndicationSettingsInterface $config_syndication_settings
   *   Content Hub config entities syndication setting.
   */
  public function __construct(ConfigSyndicationSettingsInterface $config_syndication_settings) {
    $this->configSyndicationSetting = $config_syndication_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AcquiaContentHubEvents::CLIENT_METADATA][] =
      ['onCreateClientMetadata', 100];
    return $events;
  }

  /**
   * Extends client metadata config and adds config_syndication key to config.
   *
   * @param \Drupal\acquia_contenthub\Event\ClientMetaDataEvent $event
   *   Client metadata event.
   */
  public function onCreateClientMetadata(ClientMetaDataEvent $event): void {
    $additional_config['config_syndication'] = !$this->configSyndicationSetting->isConfigSyndicationDisabled();
    $event->setAdditionalConfig($additional_config);
  }

}
