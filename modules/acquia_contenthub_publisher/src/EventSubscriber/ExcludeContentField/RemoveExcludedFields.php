<?php

namespace Drupal\acquia_contenthub_publisher\EventSubscriber\ExcludeContentField;

use Drupal\acquia_contenthub\Event\ExcludeEntityFieldEvent;
use Drupal\acquia_contenthub\EventSubscriber\ExcludeContentField\ExcludeContentFieldBase;
use Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface;

/**
 * Removes excluded fields.
 */
class RemoveExcludedFields extends ExcludeContentFieldBase {

  /**
   * The exclude field settings.
   *
   * @var \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface
   */
  protected ExcludeSettingsInterface $excludeSettings;

  /**
   * Constructor for the RemoveExcludedFields class.
   *
   * @param \Drupal\acquia_contenthub_publisher\Libs\ExcludeSettingsInterface $exclude_settings
   *   Exclude fields settings.
   */
  public function __construct(ExcludeSettingsInterface $exclude_settings) {
    $this->excludeSettings = $exclude_settings;
  }

  /**
   * {@inheritDoc}
   */
  public function shouldExclude(ExcludeEntityFieldEvent $event): bool {
    $bundle_name = $event->getEntity()->bundle();
    $entity_type_id = $event->getEntity()->getEntityTypeId();
    $field_name = $event->getFieldName();

    $excluded_fields = $this->excludeSettings->getExcludedFields($entity_type_id, $bundle_name);
    return in_array($field_name, $excluded_fields);
  }

}
