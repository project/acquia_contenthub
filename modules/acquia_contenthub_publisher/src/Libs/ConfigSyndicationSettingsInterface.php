<?php

namespace Drupal\acquia_contenthub_publisher\Libs;

/**
 * Defines config syndication settings.
 */
interface ConfigSyndicationSettingsInterface {

  /**
   * Toggles config syndication.
   *
   * @param bool $disabled
   *   False by default. If set to true the config syndication will be disabled.
   */
  public function toggleConfigSyndication(bool $disabled): void;

  /**
   * Checks if config syndication is disabled.
   *
   * @return bool
   *   TRUE if the syndication is disabled.
   */
  public function isConfigSyndicationDisabled(): bool;

}
