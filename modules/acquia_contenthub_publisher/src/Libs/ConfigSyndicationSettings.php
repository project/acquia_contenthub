<?php

namespace Drupal\acquia_contenthub_publisher\Libs;

/**
 * An implementation of the config syndication settings using drupal config.
 */
class ConfigSyndicationSettings extends ConfigSettingsBase implements ConfigSyndicationSettingsInterface {

  /**
   * The name of the drupal configuration.
   */
  private const CONFIG_NAME = 'acquia_contenthub_publisher.exclude_settings';

  /**
   * {@inheritdoc}
   */
  public function isConfigSyndicationDisabled(): bool {
    return $this->getConfig('exclude_config_entities', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function toggleConfigSyndication(bool $disabled): void {
    $this->setConfig('exclude_config_entities', $disabled);
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return self::CONFIG_NAME;
  }

}
