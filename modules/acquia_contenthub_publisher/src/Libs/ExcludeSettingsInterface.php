<?php

namespace Drupal\acquia_contenthub_publisher\Libs;

/**
 * Defines exclude settings.
 */
interface ExcludeSettingsInterface {

  /**
   * Excludes list of fields.
   *
   * List of these fields will be excluded from dependency calculations.
   * Therefore these fields will not be exported.
   *
   * @param string $entity_type_id
   *   Entity type Id.
   * @param string $bundle_name
   *   Bundle name.
   * @param array $field_names
   *   List of field names.
   */
  public function excludeFields(string $entity_type_id, string $bundle_name, array $field_names): void;

  /**
   * Provides list of excluded fields.
   *
   * @param string $entity_type_id
   *   Entity type Id.
   * @param string|null $bundle_name
   *   Bundle name.
   *
   * @return array
   *   List of excluded fields.
   */
  public function getExcludedFields(string $entity_type_id, string $bundle_name = NULL): array;

  /**
   * Removes excluded fields.
   *
   * @param string $entity_type_id
   *   Entity type Id.
   * @param string $bundle_name
   *   Bundle name.
   * @param array $field_names
   *   List of field names.
   *
   * @return bool
   *   TRUE if fields were removed, FALSE otherwise.
   */
  public function removeExcludedFields(string $entity_type_id, string $bundle_name, array $field_names): bool;

  /**
   * Resets value of excluded fields.
   */
  public function resetExcludedFields(): void;

}
