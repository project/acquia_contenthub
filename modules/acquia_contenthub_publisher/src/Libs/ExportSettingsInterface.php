<?php

namespace Drupal\acquia_contenthub_publisher\Libs;

/**
 * Defines export settings.
 */
interface ExportSettingsInterface {

  /**
   * Toggles manual export mode.
   *
   * @param bool $enabled
   *   False by default. If set to true, manual export mode will be enabled.
   */
  public function toggleManualExportMode(bool $enabled): void;

  /**
   * Checks if manual export mode enabled.
   *
   * @return bool
   *   TRUE if the manual export mode is enabled.
   */
  public function isManualExportModeEnabled(): bool;

}
