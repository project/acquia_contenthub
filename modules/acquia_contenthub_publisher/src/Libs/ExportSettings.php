<?php

namespace Drupal\acquia_contenthub_publisher\Libs;

/**
 * An implementation of the export settings using drupal config.
 */
class ExportSettings extends ConfigSettingsBase implements ExportSettingsInterface {

  /**
   * The name of the drupal configuration.
   */
  private const CONFIG_NAME = 'acquia_contenthub_publisher.export_settings';

  /**
   * {@inheritdoc}
   */
  public function isManualExportModeEnabled(): bool {
    return $this->getConfig('manual_export_mode', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function toggleManualExportMode(bool $enabled): void {
    $this->setConfig('manual_export_mode', $enabled);
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigName(): string {
    return self::CONFIG_NAME;
  }

}
