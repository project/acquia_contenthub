<?php

namespace Drupal\acquia_contenthub_publisher;

/**
 * Provides values for status and entity_type for Export View Tracker.
 */
class ContentHubExportTrackerViewHelper {

  /**
   * The name of the tracking table.
   */
  const EXPORT_TRACKING_TABLE = 'acquia_contenthub_publisher_export_tracking';

  /**
   * Function to return entity_types in an array.
   *
   * @return array
   *   Entity types array.
   */
  public static function getEntityTypes(): array {
    $types = [];

    $query = \Drupal::database()->select(self::EXPORT_TRACKING_TABLE, 't')
      ->fields('t', ['entity_type']);
    $results = $query->execute()->fetchAll();

    foreach ($results as $record) {
      $types[$record->entity_type] = $record->entity_type;
    }
    return $types;
  }

}
