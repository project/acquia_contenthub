<?php

namespace Drupal\acquia_contenthub_publisher\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Renders CH operations links of an entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ch_operations")
 */
class ContentHubOperations extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity_type = $values->acquia_contenthub_publisher_export_tracking_entity_type;
    $entity_uuid = $values->entity_uuid;
    return [
      '#type' => 'operations',
      '#links' => [
        'add_to_export_queue' => [
          'title' => $this->t('Add to export queue'),
          'url' => Url::fromRoute('acquia_contenthub_publisher.add_to_export_queue', [
            'entity_type' => $entity_type,
            'entity_uuid' => $entity_uuid,
          ]),
        ],
      ],
    ];
  }

}
