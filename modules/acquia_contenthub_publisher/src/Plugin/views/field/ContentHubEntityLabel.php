<?php

namespace Drupal\acquia_contenthub_publisher\Plugin\views\field;

use Drupal\Component\Utility\Unicode;
use Drupal\views\Plugin\views\field\EntityLabel;
use Drupal\views\ResultRow;

/**
 * Field handler to display entity title, linked to entity page.
 *
 * @ViewsField("ch_entity_label")
 */
class ContentHubEntityLabel extends EntityLabel {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $label = parent::render($values);
    if (is_null($label)) {
      return;
    }
    if ($label === '') {
      return $this->getValue($values);
    }
    return Unicode::truncate($label, 80, TRUE, TRUE);
  }

}
