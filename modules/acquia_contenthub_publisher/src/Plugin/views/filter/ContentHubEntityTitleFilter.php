<?php

namespace Drupal\acquia_contenthub_publisher\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Filter for Export Tracker Entity Title.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ch_entity_title_filter")
 */
class ContentHubEntityTitleFilter extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // This ensures required table exist for the view.
    $this->ensureMyTable();
    $this->opContains('label');
  }

}
