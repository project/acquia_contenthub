<?php

namespace Drupal\acquia_contenthub_publisher\Commands;

use Drupal\acquia_contenthub\Commands\AcquiaContentHubQueueCommands;
use Drupal\acquia_contenthub_publisher\ContentHubExportQueue;

/**
 * Export Queue Run Command.
 */
class AcquiaContentHubPublisherExportQueueCommands extends AcquiaContentHubQueueCommands {

  /**
   * Queue Name.
   */
  public const QUEUE_NAME = 'acquia_contenthub_publish_export';

  /**
   * Queue process.
   */
  public const QUEUE_PROCESS = 'Export queue';

  /**
   * Runs Content Hub publisher export queue.
   *
   * @option time-limit
   *    The maximum number of seconds allowed to run the queue.
   * @default time-limit
   *
   * @option items-limit
   *     The maximum number of items allowed to run the queue.
   * @default items-limit
   *
   * @usage drush acquia:contenthub-export-queue-run --uri=https://site_uri
   *   | Runs the queue.
   *
   * @command acquia:contenthub-export-queue-run
   * @aliases ach-export
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function run(): void {
    $this->runQueue();
  }

  /**
   * {@inheritDoc}
   */
  protected function getItemProcessingMessage($data): string {
    $message = '';
    if (isset($data->uuid, $data->type)) {
      $message = "Processing entity ($data->uuid, $data->type) from export queue.";
    }
    return $message;
  }

  /**
   * {@inheritDoc}
   */
  protected function getQueueName(): string {
    return ContentHubExportQueue::QUEUE_NAME;
  }

  /**
   * {@inheritDoc}
   */
  protected function getQueueProcessName(): string {
    return 'Export queue';
  }

}
