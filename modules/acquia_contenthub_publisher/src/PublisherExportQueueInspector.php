<?php

namespace Drupal\acquia_contenthub_publisher;

use Drupal\acquia_contenthub\AcquiaContentHubQueueInspectorBase;

/**
 * Provides publisher export queue data.
 */
class PublisherExportQueueInspector extends AcquiaContentHubQueueInspectorBase {

  /**
   * {@inheritDoc}
   */
  public function getQueueName(): string {
    return ContentHubExportQueue::QUEUE_NAME;
  }

}
