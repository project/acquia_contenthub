<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Unit\Libs\InterestList;

use Drupal\acquia_contenthub_subscriber\Libs\InterestList\InterestListTracker;
use Drupal\Tests\UnitTestCase;

/**
 * Tests interest list tracker.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_subscriber\Libs\InterestList\InterestListTracker
 *
 * @package acquia_contenthub_subscriber
 */
class InterestListTrackerTest extends UnitTestCase {

  /**
   * SUT.
   *
   * @var \Drupal\acquia_contenthub_subscriber\Libs\InterestList\InterestListTracker
   */
  protected InterestListTracker $sut;

  /**
   * Disabled entity uuid.
   */
  protected const DISABLED_UUID = 'uuid-1';

  /**
   * Enabled entity uuid.
   */
  protected const ENABLED_UUID = 'uuid-2';

  /**
   * Mock interests.
   *
   * @var array
   */
  protected array $interests = [
    self::DISABLED_UUID => ['disable_syndication' => TRUE],
    self::ENABLED_UUID => ['disable_syndication' => FALSE],
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->sut = new InterestListTracker();
  }

  /**
   * @covers ::setInterests
   */
  public function testSetInterests(): void {
    self::assertEmpty($this->sut->getAllInterests());
    $this->sut->setInterests($this->interests);
    self::assertContains(self::DISABLED_UUID, $this->sut->getDisabledEntities());
    self::assertContains(self::ENABLED_UUID, $this->sut->getEnabledEntities());
    self::assertEqualsCanonicalizing($this->interests, $this->sut->getAllInterests());
  }

  /**
   * @covers ::getDisabledEntities
   */
  public function testGetDisabledEntities(): void {
    $this->sut->setInterests($this->interests);
    self::assertContains(self::DISABLED_UUID, $this->sut->getDisabledEntities());
  }

  /**
   * @covers ::getEnabledEntities
   */
  public function testGetEnabledEntities(): void {
    $this->sut->setInterests($this->interests);
    self::assertContains(self::ENABLED_UUID, $this->sut->getEnabledEntities());
  }

  /**
   * @covers ::cleanup
   */
  public function testCleanup(): void {
    $this->sut->setInterests($this->interests);
    self::assertNotEmpty($this->sut->getAllInterests());
    $this->sut->cleanup();
    self::assertEmpty($this->sut->getAllInterests());
    self::assertEmpty($this->sut->getDisabledEntities());
    self::assertEmpty($this->sut->getEnabledEntities());
  }

}
