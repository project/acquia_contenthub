<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Functional;

use Drupal\acquia_contenthub_subscriber\Form\ContentHubImportQueueForm;
use Drupal\Core\Url;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\AcquiaContentHubAdminSettingsTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests user syndication form elements and interactions.
 *
 * @group acquia_contenthub_subscriber
 */
class UserSyndicationSettingsBrowserTest extends BrowserTestBase {

  use AcquiaContentHubAdminSettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'acquia_contenthub_test',
    'acquia_contenthub_server_test',
    'node',
    'user',
  ];

  /**
   * Content Hub administrator user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $chUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createAcquiaContentHubAdminSettings();
    /** @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configuration */
    $ach_configuration = $this->container->get('acquia_contenthub.configuration');
    $ach_configuration->getContentHubConfig()->disableContentHubUpdates();
    $this->chUser = $this->drupalCreateUser([
      'administer acquia content hub',
    ], 'user1');
    $this->drupalLogin($this->chUser);
  }

  /**
   * Tests if elements are displayed properly.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUiElementsAreDisplayed(): void {
    $this->drupalGet(Url::fromRoute(ContentHubImportQueueForm::ROUTE));
    $session = $this->assertSession();
    $session->elementTextEquals('css', '#edit-import-control summary', 'Import control');

    $session->elementTextEquals('css', 'label[for=edit-disable-user-syndication]', 'Disable user syndication');
    $session->checkboxNotChecked('disable_user_syndication');

    $session->fieldExists('proxy_user');

    $session->elementTextEquals('css', 'label[for=edit-disable-user-role-syndication]', 'Disable user role syndication');
    $session->checkboxNotChecked('disable_user_role_syndication');

    $session->buttonExists('Save Import Settings');
  }

  /**
   * Tests the form with different combination of submissions.
   *
   * We're doing it in one test case to save time on execution.
   */
  public function testFormSubmissionWithDifferentValues(): void {
    $user_syndication_settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');

    $this->assertNull($user_syndication_settings->getProxyUser());
    $this->assertFalse($user_syndication_settings->isUserSyndicationDisabled());
    $this->assertFalse($user_syndication_settings->isUserRoleSyndicationDisabled());

    $this->drupalGet(Url::fromRoute(ContentHubImportQueueForm::ROUTE));
    $session = $this->getSession();
    $page = $session->getPage();
    $page->checkField('disable_user_syndication');
    $page->checkField('disable_user_role_syndication');
    $page->fillField('edit-proxy-user', 'user1');
    $page->pressButton('Save Import Settings');
    $this->assertSession()->pageTextContains('Import control settings have been saved!');

    $this->rebuildContainer();
    $user_syndication_settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $user = $user_syndication_settings->getProxyUser();
    $this->assertEquals($this->chUser->id(), $user->id());
    $this->assertTrue($user_syndication_settings->isUserSyndicationDisabled());
    $this->assertTrue($user_syndication_settings->isUserRoleSyndicationDisabled());

    $this->getSession()->reload();
    $page->uncheckField('disable_user_syndication');
    $page->pressButton('Save Import Settings');

    $this->rebuildContainer();
    $user_syndication_settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $user = $user_syndication_settings->getProxyUser();
    $this->assertEquals($this->chUser->id(), $user->id());
    $this->assertFalse($user_syndication_settings->isUserSyndicationDisabled());
    $this->assertTrue($user_syndication_settings->isUserRoleSyndicationDisabled());

    $page = $this->getSession()->getPage();
    $page->fillField('edit-proxy-user', '');
    $page->uncheckField('disable_user_role_syndication');
    $page->pressButton('Save Import Settings');

    $this->rebuildContainer();
    $user_syndication_settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $user = $user_syndication_settings->getProxyUser();
    $this->assertEquals(NULL, $user);
    $this->assertFalse($user_syndication_settings->isUserSyndicationDisabled());
    $this->assertFalse($user_syndication_settings->isUserRoleSyndicationDisabled());
  }

}
