<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Kernel\Libs;

use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_subscriber\Libs\UserSyndicationSettings
 *
 * @group acquia_contenthub_subscriber
 *
 * @requires module depcalc
 */
class UserSyndicationSettingsKernelTest extends KernelTestBase {

  use UserCreationTrait;

  private const CONFIG_NAME = 'acquia_contenthub_subscriber.user_syndication';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'depcalc',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('acquia_contenthub_subscriber', SubscriberTracker::IMPORT_TRACKING_TABLE);
    $this->installEntitySchema('user');
  }

  /**
   * Tests if configuration was properly saved into database.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testSaveSavedIntoConfiguration(): void {
    $user = $this->createUser();

    $sut = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $sut->setProxyUser($user->id());
    $returned = $sut->getProxyUser();
    $this->assertEquals($user->id(), $returned->id(), 'The user returned from the user syndication settings service is the same that was set.');

    $sut->toggleUserSyndication(TRUE);
    $sut->toggleUserRoleSyndication(TRUE);
    $sut->save();

    $data = $this->config(self::CONFIG_NAME)->getRawData();
    $this->assertEquals(TRUE, $data['disable_user_syndication']);
    $this->assertEquals(TRUE, $data['disable_user_role_syndication']);
    $this->assertEquals($user->id(), $data['proxy_user']);
  }

}
