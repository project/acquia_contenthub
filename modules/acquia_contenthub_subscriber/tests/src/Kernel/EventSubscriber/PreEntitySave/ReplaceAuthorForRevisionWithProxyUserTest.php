<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Kernel\EventSubscriber\ReplaceAuthorForRevisionWithProxyUserTest;

use Drupal\acquia_contenthub\Event\PreEntitySaveEvent;
use Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\ReplaceAuthorForRevisionWithProxyUser;
use Drupal\acquia_contenthub_subscriber\Libs\UserSyndicationSettingsInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\UserInterface;

/**
 * Tests User id for revision when user syndication is enabled.
 *
 * @group acquia_contenthub_subscriber
 * @coversDefaultClass \Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\ReplaceAuthorForRevisionWithProxyUser
 *
 * @package Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave
 */
class ReplaceAuthorForRevisionWithProxyUserTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub_subscriber',
    'acquia_contenthub',
    'depcalc',
    'node',
  ];

  /**
   * Mocked user settings.
   *
   * @var \Drupal\acquia_contenthub_subscriber\Libs\UserSyndicationSettingsInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  protected $userSettings;

  /**
   * Author.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $author;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->userSettings = $this->prophesize(UserSyndicationSettingsInterface::class);
    $this->author = $this->createUser();
  }

  /**
   * Tests when user syndication is enabled and proxy is set.
   */
  public function testAuthorForRevisionOnPreEntitySave(): void {
    $proxy = $this->newReplaceAuthorForRevisionWithProxyUser([
      'proxy_user' => 10,
    ]);
    NodeType::create([
      'type' => 'test',
    ])->save();
    $node = Node::create([
      'type' => 'test',
      'title' => 'Test Node.',
      'uid' => $this->author->id(),
    ]);
    $node->save();

    $event = $this->prophesize(PreEntitySaveEvent::class);
    $event->getEntity()->willReturn($node);
    $this->assertEquals($node->getRevisionUserId(), $this->author->id(), 'Revision Author is set to original user.');
    $proxy->onPreEntitySave($event->reveal());
    $this->assertEquals($node->getRevisionUserId(), 10, 'Revision Author is changed to proxy user.');
  }

  /**
   * Creates a new ReplaceAuthorForRevisionWithProxyUser object.
   *
   * @param array $user_synd_config
   *   User sync config.
   *
   * @return \Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\ReplaceAuthorForRevisionWithProxyUser
   *   A new ReplaceAuthorForRevisionWithProxyUser object.
   */
  protected function newReplaceAuthorForRevisionWithProxyUser(array $user_synd_config = []): ReplaceAuthorForRevisionWithProxyUser {
    $user = NULL;
    if (isset($user_synd_config['proxy_user'])) {
      $user = $this->prophesize(UserInterface::class);
      $user->id()->willReturn($user_synd_config['proxy_user']);
      $user = $user->reveal();
    }
    $this->userSettings->getProxyUser()->willReturn($user);

    $user_synd = $user_synd_config['disable_user_syndication'] ?? TRUE;
    $this->userSettings->isUserSyndicationDisabled()->willReturn($user_synd);

    return new ReplaceAuthorForRevisionWithProxyUser($this->userSettings->reveal());
  }

}
