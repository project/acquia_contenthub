<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Kernel\EventSubscriber\UnserializeEntityContentField;

use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\Core\Serialization\Yaml;
use Drupal\depcalc\DependencyStack;
use Drupal\Tests\acquia_contenthub\Kernel\ImportExportTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests optional user role syndication.
 *
 * @coversDefaultClass \Drupal\acquia_contenthub_subscriber\EventSubscriber\EntityDataTamper\SkipUserAndUserRoleSyndication
 *
 * @group acquia_contenthub_subscriber
 *
 * @requires module depcalc
 */
class SkipUserRoleEntityTest extends ImportExportTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub_subscriber',
    'acquia_contenthub_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setup(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->installSchema('acquia_contenthub_subscriber', [SubscriberTracker::IMPORT_TRACKING_TABLE]);
    $role = Role::create(['id' => 'authenticated']);
    $role->save();
  }

  /**
   * Tests user role import when user role syndication is on.
   */
  public function testUserRoleImportWithoutDisablingUserRoleSyndication(): void {
    $settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $settings->toggleUserRoleSyndication(FALSE);

    $cdf_document = $this->createCdfDocumentFromFixtureFile('user/user.json');
    $cdf_serializer = $this->container->get('entity.cdf.serializer');
    $stack = new DependencyStack();
    $cdf_serializer->unserializeEntities($cdf_document, $stack);

    /** @var \Drupal\Core\Entity\EntityRepository $repository */
    $repository = $this->container->get('entity.repository');
    /** @var \Drupal\user\UserInterface $user */
    $user = $repository->loadEntityByUuid('user', 'f150c156-ef63-4f08-8d69-f15e5ee11106');
    $this->assertEquals($user->getRoles(), ['authenticated', 'ach_test_role']);

    $ach_test_role = Role::load('ach_test_role');
    $this->assertNotEmpty($ach_test_role);
  }

  /**
   * Tests user role import when user role syndication is off.
   */
  public function testUserRoleImportWithDisablingUserRoleSyndication(): void {
    $settings = $this->container->get('acquia_contenthub_subscriber.user_syndication.settings');
    $settings->toggleUserRoleSyndication(TRUE);

    $cdf_document = $this->createCdfDocumentFromFixtureFile('user/user.json');
    $cdf_serializer = $this->container->get('entity.cdf.serializer');
    $stack = new DependencyStack();
    $cdf_serializer->unserializeEntities($cdf_document, $stack);

    /** @var \Drupal\Core\Entity\EntityRepository $repository */
    $repository = $this->container->get('entity.repository');
    /** @var \Drupal\user\UserInterface $user */
    $user = $repository->loadEntityByUuid('user', 'f150c156-ef63-4f08-8d69-f15e5ee11106');
    $this->assertEquals($user->getRoles(), ['authenticated']);

    $ach_test_role = Role::load('ach_test_role');
    $this->assertNull($ach_test_role);

    // Tests when user role in metadata is set to NULL.
    $user_role_object = $cdf_document->getCdfEntity('238040d0-e70a-4d39-bed3-b3a74af87678');
    $data = Yaml::decode(base64_decode($user_role_object->getMetadata()['data']));
    foreach ($data as $lang_code => $user_role) {
      $data[$lang_code]['id'] = NULL;
    }
    $metadata['data'] = base64_encode(Yaml::encode($data));
    $user_role_object->setMetadata($metadata);
    $stack = new DependencyStack();
    $cdf_serializer->unserializeEntities($cdf_document, $stack);

    /** @var \Drupal\Core\Entity\EntityRepository $repository */
    $repository = $this->container->get('entity.repository');
    /** @var \Drupal\user\UserInterface $user */
    $user = $repository->loadEntityByUuid('user', 'f150c156-ef63-4f08-8d69-f15e5ee11106');
    $this->assertEquals($user->getRoles(), ['authenticated']);
  }

}
