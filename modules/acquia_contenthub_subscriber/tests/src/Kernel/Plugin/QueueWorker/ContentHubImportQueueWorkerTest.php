<?php

namespace Drupal\Tests\acquia_contenthub_subscriber\Kernel\Plugin\QueueWorker;

use Acquia\ContentHubClient\ContentHubClient;
use Acquia\ContentHubClient\Settings;
use Acquia\ContentHubClient\StatusCodes;
use Drupal\acquia_contenthub\Client\ClientFactory;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\KernelTests\KernelTestBase;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_subscriber\Plugin\QueueWorker\ContentHubImportQueueWorker
 *
 * @group acquia_contenthub_subscriber
 *
 * @requires module depcalc
 */
class ContentHubImportQueueWorkerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'depcalc',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('user', ['users_data']);
  }

  /**
   * Tests if the queue process halts properly, without ContentHubClient.
   */
  public function testMaintenanceModeWithoutContentHubClient(): void {
    $client = $this->prophesize(ContentHubClient::class);
    $client->getSettings()->willReturn([]);

    $this->expectException(SuspendQueueException::class);
    $this->expectExceptionMessage('Acquia Content Hub client cannot be initialized.');
    /** @var \Drupal\acquia_contenthub_subscriber\Plugin\QueueWorker\ContentHubImportQueueWorker $queue_worker */
    $queue_worker = $this->container->get('plugin.manager.queue_worker')->createInstance('acquia_contenthub_subscriber_import');
    $data = new \stdClass();
    $data->uuids = 'uuid1, uuid2, uuid3';
    $queue_worker->processItem($data);
  }

  /**
   * Tests if the queue process halts properly upon maintenance mode.
   */
  public function testMaintenanceModeInterruption(): void {
    $client = $this->prophesize(ContentHubClient::class);
    $settings = $this->prophesize(Settings::class);
    $settings->getWebhook('uuid')->willReturn('webhook-uuid');
    $client->getSettings()->willReturn($settings->reveal());
    $client->getInterestList(Argument::any(), Argument::any(), Argument::any([]))->willReturn([]);
    $client->getResponse()->willReturn(new Response(503, [], json_encode([
      'error' => [
        'code' => StatusCodes::SERVICE_UNDER_MAINTENANCE,
        'message' => 'service under maintenance',
      ],
    ])));
    $client_factory = $this->prophesize(ClientFactory::class);
    $client_factory->getClient()->willReturn($client->reveal());

    $this->container->set('acquia_contenthub.client.factory', $client_factory->reveal());

    /** @var \Drupal\acquia_contenthub_subscriber\Plugin\QueueWorker\ContentHubImportQueueWorker $queue_worker */
    $queue_worker = $this->container->get('plugin.manager.queue_worker')->createInstance('acquia_contenthub_subscriber_import');

    $this->expectException(SuspendQueueException::class);
    $this->expectExceptionMessage('Service is under maintenance, suspending import queue');
    $data = new \stdClass();
    $data->uuids = 'uuid1, uuid2, uuid3';
    $queue_worker->processItem($data);
  }

}
