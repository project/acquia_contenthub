<?php

namespace Drupal\acquia_contenthub_subscriber\Traits;

use Drupal\acquia_contenthub\QueueInspectorInterface;

/**
 * Helper functions for subscriber import queue operations.
 */
trait ImportQueueTrait {

  /**
   * Filters the incoming uuid list by the already enqueued ones.
   *
   * @param array $uuids
   *   Array of the uuids to be filtered.
   * @param \Drupal\acquia_contenthub\QueueInspectorInterface $queueInspector
   *   Queue Inspector.
   *
   * @return array
   *   List of entity uuids.
   */
  public function filterByAlreadyEnqueuedItems(array $uuids, QueueInspectorInterface $queueInspector): array {
    if (empty($uuids)) {
      return [];
    }
    $queue_data = $queueInspector->getQueueData();
    $q_data = array_column($queue_data, 'uuid');
    return array_diff($uuids, $q_data);
  }

}
