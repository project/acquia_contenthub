<?php

namespace Drupal\acquia_contenthub_subscriber;

/**
 * Defines events for the acquia_contenthub_subscriber module.
 */
final class ContentHubSubscriberEvents {

  /**
   * Event triggered while parsing CDF.
   *
   * This event allows to replace entity uuid with entity id.
   *
   * @see \Drupal\acquia_contenthub_subscriber\Event\ViewFilterPluginReplaceEntityUuidWithIdEvent
   */
  public const REPLACE_ENTITY_UUID_WITH_ID = 'acquia_contenthub_replace_entity_uuid_with_id';

}
