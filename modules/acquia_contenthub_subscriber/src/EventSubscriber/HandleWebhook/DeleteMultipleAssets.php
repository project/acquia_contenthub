<?php

namespace Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook;

use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\HandleWebhookEvent;
use Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface;
use Drupal\acquia_contenthub_subscriber\Libs\UserDeletionHandlerInterface;
use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\Component\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Deletes multiple assets based on a webhook v2.
 *
 * @package Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook
 */
class DeleteMultipleAssets implements EventSubscriberInterface {

  /**
   * The subscription tracker.
   *
   * @var \Drupal\acquia_contenthub_subscriber\SubscriberTracker
   */
  protected SubscriberTracker $tracker;

  /**
   * CH configurations.
   *
   * @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface
   */
  protected ContentHubConfigurationInterface $achConfigurations;

  /**
   * User deletion handler service.
   *
   * @var \Drupal\acquia_contenthub_subscriber\Libs\UserDeletionHandlerInterface
   */
  protected UserDeletionHandlerInterface $userDeletionHandler;

  /**
   * DeleteAssets constructor.
   *
   * @param \Drupal\acquia_contenthub_subscriber\SubscriberTracker $tracker
   *   The subscription tracker.
   * @param \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ch_configuration
   *   CH Configurations.
   * @param \Drupal\acquia_contenthub_subscriber\Libs\UserDeletionHandlerInterface $handler
   *   The user deletion handler service.
   */
  public function __construct(SubscriberTracker $tracker, ContentHubConfigurationInterface $ch_configuration, UserDeletionHandlerInterface $handler) {
    $this->tracker = $tracker;
    $this->achConfigurations = $ch_configuration;
    $this->userDeletionHandler = $handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AcquiaContentHubEvents::HANDLE_WEBHOOK][] = 'onHandleWebhook';
    return $events;
  }

  /**
   * Handles webhook events.
   *
   * @param \Drupal\acquia_contenthub\Event\HandleWebhookEvent $event
   *   The HandleWebhookEvent object.
   *
   * @throws \Exception
   */
  public function onHandleWebhook(HandleWebhookEvent $event): void {
    $payload = $event->getPayload();
    $assets = $payload['assets'] ?? [];
    $client = $event->getClient();
    $settings = $client->getSettings();

    if (!$event->isWebhookV2() || !$this->isValidRequestPayload($payload)) {
      return;
    }
    $send_update = $this->achConfigurations->getContentHubConfig()->shouldSendContentHubUpdates();
    $entity_uuids = [];
    foreach ($assets as $asset) {
      if ($this->shouldSkipAsset($asset, $settings->getUuid())) {
        continue;
      }
      $entity = $this->tracker->getEntityByRemoteIdAndHash($asset['uuid']);
      if (!$entity) {
        $entity_uuids[] = $asset['uuid'];
        continue;
      }
      $status = $this->tracker->getStatusByUuid($asset['uuid']);

      // If entity updating is disabled, delete tracking but not the entity.
      if ($status === SubscriberTracker::AUTO_UPDATE_DISABLED) {
        $this->tracker->delete('entity_uuid', $asset['uuid']);
        continue;
      }

      $entity_type_id = $entity->getEntityTypeId();
      if ($entity_type_id === 'user') {
        /** @var \Drupal\user\UserInterface $entity */
        $this->userDeletionHandler->blockUser($entity);
        continue;
      }
      if ($entity_type_id === 'user_role') {
        /** @var \Drupal\user\RoleInterface $entity */
        $this->userDeletionHandler->deleteUserRole($entity);
        continue;
      }
      $entity->delete();
    }

    // Clean up the tracker. The entity was deleted before import.
    if (!empty($entity_uuids)) {
      $this->tracker->deleteMultipleByUuids($entity_uuids);
      $webhook_uuid = $settings->getWebhook('uuid');
      if ($send_update && Uuid::isValid($webhook_uuid)) {
        $client->deleteMultipleInterest($webhook_uuid, $entity_uuids, 'subscriber');
      }
    }
  }

  /**
   * Checks if asset needs to be skipped.
   *
   * @param array $asset
   *   Payload asset.
   * @param string $client_uuid
   *   Client uuid.
   *
   * @return bool
   *   True if asset to be skipped otherwise False.
   */
  protected function shouldSkipAsset(array $asset, string $client_uuid): bool {
    return (empty($asset['origin']) || $asset['origin'] === $client_uuid ||
      !$this->isSupportedType($asset['type']));
  }

  /**
   * Determines if given entity type is supported.
   *
   * @param string $type
   *   The CDF type.
   *
   * @return bool
   *   TRUE if is supported type; FALSE otherwise.
   */
  protected function isSupportedType(string $type): bool {
    $supported_types = [
      'drupal8_content_entity',
      'drupal8_config_entity',
    ];

    return in_array($type, $supported_types, TRUE);
  }

  /**
   * Validates request payload.
   *
   * A valid payload should be successful, an event about deletion, and cannot
   * come from the same origin.
   *
   * @param array $payload
   *   The payload to validate.
   *
   * @return bool
   *   True if the payload is applicable.
   */
  protected function isValidRequestPayload(array $payload): bool {
    $assets = $payload['assets'] ?? [];
    if (!isset($payload['status'], $payload['crud'])) {
      return FALSE;
    }
    return 'successful' === $payload['status'] &&
      'delete' === $payload['crud'] &&
      !empty($assets);
  }

}
