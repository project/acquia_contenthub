<?php

namespace Drupal\acquia_contenthub_subscriber\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event to replace entity uuid with entity id while parsing cdf.
 *
 * @package Drupal\acquia_contenthub\Event
 */
class ViewFilterPluginReplaceEntityUuidWithIdEvent extends Event {

  /**
   * The filter data.
   *
   * @var array
   */
  protected $filterData;

  /**
   * The plugin id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * ViewFilterPluginReplaceEntityUuidWithIdEvent constructor.
   *
   * @param array $filter_data
   *   The filter data.
   * @param string $plugin_id
   *   The plugin id.
   */
  public function __construct(array $filter_data, string $plugin_id) {
    $this->filterData = $filter_data;
    $this->pluginId = $plugin_id;
  }

  /**
   * Provides filter data.
   *
   * @return array
   *   Filter data.
   */
  public function getFilterData(): array {
    return $this->filterData;
  }

  /**
   * Sets filter data.
   */
  public function setFilterData(array $filter_data): void {
    $this->filterData = $filter_data;
  }

  /**
   * Provides plugin id.
   *
   * @return string
   *   Plugin id.
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

}
