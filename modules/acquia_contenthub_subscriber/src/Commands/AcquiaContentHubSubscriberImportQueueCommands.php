<?php

namespace Drupal\acquia_contenthub_subscriber\Commands;

use Drupal\acquia_contenthub\Commands\AcquiaContentHubQueueCommands;
use Drupal\acquia_contenthub_subscriber\ContentHubImportQueue;

/**
 * Import Queue Run Command.
 */
class AcquiaContentHubSubscriberImportQueueCommands extends AcquiaContentHubQueueCommands {

  /**
   * Queue Name.
   */
  public const QUEUE_NAME = 'acquia_contenthub_subscriber_import';

  /**
   * Queue process.
   */
  public const QUEUE_PROCESS = 'Import queue';

  /**
   * Runs Content Hub subscriber import queue.
   *
   * @option time-limit
   *     The maximum number of seconds allowed to run the queue.
   * @default time-limit
   *
   * @option items-limit
   *      The maximum number of items allowed to run the queue.
   * @default items-limit
   *
   * @usage drush acquia:contenthub-import-queue-run --uri=https://site_uri
   *   | Runs the queue.
   *
   * @command acquia:contenthub-import-queue-run
   * @aliases ach-import
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function run(): void {
    $this->runQueue();
  }

  /**
   * {@inheritDoc}
   */
  protected function getItemProcessingMessage($data): string {
    $message = '';
    if (isset($data->uuids)) {
      $message = "Processing entities: ($data->uuids) from import queue.";
    }
    return $message;
  }

  /**
   * {@inheritDoc}
   */
  protected function getQueueName(): string {
    return ContentHubImportQueue::QUEUE_NAME;
  }

  /**
   * {@inheritDoc}
   */
  protected function getQueueProcessName(): string {
    return 'Import queue';
  }

}
