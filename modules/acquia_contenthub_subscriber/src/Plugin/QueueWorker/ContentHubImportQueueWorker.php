<?php

namespace Drupal\acquia_contenthub_subscriber\Plugin\QueueWorker;

use Acquia\ContentHubClient\Syndication\SyndicationEvents;
use Acquia\ContentHubClient\Syndication\SyndicationStatus;
use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Client\CdfMetricsManager;
use Drupal\acquia_contenthub\Client\ClientFactory;
use Drupal\acquia_contenthub\Event\Queue\QueueItemProcessFinishedEvent;
use Drupal\acquia_contenthub\Libs\InterestList\InterestListStorageInterface;
use Drupal\acquia_contenthub\Libs\InterestList\InterestListTrait;
use Drupal\acquia_contenthub\Libs\Logging\ContentHubLoggerInterface;
use Drupal\acquia_contenthub\Libs\Traits\ResponseCheckerTrait;
use Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface;
use Drupal\acquia_contenthub_subscriber\CdfImporter;
use Drupal\acquia_contenthub_subscriber\Exception\ContentHubImportException;
use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\depcalc\DependencyStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Queue worker for importing entities.
 *
 * @QueueWorker(
 *   id = "acquia_contenthub_subscriber_import",
 *   title = "Queue Worker to import entities from contenthub."
 * )
 */
class ContentHubImportQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use InterestListTrait;
  use ResponseCheckerTrait;

  /**
   * The role of the site.
   */
  protected const SITE_ROLE = 'subscriber';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The CDF importer object.
   *
   * @var \Drupal\acquia_contenthub_subscriber\CdfImporter
   */
  protected $importer;

  /**
   * The Content Hub Client.
   *
   * @var \Acquia\ContentHubClient\ContentHubClient
   */
  protected $client;

  /**
   * The Subscriber Tracker.
   *
   * @var \Drupal\acquia_contenthub_subscriber\SubscriberTracker
   */
  protected $tracker;

  /**
   * CH configurations.
   *
   * @var \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface
   */
  protected ContentHubConfigurationInterface $achConfigurations;

  /**
   * Content Hub logger.
   *
   * @var \Drupal\acquia_contenthub\Libs\Logging\ContentHubLoggerInterface
   */
  protected $chLogger;

  /**
   * Cdf Metrics Manager.
   *
   * @var \Drupal\acquia_contenthub\Client\CdfMetricsManager
   */
  protected $cdfMetricsManager;

  /**
   * The interest list storage.
   *
   * @var \Drupal\acquia_contenthub\Libs\InterestList\InterestListStorageInterface
   */
  protected InterestListStorageInterface $interestListStorage;

  /**
   * ContentHubExportQueueWorker constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   Dispatcher.
   * @param \Drupal\acquia_contenthub_subscriber\CdfImporter $cdf_importer
   *   Cdf Importer.
   * @param \Drupal\acquia_contenthub\Client\ClientFactory $client_factory
   *   The client factory.
   * @param \Drupal\acquia_contenthub_subscriber\SubscriberTracker $tracker
   *   The Subscriber Tracker.
   * @param \Drupal\acquia_contenthub\Settings\ContentHubConfigurationInterface $ach_configurations
   *   CH configurations.
   * @param \Drupal\acquia_contenthub\Libs\Logging\ContentHubLoggerInterface $ch_logger
   *   Content Hub logger service.
   * @param \Drupal\acquia_contenthub\Client\CdfMetricsManager $cdf_metrics_manager
   *   Cdf metrics manager.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\acquia_contenthub\Libs\InterestList\InterestListStorageInterface $interest_storage
   *   The interest list storage.
   *
   * @throws \Exception
   */
  public function __construct(EventDispatcherInterface $dispatcher, CdfImporter $cdf_importer, ClientFactory $client_factory, SubscriberTracker $tracker, ContentHubConfigurationInterface $ach_configurations, ContentHubLoggerInterface $ch_logger, CdfMetricsManager $cdf_metrics_manager, array $configuration, $plugin_id, $plugin_definition, InterestListStorageInterface $interest_storage) {
    $this->importer = $cdf_importer;
    if (!empty($this->importer->getUpdateDbStatus())) {
      throw new \Exception("Site has pending database updates. Apply these updates before importing content.");
    }
    $this->dispatcher = $dispatcher;
    $this->client = $client_factory->getClient();
    $this->tracker = $tracker;
    $this->achConfigurations = $ach_configurations;
    $this->chLogger = $ch_logger;
    $this->cdfMetricsManager = $cdf_metrics_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->interestListStorage = $interest_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('acquia_contenthub_subscriber.cdf_importer'),
      $container->get('acquia_contenthub.client.factory'),
      $container->get('acquia_contenthub_subscriber.tracker'),
      $container->get('acquia_contenthub.configuration'),
      $container->get('acquia_contenthub_subscriber.ch_logger'),
      $container->get('acquia_contenthub.cdf_metrics_manager'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('acquia_contenthub.interest_list_storage')
    );
  }

  /**
   * Processes acquia_contenthub_subscriber_import queue items.
   *
   * @param mixed $data
   *   The data in the queue.
   *
   * @throws \Exception
   */
  public function processItem($data): void {
    $process_items = explode(', ', $data->uuids);
    $result = $this->doProcess($process_items, $data->filter_uuid ?? NULL);
    $event = new QueueItemProcessFinishedEvent($this->getPluginId(), $process_items, $result);
    $this->dispatcher->dispatch($event, AcquiaContentHubEvents::QUEUE_ITEM_PROCESS_FINISHED);
  }

  /**
   * Executes the actual processing of items.
   *
   * @param array $process_items
   *   The uuids to process.
   * @param string|null $filter_uuid
   *   The filter uuid of the queue item if there's one.
   *
   * @return bool
   *   True if the processing was successful.
   *
   * @throws \Drupal\acquia_contenthub_subscriber\Exception\ContentHubImportException
   */
  protected function doProcess(array $process_items, ?string $filter_uuid): bool {
    if (!$this->client) {
      $this->chLogger->getChannel()->error('Acquia Content Hub client cannot be initialized.');
      throw new SuspendQueueException('Acquia Content Hub client cannot be initialized.');
    }

    $settings = $this->client->getSettings();
    $webhook = $settings->getWebhook('uuid');
    if (!$webhook) {
      return FALSE;
    }

    $interests = $this->getInterestList($webhook, $process_items);
    $interest_response = $this->client->getResponse();
    if (!$this->isSuccessful($interest_response)) {
      $msg = sprintf('Error during fetching interest lists: %s', $interest_response->getBody());
      if ($this->isMaintenanceError($interest_response)) {
        $msg = 'Service is under maintenance, suspending import queue';
      }
      throw new SuspendQueueException($msg);
    }

    if (is_null($interests)) {
      return FALSE;
    }

    $uuids = $this->filterDeletedItems($process_items, array_keys($interests));
    $this->logMissingUuids($uuids, $process_items);
    if (!$uuids) {
      return FALSE;
    }

    $stack = $this->importEntities($uuids, $webhook);
    if (!$stack) {
      return FALSE;
    }

    if (!$this->shouldSendUpdate()) {
      return TRUE;
    }
    $uuids = $this->filterDisabledEntities($webhook, $uuids);
    $this->updateInterestList($webhook, $uuids, SyndicationStatus::IMPORT_SUCCESSFUL);

    $this->addDependenciesToInterestList(
      $webhook,
      $this->filterDisabledEntities($webhook, array_keys($stack->getDependencies())),
      $filter_uuid
    );

    return TRUE;
  }

  /**
   * Returns the interest list related to webhook.
   *
   * @param string $webhook_uuid
   *   Webhook uuid.
   * @param array $process_items
   *   Array of uuids to be processed.
   *
   * @return array
   *   List of entity uuids.
   */
  protected function getInterestList(string $webhook_uuid, array $process_items): ?array {
    try {
      return $this->interestListStorage->getInterestList($webhook_uuid, static::SITE_ROLE, ['uuids' => $process_items]);
    }
    catch (\Exception $exception) {
      $this->chLogger->getChannel()->error(
        sprintf(
          'Following error occurred while we were trying to get the interest list: %s',
          $exception->getMessage()
        )
      );
    }
    return NULL;
  }

  /**
   * Imports entities and logs exceptions.
   *
   * @param array $uuids
   *   The entities to import.
   * @param string $webhook
   *   The webhook which the interest list belongs to.
   *
   * @return \Drupal\depcalc\DependencyStack|null
   *   Dependency stack if the process was successful.
   *
   * @throws \Drupal\acquia_contenthub_subscriber\Exception\ContentHubImportException
   */
  protected function importEntities(array $uuids, string $webhook): ?DependencyStack {
    try {
      $stack = $this->importer->importEntities(...$uuids);
      $this->cdfMetricsManager->sendClientCdfUpdates();

      return $stack;
    }
    catch (ContentHubImportException $e) {
      $e_uuids = $e->getUuids();
      $deletable = !array_diff($uuids, $e_uuids) && $e->isEntitiesMissing();
      $note = $e->getCode() === ContentHubImportException::MISSING_ENTITIES ?
        'Check export table, nullify hashes of entities marked as missing and try to re-export original entity/entities.' : 'N/A';
      if (!$deletable) {
        if ($this->shouldSendUpdate()) {
          $event_ref = $this->chLogger->logError('Entity uuids: "{uuids}". Error: "{error}". Note: {note}',
            [
              'uuids' => implode(',', $e_uuids),
              'error' => $e->getMessage(),
              'note' => $note,
            ]
          );
          $uuids = $this->filterDisabledEntities($webhook, $uuids);
          $this->updateInterestList($webhook, $uuids, SyndicationStatus::IMPORT_FAILED, $event_ref);
        }
        else {
          // There are import problems but probably on dependent entities.
          $this->chLogger->getChannel()
            ->error(sprintf('Import failed: %s.', $e->getMessage()));
        }

        $triggering_uuids = $e->getTriggeringUuids();
        array_walk($triggering_uuids, function (&$msg) use ($note) {
          $msg = sprintf('%s Note: %s', $msg, $note);
        });

        $this->chLogger->getEvent()->logMultipleEntityEvents(
          SyndicationEvents::IMPORT_FAILURE['severity'],
          $triggering_uuids,
          SyndicationEvents::IMPORT_FAILURE['name']
        );

        throw $e;
      }
      // The UUIDs can't be imported since they aren't in the Service.
      // The missing UUIDs are the same as the ones that were sent for import.
      foreach ($uuids as $uuid) {
        $this->deleteFromTrackingTableAndInterestList($uuid, $webhook);
      }
    }
    catch (\Exception $default_exception) {
      $this->chLogger->logError(
        'Error: {error}',
        ['error' => $default_exception->getMessage()]
      );

      throw $default_exception;
    }

    return NULL;
  }

  /**
   * Updates interest list.
   *
   * @param string $webhook
   *   The webhook uuid.
   * @param array $uuids
   *   Entity uuids to update.
   * @param string $status
   *   The status of the syndication.
   * @param string|null $event_ref
   *   The event reference.
   */
  protected function updateInterestList(string $webhook, array $uuids, string $status, ?string $event_ref = NULL): void {
    $interest_list = $this->buildInterestList($uuids, $status, NULL, $event_ref);
    try {
      $this->client->updateInterestListBySiteRole(
        $webhook,
        static::SITE_ROLE,
        $interest_list
      );
    }
    catch (\Exception $e) {
      $this->chLogger->getChannel()->error(
        sprintf('Could not update interest list. Reason: %s', $e->getMessage())
      );
    }
  }

  /**
   * Deletes entity from tracking table and interest list.
   *
   * @param string $uuid
   *   The uuid of the entity.
   * @param string $webhook
   *   The webhook the entity is related to.
   */
  protected function deleteFromTrackingTableAndInterestList(string $uuid, string $webhook): void {
    try {
      if ($this->tracker->getEntityByRemoteIdAndHash($uuid)) {
        return;
      }
      // If we cannot load, delete interest and tracking record.
      if ($this->shouldSendUpdate()) {
        $this->client->deleteInterest($uuid, $webhook);
      }
      $this->tracker->delete('entity_uuid', $uuid);
      $this->chLogger->getChannel()->info(
        sprintf(
          'The following entity was deleted from interest list and tracking table: %s',
          $uuid
        )
      );
    }
    catch (\Exception $ex) {
      $this->chLogger->getChannel()
        ->error(sprintf(
          'Entity deletion from tracking table and interest list failed. Entity: %s. Message: %s',
          $uuid,
          $ex->getMessage()
        ));
    }
  }

  /**
   * Filters potentially deleted items.
   *
   * @param array $process_items
   *   The entities being processed.
   * @param array $interests
   *   The list of entity uuids.
   *
   * @return array
   *   Filtered list of entity uuids.
   */
  protected function filterDeletedItems(array $process_items, array $interests): array {
    $uuids = array_intersect($process_items, $interests);
    if (!$uuids) {
      $this->chLogger->getChannel()
        ->info('There are no matching entities in the queues and the site interest list.');
    }
    return $uuids;
  }

  /**
   * Logs the uuids no longer on the interest list for this webhook.
   *
   * @param array $uuids
   *   Entity uuids.
   * @param array $process_items
   *   The entities being processed.
   */
  protected function logMissingUuids(array $uuids, array $process_items): void {
    if (count($uuids) !== count($process_items)) {
      $missing_uuids = array_diff($process_items, $uuids);
      $this->chLogger->getChannel()->info(
          sprintf(
            'Skipped importing the following missing entities: %s. This occurs when entities are deleted at the Publisher before importing.',
            implode(', ', $missing_uuids))
        );
    }
  }

  /**
   * Adds dependencies to interest list.
   *
   * @param string $webhook
   *   The webhook to assign entities to.
   * @param array $uuids
   *   The entity uuids to add.
   * @param string|null $filter_uuid
   *   Filter uuid to include in the reason if there's one.
   */
  protected function addDependenciesToInterestList(string $webhook, array $uuids, ?string $filter_uuid): void {
    $interest_list = $this->buildInterestList(
      $uuids,
      SyndicationStatus::IMPORT_SUCCESSFUL,
      $filter_uuid ?? 'manual'
    );
    try {
      $this->client->addEntitiesToInterestListBySiteRole($webhook, static::SITE_ROLE, $interest_list);
      $this->chLogger->getChannel()->info(
        sprintf(
          'The following imported entities have been added to the interest list on Content Hub for webhook "%s": [%s].',
          $webhook,
          implode(', ', $uuids)
        )
      );
    }
    catch (\Exception $e) {
      $this->chLogger->getChannel()->error(
        sprintf(
          'Error adding the following entities to the interest list for webhook "%s": [%s]. Error message: "%s".',
          $webhook,
          implode(', ', $uuids),
          $e->getMessage()
        )
      );
    }
  }

  /**
   * Returns send_contenthub_updates config value.
   *
   * @return bool
   *   Setting of the configuration.
   */
  protected function shouldSendUpdate(): bool {
    return $this->achConfigurations->getContentHubConfig()->shouldSendContentHubUpdates();
  }

  /**
   * Filters uuids of disabled entities.
   *
   * @param string $webhook_uuid
   *   Webhook uuid.
   * @param array $uuids
   *   Source uuids.
   *
   * @return array
   *   List of entity uuids without disabled entity uuids.
   */
  protected function filterDisabledEntities(string $webhook_uuid, array $uuids): array {
    $disabled_entities = $this->interestListStorage->getInterestList($webhook_uuid, 'subscriber', [
      'uuids' => $uuids,
      'disable_syndication' => TRUE,
    ]);
    if (empty($disabled_entities)) {
      return $uuids;
    }
    $disabled_entities = array_keys($disabled_entities);
    return array_values(array_diff($uuids, $disabled_entities));
  }

}
