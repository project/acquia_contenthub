<?php

namespace Drupal\acquia_contenthub_subscriber\Libs;

use Drupal\user\UserInterface;

/**
 * Defines a user syndication settings operator.
 */
interface UserSyndicationSettingsInterface {

  /**
   * Toggles user syndication.
   *
   * @param bool $disabled
   *   False by default. If set to true the user syndication will be disabled.
   */
  public function toggleUserSyndication(bool $disabled): void;

  /**
   * Toggles user role syndication.
   *
   * @param bool $disabled
   *   FALSE by default. If set to true the user role syndication will be
   *   disabled.
   */
  public function toggleUserRoleSyndication(bool $disabled): void;

  /**
   * Sets proxy user.
   *
   * Proxy user is used to author contents which arrive to the site. It can
   * only be used if the user syndication is disabled.
   *
   * @param int $id
   *   The id of the user.
   */
  public function setProxyUser(int $id): void;

  /**
   * Returns the proxy user or null if not set.
   *
   * @return \Drupal\user\UserInterface|null
   *   The proxy user or null.
   */
  public function getProxyUser(): ?UserInterface;

  /**
   * Sets proxy user to NULL.
   */
  public function resetProxyUser(): void;

  /**
   * Checks if user syndication is disabled.
   *
   * @return bool
   *   TRUE if the syndication is disabled.
   */
  public function isUserSyndicationDisabled(): bool;

  /**
   * Checks if user role syndication is disabled.
   *
   * @return bool
   *   TRUE if the role syndication is disabled.
   */
  public function isUserRoleSyndicationDisabled(): bool;

  /**
   * Persists current state.
   */
  public function save(): void;

}
