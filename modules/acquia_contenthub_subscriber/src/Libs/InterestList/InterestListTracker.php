<?php

namespace Drupal\acquia_contenthub_subscriber\Libs\InterestList;

/**
 * Interest list tracker.
 *
 * Keeps track of disabled/enabled entities in interest list.
 */
class InterestListTracker {

  /**
   * Entities that are disabled for syndication for this webhook.
   *
   * @var array
   */
  protected array $disabledEntities = [];

  /**
   * Entities that are enabled for syndication for this webhook.
   *
   * @var array
   */
  protected array $enabledEntities = [];

  /**
   * Sets interest list items in enabled and disabled entities.
   *
   * @param array $interest_list
   *   Interest list items.
   */
  public function setInterests(array $interest_list): void {
    foreach ($interest_list as $uuid => $interest_data) {
      if (isset($interest_data['disable_syndication']) && $interest_data['disable_syndication'] === TRUE) {
        $this->disabledEntities[$uuid] = $interest_data;
      }
      else {
        $this->enabledEntities[$uuid] = $interest_data;
      }
    }
  }

  /**
   * Returns uuids of disabled entities.
   *
   * @return array
   *   Disabled entities uuids.
   */
  public function getDisabledEntities(): array {
    return array_keys($this->disabledEntities);
  }

  /**
   * Returns uuids of enabled entities.
   *
   * @return array
   *   Enabled entities uuids.
   */
  public function getEnabledEntities(): array {
    return array_keys($this->enabledEntities);
  }

  /**
   * Returns all entities in interest list.
   *
   * @return array
   *   All interest items.
   */
  public function getAllInterests(): array {
    return array_merge($this->enabledEntities, $this->disabledEntities);
  }

  /**
   * Cleans up tracked enabled & disabled interests.
   */
  public function cleanup(): void {
    $this->disabledEntities = [];
    $this->enabledEntities = [];
  }

}
