<?php

namespace Drupal\acquia_contenthub_subscriber\Libs;

use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Defines the behavior of a user remover object.
 */
interface UserDeletionHandlerInterface {

  /**
   * Blocks the specified user.
   *
   * When the user is the subject of a delete event (from Content Hub Service),
   * the publisher sends the delete request to Content Hub.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to block.
   */
  public function blockUser(UserInterface $user): void;

  /**
   * Deletes user role according to user syndication settings.
   *
   * If user role syndication is disabled, user roles should not be deleted.
   *
   * @param \Drupal\user\RoleInterface $role
   *   The user role to delete.
   */
  public function deleteUserRole(RoleInterface $role): void;

}
