services:

  acquia_contenthub_subscriber.tracker:
    class: Drupal\acquia_contenthub_subscriber\SubscriberTracker
    arguments: ['@database']

  acquia_contenthub_subscriber.logger_channel:
    class: Drupal\Core\Logger\LoggerChannel
    factory: logger.factory:get
    arguments: ['acquia_contenthub_subscriber']

  acquia_contenthub_subscriber.handle_webhook.import_assets:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\ImportUpdateAssets
    arguments: ['@queue', '@acquia_contenthub_subscriber.tracker', '@acquia_contenthub_subscriber.logger_channel', '@acquia_contenthub.configuration', '@acquia_contenthub.cdf_metrics_manager', '@acquia_contenthub_subscriber.import_queue_inspector', '@acquia_contenthub.interest_list_storage']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.handle_webhook.import_multiple_assets:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\ImportUpdateMultipleAssets
    arguments: ['@queue', '@acquia_contenthub_subscriber.tracker', '@acquia_contenthub_subscriber.logger_channel', '@acquia_contenthub.configuration', '@acquia_contenthub.cdf_metrics_manager', '@acquia_contenthub_subscriber.import_queue_inspector', '@acquia_contenthub.interest_list_storage']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.handle_webhook.dump_assets:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\DumpAssets
    arguments: ['@acquia_contenthub_subscriber.tracker', '@entity_type.manager', '@acquia_contenthub_common_actions']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.handle_webhook.import_delete_assets:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\DeleteAssets
    arguments: ['@acquia_contenthub_subscriber.tracker', '@acquia_contenthub.configuration', '@acquia_contenthub_subscriber.user_deletion_handler']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.handle_webhook.import_delete_multiple_assets:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\DeleteMultipleAssets
    arguments: ['@acquia_contenthub_subscriber.tracker', '@acquia_contenthub.configuration', '@acquia_contenthub_subscriber.user_deletion_handler']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.handle_webhook.purge:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\HandleWebhook\Purge
    arguments: ['@queue', '@acquia_contenthub.logger_channel', '@acquia_contenthub.cdf_metrics_manager']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.data_tamper.disabled_entity:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\EntityDataTamper\DisabledEntity
    arguments: ['@acquia_contenthub_subscriber.tracker', '@acquia_contenthub.interest_list_storage', '@acquia_contenthub.configuration']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.load_local_entity.from_tracker:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\LoadLocalEntity\LoadFromTrackingData
    arguments: ['@acquia_contenthub_subscriber.tracker']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.load_local_entity.taxonomy_term:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\LoadLocalEntity\TaxonomyTermMatch
    arguments: ['@entity_type.manager']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.load_local_entity.load_matching_redirect:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\LoadLocalEntity\LoadMatchingRedirect
    tags:
    - { name: event_subscriber }

  acquia_contenthub_subscriber.load_local_entity.load_matching_pathalias:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\LoadLocalEntity\LoadMatchingPathAlias
    arguments: ['@entity_type.manager', '@config.factory', '@acquia_contenthub_subscriber.logger_channel']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.entity_import.track:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\EntityImport\TrackEntity
    arguments: ['@acquia_contenthub_subscriber.tracker']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.parse_cdf.file:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\ParseCdf\File
    arguments: [ '@acquia_contenthub.file_scheme_handler.manager', '@acquia_contenthub_subscriber.tracker', '@acquia_contenthub_subscriber.logger_channel' ]
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.acquia_contenthub_import_queue:
    class: Drupal\acquia_contenthub_subscriber\ContentHubImportQueue
    arguments: ['@queue', '@plugin.manager.queue_worker', '@messenger']

  acquia_contenthub_subscriber.acquia_contenthub_import_queue_by_filter:
    class: Drupal\acquia_contenthub_subscriber\ContentHubImportQueueByFilter
    arguments: ['@queue', '@plugin.manager.queue_worker', '@messenger']

  acquia_contenthub_subscriber.promote_entity_status_tracking.tracking_totals:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\PromoteEntityStatusTrack\TrackTotals
    arguments: ['@acquia_contenthub_subscriber.tracker']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.prevent_exporting_imported_entities_if_has_dual_config:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\PrunePublishEntities\PruneImportedEntitiesFromExport
    arguments: ['@acquia_contenthub_subscriber.tracker', '@pub.sub_status.checker', '@entity.repository']
    tags:
      - { name: event_subscriber }

  acquia_contenthub.config_entity_null_uuid.pre_entity_save:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\ConfigEntityWithNullUuid
    tags:
      - { name: event_subscriber }

  acquia_contenthub.entity_subqueue.pre_entity_save:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\EntitySubqueuePreSave
    arguments: ['@database']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.ch_logger:
    class: Drupal\acquia_contenthub_subscriber\ContentHubImportLogger
    arguments: [ '@acquia_contenthub_subscriber.logger_channel', '@acquia_contenthub.event_logger']

  acquia_contenthub_subscriber.cdf_importer:
    class: Drupal\acquia_contenthub_subscriber\CdfImporter
    arguments: [ '@event_dispatcher', '@entity.cdf.serializer', '@acquia_contenthub.client.factory', '@acquia_contenthub_subscriber.logger_channel', '@acquia_contenthub_subscriber.tracker' ]

  acquia_contenthub_subscriber.requeuer:
    class: Drupal\acquia_contenthub_subscriber\SubscriberRequeuer
    arguments: ['@acquia_contenthub_subscriber.tracker', '@acquia_contenthub.client.factory', '@event_dispatcher', '@cache.depcalc', '@acquia_contenthub_subscriber.logger_channel', '@entity_type.manager']

  acquia_contenthub_subscriber.import_queue_inspector:
    class: Drupal\acquia_contenthub_subscriber\SubscriberImportQueueInspector
    arguments: [ '@queue', '@database', '@acquia_contenthub.client.factory', '@acquia_contenthub.logger_channel' ]

  acquia_contenthub_subscriber.parse_view_filters_handler:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\ParseCdf\ViewFiltersParseCdfHandler
    arguments: ['@event_dispatcher']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.taxonomy_term_id:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\ReplaceEntityUuidWithId\TaxonomyIndexIdViewFilterPlugin
    arguments: ['@entity_type.manager', '@acquia_contenthub_subscriber.logger_channel']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.numeric:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\ReplaceEntityUuidWithId\NumericViewFilterPlugin
    arguments: ['@entity_type.manager', '@acquia_contenthub_subscriber.logger_channel']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.user_syndication.settings:
    class: Drupal\acquia_contenthub_subscriber\Libs\UserSyndicationSettings
    arguments: ['@entity_type.manager', '@config.factory']

  acquia_contenthub_subscriber.replace_author_with_proxy_user:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\UnserializeContentEntityField\ReplaceAuthorWithProxyUser
    arguments: ['@acquia_contenthub_subscriber.user_syndication.settings']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.pre_entity_save:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\PreEntitySave\ReplaceAuthorForRevisionWithProxyUser
    arguments: [ '@acquia_contenthub_subscriber.user_syndication.settings' ]
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.user_deletion_handler:
    class: Drupal\acquia_contenthub_subscriber\Libs\UserDeletionHandler
    arguments:
      [ '@acquia_contenthub_subscriber.user_syndication.settings', '@acquia_contenthub_subscriber.logger_channel' ]

  acquia_contenthub_subscriber.skip_user_and_user_role_syndication:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\EntityDataTamper\SkipUserAndUserRoleSyndication
    arguments: [ '@acquia_contenthub_subscriber.user_syndication.settings', '@entity_type.manager', '@acquia_contenthub_subscriber.logger_channel', '@entity.repository']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.skip_tag_invalidation_on_subscriber:
    class: Drupal\acquia_contenthub_subscriber\EventSubscriber\InvalidateDepcalcCache\SkipDepcalcInvalidationOnSubscriberSites
    arguments: [ '@pub.sub_status.checker']
    tags:
      - { name: event_subscriber }

  acquia_contenthub_subscriber.interest_list_tracker:
    class: Drupal\acquia_contenthub_subscriber\Libs\InterestList\InterestListTracker

  acquia_contenthub_subscriber.interest_list_handler:
    class: Drupal\acquia_contenthub_subscriber\Libs\InterestList\InterestListHandler
    arguments: ['@acquia_contenthub_subscriber.tracker', '@entity_type.manager', '@acquia_contenthub.client.factory', '@acquia_contenthub_subscriber.logger_channel']
