<?php

namespace Drupal\Tests\acquia_contenthub_translations\Kernel;

use Drupal\acquia_contenthub_translations\EntityHandler\HandlerRegistry;
use Drupal\acquia_contenthub_translations\Form\ContentHubTranslationsSettingsForm;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests updates to old handler configurations.
 *
 * @requires module depcalc
 *
 * @group acquia_contenthub_translations
 *
 * @package Drupal\Tests\acquia_contenthub_translations\Kernel
 */
class UpdateOldHandlerConfigurationsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'depcalc',
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'acquia_contenthub_translations',
  ];

  /**
   * Tests databases update is run successfully.
   *
   * @covers ::acquia_contenthub_translations_update_82001
   */
  public function testDbUpdatesAreSuccessful(): void {
    $config = $this->config(ContentHubTranslationsSettingsForm::CONFIG);
    $config
      ->set('selective_language_import', TRUE)
      ->set('override_translation', FALSE)
      ->save();
    $this->assertNull($config->get(HandlerRegistry::OVERRIDDEN_HANDLERS_CONFIG_KEY));
    $this->assertNull($config->get(HandlerRegistry::ENTITY_HANDLERS_CONFIG_KEY));
    $this->assertNull($config->get(HandlerRegistry::UNSPECIFIED_CONFIG_KEY));
    $this->container->get('module_handler')->loadInclude('acquia_contenthub_translations', 'install');
    $sandbox = [];
    acquia_contenthub_translations_update_82001($sandbox);
    $updated_config = $this->config(ContentHubTranslationsSettingsForm::CONFIG);
    $this->assertIsArray($updated_config->get(HandlerRegistry::OVERRIDDEN_HANDLERS_CONFIG_KEY));
    $this->assertIsArray($updated_config->get(HandlerRegistry::ENTITY_HANDLERS_CONFIG_KEY));
    $this->assertIsArray($updated_config->get(HandlerRegistry::UNSPECIFIED_CONFIG_KEY));
  }

}
