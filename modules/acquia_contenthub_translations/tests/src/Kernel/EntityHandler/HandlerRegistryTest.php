<?php

namespace Drupal\Tests\acquia_contenthub_translations\Kernel\EntityHandler;

use Drupal\acquia_contenthub_translations\EntityHandler\HandlerRegistry;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\acquia_contenthub_translations\EntityHandler\HandlerRegistry
 *
 * @requires module depcalc
 *
 * @group acquia_contenthub_translations
 *
 * @package Drupal\Tests\acquia_contenthub_translations\Kernel
 */
class HandlerRegistryTest extends KernelTestBase {

  /**
   * System under test.
   *
   * @var \Drupal\acquia_contenthub_translations\EntityHandler\HandlerRegistry
   */
  protected $sut;

  /**
   * The acquia_contenthub_translations config object used by the registry.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'acquia_contenthub_translations',
    'depcalc',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->config = $this->container->get('acquia_contenthub_translations.config');
    $this->sut = $this->container->get('acquia_contenthub_translations.nt_entity_handler.registry');
  }

  /**
   * @covers ::getHandlerIdFor
   */
  public function testGetHandlerIdForDefaultEntities(): void {
    // Default configuration.
    $this->assertEquals('flexible',
      $this->sut->getHandlerIdFor('file', 'file')
    );
    $this->assertEquals('removable',
      $this->sut->getHandlerIdFor('path_alias', 'path_alias')
    );
    $this->assertEquals('removable',
      $this->sut->getHandlerIdFor('redirect', 'redirect')
    );
    $this->assertEquals('unspecified',
      $this->sut->getHandlerIdFor('unspecified', 'unspecified')
    );
  }

  /**
   * @covers ::getUnspecified
   * @covers ::addToUnspecified
   */
  public function testUnspecifiedList(): void {
    $this->sut->addToUnspecified('random_entity', 'random_bundle');

    $this->assertEquals('unspecified',
      $this->sut->getHandlerIdFor('random_entity', 'random_bundle')
    );

    $list = $this->config->get(HandlerRegistry::UNSPECIFIED_CONFIG_KEY);
    $this->assertTrue(isset($list['random_entity:random_bundle']));

    $this->assertTrue($this->sut->isUnspecified('random_entity', 'random_bundle'));
  }

  /**
   * @covers ::getHandlerMapping
   * @covers ::addEntityToRegistry
   */
  public function testEntityHandlerMapping(): void {
    $this->sut->addEntityToRegistry('an_entity', 'a_bundle', 'removable');

    $this->assertEquals('removable',
      $this->sut->getHandlerIdFor('an_entity', 'a_bundle')
    );

    $list = $this->config->get(HandlerRegistry::ENTITY_HANDLERS_CONFIG_KEY);
    $this->assertEquals('removable', $list['an_entity:a_bundle']);
  }

}
