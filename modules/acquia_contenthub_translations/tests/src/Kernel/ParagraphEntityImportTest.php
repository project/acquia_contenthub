<?php

namespace Drupal\Tests\acquia_contenthub_translations\Kernel;

use Acquia\ContentHubClient\ContentHubClient;
use Acquia\ContentHubClient\Settings;
use Drupal\acquia_contenthub\Client\ClientFactory;
use Drupal\acquia_contenthub_subscriber\SubscriberTracker;
use Drupal\acquia_contenthub_translations\Data\EntityTranslations;
use Drupal\acquia_contenthub_translations\Data\EntityTranslationsTracker;
use Drupal\acquia_contenthub_translations\Exceptions\TranslationDataException;
use Drupal\acquia_contenthub_translations\Form\ContentHubTranslationsSettingsForm;
use Drupal\depcalc\DependencyStack;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\acquia_contenthub\Kernel\Traits\CdfDocumentCreatorTrait;
use Prophecy\Argument;

/**
 * Tests the single entity translation import.
 *
 * @requires module depcalc
 * @requires module paragraphs
 * @requires module entity_reference_revisions
 *
 * @group acquia_contenthub_translations
 *
 * @package Drupal\Tests\acquia_contenthub_translations\Kernel
 */
class ParagraphEntityImportTest extends EntityKernelTestBase {

  use CdfDocumentCreatorTrait;

  /**
   * Entity cdf serializer.
   *
   * @var \Drupal\acquia_contenthub\EntityCdfSerializer
   */
  protected $cdfSerializer;

  /**
   * Undesired language registrar.
   *
   * @var \Drupal\acquia_contenthub_translations\UndesiredLanguageRegistry\UndesiredLanguageRegistryInterface
   */
  protected $undesiredLanguageRegistrar;

  /**
   * Content Hub client factory.
   *
   * @var \Drupal\acquia_contenthub\Client\ClientFactory
   */
  protected $factory;

  /**
   * Content hub client mock.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $client;

  /**
   * Content Hub client settings.
   *
   * @var \Acquia\ContentHubClient\Settings
   */
  protected $settings;

  /**
   * Fixtures to import.
   *
   * @var array
   */
  protected $fixtures = [
    // Node & paragraph has 2 languages - hi(default) & en.
    0 => 'node/node-with-paragraph-translation.json',
    // Node has 2 & new paragraph has 1 language - hi(default) &en.
    1 => 'node/node-with-paragraph-no-translation.json',
    // Newly added paragraph - hi.
    2 => 'node/paragraph-no-translation.json',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'node',
    'field',
    'depcalc',
    'acquia_contenthub_test',
    'acquia_contenthub',
    'acquia_contenthub_subscriber',
    'acquia_contenthub_translations',
    'language',
    'editor',
    'ckeditor5',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setup();
    $this->installConfig(['language']);
    $this->installConfig(['editor']);

    $this->installSchema('acquia_contenthub_subscriber', [SubscriberTracker::IMPORT_TRACKING_TABLE]);
    $this->installSchema('acquia_contenthub_translations',
      [EntityTranslations::TABLE, EntityTranslationsTracker::TABLE]
    );
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->config(ContentHubTranslationsSettingsForm::CONFIG)->set('selective_language_import', TRUE)->save();
    $this->undesiredLanguageRegistrar = $this->container->get('acquia_contenthub_translations.undesired_language_registrar');
    $this->cdfSerializer = $this->container->get('entity.cdf.serializer');
    $this->settings = $this->prophesize(Settings::class);
    $this->settings->getWebhook('uuid')->willReturn('webhook-uuid');
    $this->factory = $this->prophesize(ClientFactory::class);
    $this->client = $this->prophesize(ContentHubClient::class);
    $this->client->getSettings()->willReturn($this->settings->reveal());
    $this->client->getInterestList(Argument::any(), Argument::any(), Argument::any([]))->willReturn([]);
    $this->factory->getClient()->willReturn($this->client->reveal());
    $this->container->set('acquia_contenthub.client.factory', $this->factory->reveal());
  }

  /**
   * Tests paragraphs while importing with single translation.
   */
  public function testParagraphSaveWithWorkflowsTranslation(): void {
    // First import.
    $cdf_document_v1 = $this->createCdfDocumentFromFixtureFile($this->fixtures[0], 'acquia_contenthub_translations');
    $this->cdfSerializer->unserializeEntities($cdf_document_v1, new DependencyStack());
    $this->assertEmpty($this->undesiredLanguageRegistrar->getUndesiredLanguages());

    $this->expectException(TranslationDataException::class);
    $this->expectExceptionMessage('Could not find the following languages: \'hi\'. They are missing from the CDF document and/or they are not enabled on the site.');
    // Second import.
    $cdf_document_v2 = $this->createCdfDocumentFromFixtureFile($this->fixtures[1], 'acquia_contenthub_translations');
    $this->cdfSerializer->unserializeEntities($cdf_document_v2, new DependencyStack());
    $this->assertContains('hi', $this->undesiredLanguageRegistrar->getUndesiredLanguages());

    // Third import - new paragraph.
    $cdf_document_v3 = $this->createCdfDocumentFromFixtureFile($this->fixtures[2], 'acquia_contenthub_translations');
    $this->cdfSerializer->unserializeEntities($cdf_document_v3, new DependencyStack());
    $this->assertContains('hi', $this->undesiredLanguageRegistrar->getUndesiredLanguages());
  }

}
