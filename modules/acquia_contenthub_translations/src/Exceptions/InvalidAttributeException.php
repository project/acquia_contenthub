<?php

namespace Drupal\acquia_contenthub_translations\Exceptions;

/**
 * Thrown on TrackedEntity attribute errors.
 */
class InvalidAttributeException extends \Exception {}
