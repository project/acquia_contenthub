<?php

namespace Drupal\acquia_contenthub_translations\Exceptions;

/**
 * Used for non-translatable entity handler related exception.
 */
class NonTranslatableEntityHandlerException extends \Exception {}
