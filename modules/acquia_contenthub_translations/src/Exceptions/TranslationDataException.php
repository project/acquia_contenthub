<?php

namespace Drupal\acquia_contenthub_translations\Exceptions;

/**
 * Thrown on translation data level errors.
 */
class TranslationDataException extends \Exception {}
