#!/usr/bin/env bash

# Only run on version branches or tags of the module.
# Examples: 8.x-2.x, 8.x-2.40, 3.2.x, 3.2.0
if [[ ! ${GIT_BRANCH} =~ ^(3|8.x)(.|-)[0-9]{1,2}\.x$ ]] && [[ ! ${TAG_NAME} =~ ^(3|8.x)(.|-)[0-9]{1,2}\.[0-9]{1,2}$ ]]; then
  echo 'Not a supported version branch nor tag, skipping.'
  exit
fi

build_dir=$PWD

# If TAG_NAME is not empty, the subject is a tag.
branch=${TAG_NAME:-${GIT_BRANCH}}
deploy_dir=/ramfs/acquia/ach-deploy
echo "ACH Deploy dir: $deploy_dir"
mkdir -p $deploy_dir
cd $deploy_dir

echo "Target git repository: $DRUPAL_ORG"
echo "Git repository: ${GIT_URL}"
echo "Target branch: $branch"

# Communication through HTTPS requires an access token, avoids interactivity.
export GIT_ASKPASS=$build_dir/scripts/ask_pass_gh.sh
git clone $GIT_URL --branch $branch && cd acquia_contenthub || exit 1
git remote add do $DRUPAL_ORG

export GIT_ASKPASS=$build_dir/scripts/ask_pass_do.sh
git push do $branch:$branch
